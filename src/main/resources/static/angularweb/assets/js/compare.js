(function(){
	$('.models-content').on('click', function(event) {
		event.preventDefault();
		var h = $(this).next('.this-style').height();
		if($(this).parent('li').hasClass('selected')){
			$(this).parent('li').removeClass('selected').removeAttr('style').siblings('li').removeClass('selected');
		}else{
			$('#models-list li').removeClass('selected').removeAttr('style');
			$(this).parent('li').addClass('selected').css('height',110+h);
		}
		
	});
	$(window).resize(function(event) {
		$('#models-list li').each(function(index, el) {
			if($(this).find('.this-style').is(':visible')){
				$(this).height(110+$(this).find('.this-style').height())
			}
		});
	});

	
	$('.style-content').on('click', function(event) {
		event.preventDefault();

		var $selectedModels = $('#selected-models li.car-box');
		var html = $(this).html();

		//循环对比列表
		for (var i = 0; i < $selectedModels.length; i++) {

			//如果为空添加内容并跳出for循环
			if(!$selectedModels.eq(i).hasClass('selected')){

				//判断车型是否已在队列
				if($(this).hasClass('selected')) {
					//alert('车型已在对比队列!');
					$('#popup-sendCode').popup({
						content: '车型已在对比队列!'
					});
					return;
				}

				//添加到对比队列
				$(this).addClass('selected');
				$selectedModels.eq(i).addClass('selected').html('<a href="javascript:;" class="del"></a>'+html+'<a href="#" class="btn-testdrive">预约试驾</a><br /><a href="#" class="btn-view-car">查看车型详情</a>').attr('car-id',$(this).attr('car-id')).animateCss('zoomIn');

				//对比队列绑定删除事件
				$selectedModels.eq(i).find('a.del').on('click', function(event) {
					event.preventDefault();
					var carIndex = $(this).parent('li').index();
					
					var $thisUl = $(this).parents('ul');

					//移除对比列表
					$(this).parent('.selected').remove();

					//移除车型列表已选标记
					$('.style-content[car-id='+$(this).parent('li').attr('car-id')+']').removeClass('selected');

					//移除对比列表对应车型内容
					$('table.details tr').each(function(index, el) {
						$(this).find('td').eq(carIndex).remove();
					});
		
					var len = $thisUl.find('.car-box').length;
					//对比列表少于2,则添加一个
					if(len<2){
						$thisUl.find('.car-box').removeClass('show').after('<li class="car-box"></li>');

					}
					
					
					var len = $('#selected-models .selected').length;
					if(len==2){
						$('#selected-models ul, #result-box').removeClass().addClass('two');
						$('#selected-models').find('.add').show();
					}else if(len==1){
						$(this).parent('li').removeClass('selected').removeAttr('car-id').html('');	
						$('#selected-models').find('ul').removeClass().addClass('two').find('.add').hide();
						$('#models-list').show();
						$('#compare-result').hide();
					}

				});

				//获取对比队列车型参数信息
				var len = $('#selected-models li.selected').length;

				if(len > 1){
					if(len == 2 && $(window).width()>640){
						$selectedModels.parents('.selected-models').find('.add').show();
					}
					
					$('#selected-models li.selected').addClass('show');


					//获取车型基本参数数据并显示
					var carId=[];
					for (var i = 0; i < len; i++) {
						carId[i] = Number($selectedModels.eq(i).attr('car-id'));//车id
						
					}
					//console.log(carId);
					getCardata(carId);


				}

				//跳出for循环
				break;
				
			}
		}
	});	

	//获取车型数据,测试数据,实际数据后台处理
	function getCardata(arr) {
		var resultHtml;
		if(arr.length==2) {
			resultHtml = '<table class="details"><tr><td class="config_title"><a id="jbcs" name="jbcs"></a>基本参数</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>48FPK数字液晶仪表（选配）</p><p>Dynaudio丹拿高级环绕音响带12扬声器（选配）</p><p>“MIB Standard Navi 导航包”：含MIB Standard Navi 导航+RVC倒车影像系统+后排USB接口+动感车顶鲨鱼鳍天线+后备箱12V电源（选配）</p><p>“FPK数字液晶仪表包”：含FPK数字液晶仪表(选○8选装包时须同时选○10选装包)（选配）</p><p>“丹拿音响包”：含Dynaudio丹拿高级环绕音响带12扬声器（选配）</p><span>说明</span></td><td><p>48FPK数字液晶仪表（选配）</p><p class="m11">Dynaudio丹拿高级环绕音响带12扬声器（选配）</p><p class="m7">“MIB Standard Navi 导航包”：含MIB Standard Navi 导航+RVC倒车影像系统+后排USB接口+动感车顶鲨鱼鳍天线+后备箱12V电源（选配）</p><p>“FPK数字液晶仪表包”：含FPK数字液晶仪表(选○8选装包时须同时选○10选装包)（选配）</p><p>“丹拿音响包”：含Dynaudio丹拿高级环绕音响带12扬声器（选配）</p><span>说明</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><img src="images/compare/test1.jpg" alt="" class="pic"><p>涡轮增压</p><span>发动机型号</span></td><td><img src="images/compare/test1.jpg" alt="" class="pic"><p>涡轮增压</p><span>发动机型号</span></td></tr><tr><td><p><img src="images/compare/icon2.png" alt=""></p><span>panaroma电动全景天窗带遮阳帘</span></td><td><p><img src="images/compare/icon2.png" alt=""></p><span>panaroma电动全景天窗带遮阳帘</span></td></tr><tr class="bg"><td><p><img src="images/compare/icon3.png" alt=""></p><span>跑车式豪华无框车门</span></td><td><p><img src="images/compare/icon3.png" alt=""></p><span>跑车式豪华无框车门</span></td></tr><tr><td><p><img src="images/compare/icon4.png" alt=""></p><span>17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"</span></td><td><p><img src="images/compare/icon4.png" alt=""></p><span>17寸合金轮毂"Lakeville"</span></td></tr><tr><td class="config_title"><a id="wgpz" name="wgpz"></a>外观配置</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td class="config_title"><a id="nspz" name="nspz"></a>内饰配置</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td class="config_title"><a id="aqkj" name="aqkj"></a>安全科技</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td class="config_title"><a id="sskj" name="sskj"></a>舒适科技</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr></table>';
			$('#result-box').removeClass().addClass('two');

		}else if(arr.length==3){
			resultHtml = '<table class="details"><tr><td class="config_title"><a id="jbcs" name="jbcs"></a>基本参数</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><img src="images/compare/test1.jpg" alt="" class="pic"><p>涡轮增压</p><span>发动机型号</span></td><td><img src="images/compare/test1.jpg" alt="" class="pic"><p>涡轮增压</p><span>发动机型号</span></td><td><img src="images/compare/test1.jpg" alt="" class="pic"><p>涡轮增压</p><span>发动机型号</span></td></tr><tr><td><p><img src="images/compare/icon2.png" alt=""></p><span>panaroma电动全景天窗带遮阳帘</span></td><td><p><img src="images/compare/icon2.png" alt=""></p><span>panaroma电动全景天窗带遮阳帘</span></td><td><p><img src="images/compare/icon2.png" alt=""></p><span>panaroma电动全景天窗带遮阳帘</span></td></tr><tr class="bg"><td><p><img src="images/compare/icon3.png" alt=""></p><span>跑车式豪华无框车门</span></td><td><p><img src="images/compare/icon3.png" alt=""></p><span>跑车式豪华无框车门</span></td><td><p><img src="images/compare/icon3.png" alt=""></p><span>跑车式豪华无框车门</span></td></tr><tr><td><p><img src="images/compare/icon4.png" alt=""></p><span>17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"</span></td><td><p><img src="images/compare/icon4.png" alt=""></p><span>17寸合金轮毂"Lakeville"</span></td><td><p><img src="images/compare/icon4.png" alt=""></p><span>17寸合金轮毂"Lakeville"</span></td></tr><tr><td class="config_title"><a id="wgpz" name="wgpz"></a>外观配置</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td class="config_title"><a id="nspz" name="nspz"></a>内饰配置</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td class="config_title"><a id="aqkj" name="aqkj"></a>安全科技</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td class="config_title"><a id="sskj" name="sskj"></a>舒适科技</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr></table>';
			$('#result-box').removeClass().addClass('three');
		}

		$('#result-box').html(resultHtml);

		$('#models-list').hide();
		$('#compare-result').show();
	}


	$('#selected-models .add').on('click', function(event) {
		event.preventDefault();
		$(this).hide();
		$(this).parents('ul').removeClass('two').addClass('three').find('li').eq(1).after('<li class="car-box"></li>')
		$('#models-list').show();
		$('#compare-result').hide();
		$(window).scrollTop(0);
	});

	$('a.hide-same').on('click', function(event) {
		event.preventDefault();
		if($(this).hasClass('active')){
			$(this).removeClass('active')
		}else{
			$(this).addClass('active')
		}
	});


	var flag=false;
	var $SM = $('#selected-models'), 
		$navBox = $('#compare-result .nav-box');
	$(window).scroll(function(event) {
		
		//console.log(window.scrollY);
		if($(this).scrollTop() > $SM.height()+88 && !$SM.hasClass('fixed')){
			flag=true;
			//$('#compare').css('padding-top',$SM.height()+88);

			$SM.addClass('fixed').width($('.mainbody').width());
			$navBox.addClass('fixed').css({'top':$SM.height(), 'width':$('.mainbody').width()});
			
		}else if($SM.hasClass('fixed') && $(this).scrollTop() < $SM.height()+88){
			flag=false;
			$SM.removeClass('fixed').removeAttr('style');
			$navBox.removeClass('fixed').removeAttr('style');
			//$('#compare').removeAttr('style');
		}
	});

	$(window).resize(function(event) {
		if(!$SM.hasClass('fixed')) return;
		$('#compare').css('padding-top',$SM.height()+88);

		$SM.addClass('fixed').width($('.mainbody').width());
		$navBox.addClass('fixed').css({'top':$SM.height(), 'width':$('.mainbody').width()});
	});

	var $mobileNav=$(".m_anchorlink"),
		$pcNav = $(".anchorlink");
	$(window).scroll(function(){

		if($(window).width()<=640){
			var top = $(this).scrollTop();
			var s = [];
			for(i=0;i<$mobileNav.size();i++){
				var sid = $mobileNav.eq(i).attr("href");
				var _top = $(sid).offset().top-143;
				var b = _top - top;
				if(b<0) b=$mobileNav.size()-i;
				s.push(b);
			}
			//console.log(s);
			var f = s.indexOf(Math.min.apply(Math, s));
		
			$mobileNav.eq(f).addClass("active").parents('li').siblings('li').find('a').removeClass('active')
			var text = $(".nav-mobile a.active").html();
			$('.btn-select').html(text);
		}else{
			var top = $(this).scrollTop();
			var s = [];
			for(i=0;i<$pcNav.size();i++){
				var sid = $pcNav.eq(i).attr("href");
				var _top = $(sid).offset().top-233;
				var b = _top - top;
				if(b<0) b=$pcNav.size()-i;
				s.push(b);
			}
			//console.log(s);
			var f = s.indexOf(Math.min.apply(Math, s));//console.log(f)
		
			$pcNav.eq(f).addClass("active").siblings('a').removeClass('active')
		}
		
	})

	$('.nav-mobile a.btn-select').on('click', function(event) {
		event.preventDefault();
		var $navBox = $('.nav-box');
		if($($navBox).hasClass('show')){
			$navBox.removeClass('show').animate({height: '88px'},200);
		}else{
			$navBox.addClass('show').animate({height: '338px'},200);
		}
		
	});

	
	function scrollOffset(top,time){
		$('html,body').stop().animate({scrollTop:top},time);
	}
	$('#compare-result .nav a').on('click', function(event) {
		event.preventDefault();
		if($(this).hasClass('active')) return;
		$(this).addClass('active').siblings('a').removeClass('active');

		var $navBox = $('.nav-box');
		$navBox.removeClass('show').css('height','88px')
		
		var sid = $(this).attr("href");
		var top = $(sid).offset().top;
		var topY;
		
		if(top<=334){
			topY=0;
		}else{
			
			if(flag){
				topY = top-233;console.log('true');
			}else{
				topY = top-560;console.log('false');
			}
			
		}
		scrollOffset(topY,0);
		return false;
		

	});

	$('.nav-mobile ul li a').on('click', function(event) {
		event.preventDefault();
		$(this).addClass('active').parent('li').siblings('li').find('a').removeClass('active');
		$('a.btn-select').html($(this).html());
		var $navBox = $('.nav-box');
		$navBox.removeClass('show').css('height','88px')
		
		var sid = $(this).attr("href");
		var top = $(sid).offset().top;
		var topY;
		
		if(top<=373){
			topY=0;
		}else{
			
			if(flag){
				topY = top-143;console.log('true');
			}else{
				topY = top-460;console.log('false');
			}
			
		}
		scrollOffset(topY,0);
		return false;
		
	});

	

	$('.send-result').on('click', 'a.submit', function(event) {
		event.preventDefault();
		var text;
		if(isPhoneNum($('input#tel').val())){
			text='信息已发送至您的手机';
		}else{
			text='手机号码格式不正确';
		};
		$('#popup-sendCode').popup({
			content: text
		});
		//$('#popup-sendCode').show().find('p').text(text);
	});

	// $('.popup').on('click', 'a.btn-close', function(event) {
	// 	event.preventDefault();
	// 	$(this).parents('.popup').hide()
	// });

	
})()
