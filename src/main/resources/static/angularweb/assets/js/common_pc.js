(function(){

	//导航中搜索框初始值切换
	$('.searchformtext').each(function() {
		var default_value = this.value;
		$(this).focus(function(){
			if(this.value == default_value) {
				this.value = '';
			}
		});
		$(this).blur(function(){
			if(this.value == '') {
				this.value = default_value;
			}
		});
	});


	/* pc 端主导航点击更多 */
	var MenuMore = $("#main_nav_more");
	$("#main_nav li").last().click(function(event){
		event.preventDefault();
        MenuMore.css({"left":"96px"});
        if(MenuMore.attr("data-count") == 1){
            MenuMore.stop().animate({"width":"250px"});
            MenuMore.attr({"data-count":0})
            $(".mask").fadeIn();
        }else{
            MenuMore.stop().animate({"width":"0"});
            MenuMore.attr({"data-count":1})
            $(".mask").fadeOut();
        }
	})


	$(".mask").click(function(event){
		event.preventDefault();
	    if(MenuMore.attr("data-count") == 0) {
	        $("#main_nav li").last().click();
	    }
	})


})();

//校验手机号是否合法
function isPhoneNum(phonenum){
    
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
    if(!myreg.test(phonenum)){ 
       
        return false; 
    }else{
        return true;
    }
}