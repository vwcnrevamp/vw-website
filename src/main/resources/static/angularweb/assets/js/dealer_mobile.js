var dealerData = [];//数据举例,经销商数据多字段(未确定)
dealerData[0]={//北京
	province: '北京市',
	city: '北京市',
	dealer: {//经销商
		name:'北京华信宏业',
		phone:'010-88888888',
		address:'北京市怀柔区北房镇龙云路2号',
		longitude:'116.710632',
		latitude:'40.332511',
		type:'1'// 1,经销商;2,二手经销商;3,救援
	}
}
dealerData[1]={//北京
	province: '北京市',
	city: '北京市',
	dealer: {
		name:'北京华昌',
		phone:'010-88888888',
		address:'北京市海淀区杏石口路15号',
		longitude:'116.266846',
		latitude:'39.954508',
		type:'1'
	}
}

dealerData[2]={//河北
	province: '河北省',
	city: '石家庄',
	dealer: {
		name:'石家庄冀中',
		phone:'0316-88888888',
		address:'石家庄市西二环南路188号',
		longitude:'114.443075',
		latitude:'38.026477',
		type:'1'
	}
}

dealerData[3]={//河北
	province: '河北省',
	city: '石家庄',
	dealer: {
		name:'河北世纪',
		phone:'0311-88888888',
		address:'石家庄市南二环东路3号',
		longitude:'114.520286',
		latitude:'38.007775',
		type:'1'
	}
}

dealerData[4]={//河北
	province: '河北省',
	city: '廊坊市',
	dealer: {
		name:'廊坊冀东',
		phone:'0316-88888888',
		address:'河北省廊坊市开发区金源道100号',
		longitude:'116.753895',
		latitude:'39.579564',
		type:'3'
	}
}

dealerData[5]={//河北
	province: '河北省',
	city: '廊坊市',
	dealer: {
		name:'廊坊庞大一众',
		phone:'0316-88888888',
		address:'廊坊市安次区安次工业园区',
		longitude:'116.795422',
		latitude:'39.345157',
		type:'2'
	}
}
// 百度地图API初始化	
var map = new BMap.Map("dealer-map");    // 创建Map实例
map.centerAndZoom(new BMap.Point(116.404, 39.915), 12);  // 初始化地图,设置中心点坐标和地图级别
map.setCurrentCity("北京");          // 设置地图显示的城市 此项是必须设置的

$("select").change(function() {
	var selItem = $(this).val();
	if (selItem == $(this).find('option:first').val()) {
		$(this).css("color", "#999");
	} else {
		$(this).css("color", "#414742");
	}
});

$('.province').html(writeOption(0, 0, 'province','<option value="">请选择省份</option>'));

$('.province').bind('change', function() {
	if (dealerData.length == 0) {
		alert('0');
		return;
	}
	var val = $(this).find('option:checked').val();
	var data = writeOption(val, "province", "city",'<option value="">请选择城市</option>');
	$(this).parents('.line').next('.line').find('.city').html(data);
});
$('.city').bind('change', function(event) {
	//$(this).parents('.line').next('.line').find('.dealer-type option[value=""]').prop('selected',true);
	$(this).parents('.line').next('.line').find('.dealer-type option[value="1"]').prop('selected',true).trigger('change');
	
});
$('#popup-map .dealer-type').bind('change', function() {
	$(this).parents('.map-menu').removeClass('open')
});

//获取位置信息
var myPoint = baiduLocation();
function baiduLocation(){//定位
	var geolocation = new BMap.Geolocation();
	var l = {};
	geolocation.getCurrentPosition(function(r){
		if(this.getStatus() == BMAP_STATUS_SUCCESS){
			var mk = new BMap.Marker(r.point);
			map.addOverlay(mk);
			map.panTo(r.point);
			l.a = r.point.lng;
			l.b = r.point.lat;
			//alert('您的位置：'+r.point.lng+','+r.point.lat);
			var geoc = new BMap.Geocoder();    
			
			var pt = r.point;
			geoc.getLocation(pt, function(rs) {
				var addComp = rs.addressComponents;
				$('.province option[value="'+addComp.province+'"]').prop('selected',true).parent('select').trigger('change');
				$('.city option[value="'+addComp.city+'"]').prop('selected',true).parent('select').trigger('change');
				if(!$('.city').val()) return;
				$('.dealer-type').eq(0).trigger('change')
			});
			
		}
		else {
			alert('failed'+this.getStatus());
		}        
	},{enableHighAccuracy: true});
	return l;
};

$('.dealer-type').bind('change', function() {
	if (dealerData.length == 0) {
		alert('0');
		return;
	}

	var city = $(this).parents('.line').prev('.line').find('.city').val();
	if(city ==''){
		alert('请选择城市');
		$(this).val('');
		return;
	}
	$('#navigation-box').hide();
	var val = $(this).val();//经销商类型
	map.centerAndZoom(city,11); //跳转城市地图
	map.clearOverlays();  //删除现有点
	var index = 0;
	var html='';
	var $searchBox = $('#dealer-list ul');
	for (var i = 0; i < dealerData.length; i++) {
		
		if (dealerData[i]['city'] == city) {
			var sContent =
				"<p style='font-size:18px;text-align:center;'>"+(index+1)+"."+dealerData[i]['dealer']['name']+"</p>" +
				"<p style='font-size:16px;line-height:28px;text-align:center;'>地址:"+dealerData[i]['dealer']['address']+"</p>"+
				"<div style='border-top:1px solid #dee1e3;text-align:center;mragin-top:10px;padding: 10px 0;'><p>销售热线:<a color='#019ada' href='tel:"+dealerData[i]['dealer']['phone']+"'>"+dealerData[i]['dealer']['phone']+"</a></p><a href='javascript:;' value='"+dealerData[i]['dealer']['name']+"' class='btn-dealer-info' style='color:#ffffff;background-color:#019ada;width:140px;height:42px;border-radius:3px;text-align:center;display:block;line-height:42px;font-size:18px;margin:0 auto;margin-top:15px;' onclick='showDealerInfo(this)' id='"+i+"' did='"+(index+1)+"'>更多信息</a></div>";
			if (val == 1) {
				
				var myIcon = new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25), {
					offset: new BMap.Size(10, 25),
				    imageOffset: new BMap.Size(0, 0 - index * 25)
				});

				var marker = new BMap.Marker(new BMap.Point(dealerData[i]['dealer']['longitude'], dealerData[i]['dealer']['latitude']),{icon: myIcon}); // 创建点
				map.addOverlay(marker);             // 将标注添加到地图中

				var content = dealerData[i]['dealer']['name']
				addClickHandler(sContent,marker);
				if(index%2 ==0){
					html += '<li a="'+dealerData[i]['dealer']['longitude']+'" b="'+dealerData[i]['dealer']['latitude']+'"><div class="click"><p class="name">'+(index+1)+'.'+dealerData[i]['dealer']['name']+'</p><p class="address">地 址:'+dealerData[i]['dealer']['address']+'</p><p class="phone">销售热线:<a href="tel:'+dealerData[i]['dealer']['phone']+'">'+dealerData[i]['dealer']['phone']+'</a></p><div class="icon"></div></div>'+dealerOtherInfo()+'</li>';
				}else{
					html += '<li class="li-bg" a="'+dealerData[i]['dealer']['longitude']+'" b="'+dealerData[i]['dealer']['latitude']+'"><div class="click"><p class="name">'+(index+1)+'.'+dealerData[i]['dealer']['name']+'</p><p class="address">地 址:'+dealerData[i]['dealer']['address']+'</p><p class="phone">销售热线:<a href="tel:'+dealerData[i]['dealer']['phone']+'">'+dealerData[i]['dealer']['phone']+'</a></p><div class="icon"></div></div>'+dealerOtherInfo()+'</li>';
				}
				index++;
			} else{
				if(dealerData[i]['dealer']['type']==val){
					var myIcon = new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25), {
						offset: new BMap.Size(10, 25),
					    imageOffset: new BMap.Size(0, 0 - index * 25)
					});

					var marker = new BMap.Marker(new BMap.Point(dealerData[i]['dealer']['longitude'], dealerData[i]['dealer']['latitude']),{icon: myIcon}); // 创建点
					map.addOverlay(marker);             // 将标注添加到地图中

					var content = dealerData[i]['dealer']['name']
					addClickHandler(sContent,marker);

					if(index%2 ==0){
						html += '<li a="'+dealerData[i]['dealer']['longitude']+'" b="'+dealerData[i]['dealer']['latitude']+'"><div class="click"><p class="name">'+(index+1)+'.'+dealerData[i]['dealer']['name']+'</p><p class="address">地 址:'+dealerData[i]['dealer']['address']+'</p><p class="phone">销售热线:<a href="tel:'+dealerData[i]['dealer']['phone']+'">'+dealerData[i]['dealer']['phone']+'</a></p><div class="icon"></div></div>'+dealerOtherInfo()+'</li>';
					}else{
						html += '<li class="li-bg" a="'+dealerData[i]['dealer']['longitude']+'" b="'+dealerData[i]['dealer']['latitude']+'"><div class="click"><p class="name">'+(index+1)+'.'+dealerData[i]['dealer']['name']+'</p><p class="address">地 址:'+dealerData[i]['dealer']['address']+'</p><p class="phone">销售热线:<a href="tel:'+dealerData[i]['dealer']['phone']+'">'+dealerData[i]['dealer']['phone']+'</a></p><div class="icon"></div></div>'+dealerOtherInfo()+'</li>';
					}
					index++;
				}
			}

		}
	};
	$('#dealer-list h3 span').text(index);
	$searchBox.html(html);
	dealerListClick()
	var opts = {
		width : 300,     // 信息窗口宽度
		//height: 280,     // 信息窗口高度
		//title : "信息窗口" , // 信息窗口标题
		enableMessage:true//设置允许信息窗发送短息
	};
	function addClickHandler(content,marker){
		marker.addEventListener("click",function(e){
			openInfo(content,e)}
		);
	};
	function openInfo(content,e){
		var p = e.target;
		var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
		var infoWindow = new BMap.InfoWindow(content,opts);  // 创建信息窗口对象 
		map.openInfoWindow(infoWindow,point); //开启信息窗口
	};
	
});
function dealerOtherInfo(){
	var html = '<div class="other"><div class="distance">距离<span>0</span>公里<a href="javascript:;" class="btn-go">到这去</a></div><a href="#" class="btn-testdrive">预约试驾</a><div class="code"><input type="tel" maxlength="11" id="tel" placeholder="短信获取经销商信息"><a href="javascript:;" class="btn-send" onclick="sendInfo(this)";>发送</a></div><img src="images/dealer-popup-img.jpg" class="dealer-pic"><a href="#" class="btn-dealer-index">进入经销商首页</a></div>';
	return html;
}


//数据赋值
function writeOption(val, a, b, html) {
	if(html) {
		var html = html;
	}else{
		var html = '<option value=""></option>';
	}
	
	var arr = [];
	for (var i = 0; i < dealerData.length; i++) {
		if (dealerData[i][a] == val || !val) {
			arr.push(dealerData[i][b]);
		}
	};
	$.unique(arr);
	for (var i = 0; i < arr.length; i++) {
		if(b=='dealer'){
			html += '<option value="' + arr[i]['name'] + '">' + arr[i]['name'] + '</option>';
		}else{
			html += '<option value="' + arr[i] + '">' + arr[i] + '</option>';
		}
		
	};
	return html;
}


$('a#btn-map').on('click',function(event) {
	event.preventDefault();
	
	var city = $(".city").eq(0).val();
	if(city){
		$('#popup-map').show();
		$('.dealer-mobile-form').hide();
		map.centerAndZoom(city,11); //跳转城市地图

	}else{
		$('#popup-location').show();
	}
	
});

$('#popup-map').on('click','.popup-map-top a', function(event) {
	event.preventDefault();
	$('.dealer-mobile-form').show();
	$('#popup-map').hide();
});

$('.popup').on('click','a.popup-close', function(event) {
	event.preventDefault();
	$(this).parents('.popup').hide();
	$('select,input').val('').removeAttr('style');
});

//经销商列表事件绑定
function dealerListClick(){
	$('.dealer-list li div.click').on('click', function(event) {
		
		//event.preventDefault();
		var _this = $(this).parent('li');
		if(_this.hasClass('selected')){
			_this.removeClass('selected');
		}else{
			_this.addClass('selected').siblings('li').removeClass('selected');
			$('.wrap').scrollTop($('.dealer-mobile-form').height()+60)
			//window.location.href="#dealer-list";
			if(myPoint.a){
				var pointA = new BMap.Point(myPoint.a,myPoint.b);  // 
				var pointB = new BMap.Point(Number(_this.attr('a')),Number(_this.attr('b')));

				var d = map.getDistance(pointA,pointB);
				d = d/1000;
				_this.find('.distance span').text(d.toFixed(2));
			}

			_this.find('a.btn-go').on('click', function(event) {
				event.preventDefault();
				$('a#btn-map').trigger('click')
				map.clearOverlays(); 
				dealerDistance(myPoint.a, myPoint.b, Number(_this.attr('a')), Number(_this.attr('b')), function(data){
					$('#navigation-box').show();
					$('#navi-link').attr({
						a: myPoint.a,
						b: myPoint.b,
						c: Number(_this.attr('a')),
						d: Number(_this.attr('b'))
					}).parent('.navigation').find('span.time').text(data.time).next('span.distance').text(data.distance);
				});
				event.stopPropagation();
			});	
		}
		_this.find('.phone a').on('click', function(event) {
			event.stopPropagation();alert(0)
		});
		
	}).find('.phone a').on('click', function(event) {
		event.stopPropagation();
	});
};

//计算路线
function dealerDistance(a,b,c,d,callback){
	//console.log(a);console.log(b);console.log(c);console.log(d)
	var start = new BMap.Point(a,b);
	var end = new BMap.Point(c,d);
	var d={};
	var searchComplete = function (results){
		// if (transit.getStatus() != BMAP_STATUS_SUCCESS){
		// 	return ;
		// }
		var plan = results.getPlan(0);
		d.time = plan.getDuration(true);  
		d.distance = plan.getDistance(true);             //获取距离
		//d.m = results.taxiFare.day.totalFare
		console.log(results)
	}
	var transit = new BMap.DrivingRoute(map, {renderOptions: {map: map, autoViewport: true},
		onSearchComplete: searchComplete,
		onPolylinesSet: function(){
			callback(d);
			
	}}).search(start, end);
}

$('#btn-map-menu').on('click', function(event) {
	event.preventDefault();
	var obj = $(this).parent('.map-menu');
	if(obj.hasClass('open')){
		obj.removeClass('open')
	}else{
		obj.addClass('open')
	}
	
});
$('#map-dealer-info').on('click', 'a.map-dealer-info-close', function(event) {
	event.preventDefault();
	$('#map-dealer-info').removeClass('show');
});
function showDealerInfo(t){
	
	var i = Number($(t).attr('id'));

	var $info = $('#map-dealer-info');
	var html = '<p class="name">'+$(t).attr('did')+'.'+dealerData[i]['dealer']['name']+'</p><p class="address">地 址:'+dealerData[i]['dealer']['address']+'</p><p class="phone">销售热线:<a href="tel:'+dealerData[i]['dealer']['phone']+'">'+dealerData[i]['dealer']['phone']+'</a></p><div class="icon"></div>'+dealerOtherInfo();
	$info.addClass('show').find('.info').html(html);
	if(myPoint.a){
		var pointA = new BMap.Point(myPoint.a, myPoint.b);  // 
		var pointB = new BMap.Point(dealerData[i]['dealer']['longitude'], dealerData[i]['dealer']['latitude']);

		var d = map.getDistance(pointA,pointB);
		d = d/1000;
		$info.find('.distance span').text(d.toFixed(2));
	};
	$info.find('.info a.btn-go').on('click', function(event) {
		event.preventDefault();
		dealerDistance(myPoint.a, myPoint.b, dealerData[i]['dealer']['longitude'], dealerData[i]['dealer']['latitude'], function(data){
			$('#navigation-box').show();
			$('#navi-link').attr({
				a: myPoint.a,
				b: myPoint.b,
				c: dealerData[i]['dealer']['longitude'],
				d: dealerData[i]['dealer']['latitude']
			}).parent('.navigation').find('span.time').text(data.time).next('span.distance').text(data.distance);
		});		
		$('#map-dealer-info').removeClass('show');
	});
	
}

//发送信息

function sendInfo(t){
	if(isPhoneNum($(t).prev('input').val())){
		var $o = $(t).parent('div');
		var html = $o.html();
		$(t).parent('div').text('信息已发至您的手机');
		setTimeout(function(){
			$o.html(html);
		},3000);	
	}else{
		alert('手机号码格式不正确');
	}
}
function navigation(t){
	var url = '//api.map.baidu.com/direction?origin='+$(t).attr('b')+','+$(t).attr('a')+'&destination='+$(t).attr('d')+','+$(t).attr('c')+'&mode=driving&region='+$('.city').val()+'&output=html&src=yourCompanyName|yourAppName';

	// if(browser.versions.ios && !browser.versions.weixin){
	// 	url = 'baidumap://map/direction?origin='+$(t).attr('b')+','+$(t).attr('a')+'&destination='+$(t).attr('d')+','+$(t).attr('c')+'&mode=driving&src=yourCompanyName|yourAppName';

	// } else if(browser.versions.android && !browser.versions.weixin){
	// 	url = 'bdapp://map/direction?origin='+$(t).attr('b')+','+$(t).attr('a')+'&destination='+$(t).attr('d')+','+$(t).attr('c')+'&mode=driving&src=webapp.navi.yourCompanyName.yourAppName';//android
	// }
	window.location.href=url;
	
}


//判断访问终端
var browser = {
	versions: function() {
		var u = navigator.userAgent,
			app = navigator.appVersion;
		return {
			trident: u.indexOf('Trident') > -1, //IE内核
			presto: u.indexOf('Presto') > -1, //opera内核
			webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
			gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
			mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
			ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
			android: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, //android终端
			iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
			iPad: u.indexOf('iPad') > -1, //是否iPad
			webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
			weixin: u.indexOf('MicroMessenger') > -1, //是否微信 （2015-01-22新增）
			qq: u.match(/\sQQ/i) == " qq" //是否QQ
		};
	}(),
	language: (navigator.browserLanguage || navigator.language).toLowerCase()
}