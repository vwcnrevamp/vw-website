(function(){

	//导航中搜索框初始值切换
	$('.searchformtext').each(function() {
		var default_value = this.value;
		$(this).focus(function(){
			if(this.value == default_value) {
				this.value = '';
			}
		});
		$(this).blur(function(){
			if(this.value == '') {
				this.value = default_value;
			}
		});
	});


	var MenuMore = $("#main_nav_more");

	//if($(window).width() < 640){
	    $(".burger_menu").click(function(event){
	    	event.preventDefault();
	        if(MenuMore.attr("data-count") == 1) {
	            MenuMore.stop().animate({"left": "0"});
	            MenuMore.attr({"data-count":0});
	            $(this).children("div").eq(1).addClass("hideline");
	            $(this).children("div").eq(0).addClass("rotate");
	            $(this).children("div").eq(2).addClass("re_rotate");
	        }else{
	            MenuMore.stop().animate({"left": "100%"})
	            MenuMore.attr({"data-count":1})
	            $(this).children("div").eq(1).removeClass("hideline");
	            $(this).children("div").eq(0).removeClass("rotate");
	            $(this).children("div").eq(2).removeClass("re_rotate");
	        }
	    })

	//}

})();

//校验手机号是否合法
function isPhoneNum(phonenum){
    
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
    if(!myreg.test(phonenum)){ 
       
        return false; 
    }else{
        return true;
    }
}