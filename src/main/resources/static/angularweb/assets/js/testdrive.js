var dealerData = [];//数据举例,经销商数据多字段(未确定)
dealerData[0]={//北京
	province: '北京',
	city: '北京',
	dealer: {//经销商
		name:'北京华信宏业',
		phone:'010-88888888',
		address:'北京市怀柔区北房镇龙云路2号',
		longitude:'116.710632',
		latitude:'40.332511',
		type:'1'// 1,经销商;2,二手经销商;3,救援
	}
}
dealerData[1]={//北京
	province: '北京',
	city: '北京',
	dealer: {
		name:'北京华昌',
		phone:'010-88888888',
		address:'北京市海淀区杏石口路15号',
		longitude:'116.266846',
		latitude:'39.954508',
		type:'1'
	}
}

dealerData[2]={//河北
	province: '河北',
	city: '石家庄',
	dealer: {
		name:'石家庄冀中',
		phone:'0316-88888888',
		address:'石家庄市西二环南路188号',
		longitude:'114.443075',
		latitude:'38.026477',
		type:'1'
	}
}

dealerData[3]={//河北
	province: '河北',
	city: '石家庄',
	dealer: {
		name:'河北世纪',
		phone:'0311-88888888',
		address:'石家庄市南二环东路3号',
		longitude:'114.520286',
		latitude:'38.007775',
		type:'1'
	}
}

dealerData[4]={//河北
	province: '河北',
	city: '廊坊',
	dealer: {
		name:'廊坊冀东',
		phone:'0316-88888888',
		address:'河北省廊坊市开发区金源道100号',
		longitude:'116.753895',
		latitude:'39.579564',
		type:'3'
	}
}

dealerData[5]={//河北
	province: '河北',
	city: '廊坊',
	dealer: {
		name:'廊坊庞大一众',
		phone:'0316-88888888',
		address:'廊坊市安次区安次工业园区',
		longitude:'116.795422',
		latitude:'39.345157',
		type:'2'
	}
}
// 百度地图API功能
/*var map = new BMap.Map("dealer-map");    // 创建Map实例
map.centerAndZoom(new BMap.Point(116.404, 39.915), 11);  // 初始化地图,设置中心点坐标和地图级别
map.setCurrentCity("北京");          // 设置地图显示的城市 此项是必须设置的
map.enableScrollWheelZoom(true);*/

$('#province').html(writeOption(0, 0, 'province', '<option value="">省份</option>'));
$('#province').bind('change', function() {
	if (dealerData.length == 0) {
		alert('0');
		return;
	}
	var val = $(this).find('option:checked').val();
	var data = writeOption(val, "province", "city", '<option value="">城市</option>');
	$('#city').html(data).trigger('chosen:updated');
});
$('#city').bind('change', function(event) {
	if($(this).val() == '') return;
	var data = writeOption($(this).val(), "city", "dealer", '<option value="">经销商</option>');
	$('#dealer').html(data).trigger('chosen:updated');
	map.centerAndZoom($(this).val(),11); //跳转城市地图
	map.clearOverlays();  //删除现有点
	var index = 0;
	for (var i = 0; i < dealerData.length; i++) {
		if (dealerData[i]['city'] == $(this).val()) {
			var myIcon = new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25), {
				offset: new BMap.Size(10, 25),
			    imageOffset: new BMap.Size(0, 0 - index * 25)
			});

			var marker = new BMap.Marker(new BMap.Point(dealerData[i]['dealer']['longitude'], dealerData[i]['dealer']['latitude']),{icon: myIcon}); // 创建点
			map.addOverlay(marker);             // 将标注添加到地图中

			var sContent =
				"<p style='font-size:18px;'>"+(index+1)+"."+dealerData[i]['dealer']['name']+"</p>" +
				"<p style='font-size:16px;height:28px;line-height:28px;'>"+dealerData[i]['dealer']['address']+"</p>"+
				"<div style='border-top:1px solid #dee1e3;margin-top:15px;padding-top:24px;overflow: hidden;'><div style='float:left;border-right:1px solid #dee1e3;padding-right:20px;'><p style='height:28px;line-height:28px;font-size16px;width:160px;'>销售热线:<span style='color:#1597d9;'>"+dealerData[i]['dealer']['phone']+"</span></p><a href='#' style='color:#1597d9;margin-top:30px;display:block;background:url(images/icon-arrow1.png) center left no-repeat;padding-left:24px;'>进入销售商首页</a><a href='#' style='color:#ffffff;background-color:#019ada;width:148px;height:42px;border-radius:3px;text-align:center;margin-top:30px;display:block;line-height:42px;font-size:18px;'>预约试驾</a></div><div style='float:left;padding-left:20px;'><img src='images/dealer-popup-img.jpg' /><div style='margin-top:12px;overflow: hidden;'><input type='text' placeholder='短信获取经销商信息' style='float:left;border:1px solid #dee1e3;background-color:#ffffff;width:229px;height:44px;-webkit-appearance:none;line-height:44px;border-radius:3px;padding:0px 12px;'><a href='#' style='color:#ffffff;background-color:#019ada;width:78px;height:42px;border-radius:3px;text-align:center;display:block;line-height:42px;font-size:18px;float:left;margin-left:10px;'>发送</a></div></div></div>";

			var content = dealerData[i]['dealer']['name']
			addClickHandler(sContent,marker);
			index++;
		}
	}
	var opts = {
		width : 523,     // 信息窗口宽度
		height: 280,     // 信息窗口高度
		//title : "信息窗口" , // 信息窗口标题
		enableMessage:true//设置允许信息窗发送短息
	};
	function addClickHandler(content,marker){
		marker.addEventListener("click",function(e){
			openInfo(content,e)}
		);
	}
	function openInfo(content,e){
		var p = e.target;
		var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
		var infoWindow = new BMap.InfoWindow(content,opts);  // 创建信息窗口对象 
		map.openInfoWindow(infoWindow,point); //开启信息窗口
	}
});

//数据赋值
function writeOption(val, a, b, html) {
	if(html) {
		var html = html;
	}else{
		var html = '<option value=""></option>';
	}
	
	var arr = [];
	for (var i = 0; i < dealerData.length; i++) {
		if (dealerData[i][a] == val || !val) {
			arr.push(dealerData[i][b]);
		}
	};
	$.unique(arr);
	for (var i = 0; i < arr.length; i++) {
		if(b=='dealer'){
			html += '<option value="' + arr[i]['name'] + '">' + arr[i]['name'] + '</option>';
		}else{
			html += '<option value="' + arr[i] + '">' + arr[i] + '</option>';
		}
		
	};
	return html;
}
$('select').chosen({
	disable_search:true,
});

$(".testdrive-form span.sex").on('click', function(event) {
	event.preventDefault();
	if($(this).hasClass('selected')) return;
	$(this).addClass('selected').siblings('span.sex').removeClass('selected')
});
$(".testdrive-form span.radio").on('click', function(event) {
	event.preventDefault();
	if($(this).hasClass('selected')) {
		$(this).removeClass('selected')
	}else{
		$(this).addClass('selected')
	}
});
$('.line.clause').on('click', 'a', function(event) {
	event.preventDefault();
	$('#popup-clause').show();
});

$('#popup-ok').on('click', 'a.ok', function(event) {
	$(this).parents('#popup-ok').hide();
	$('select option[value=""]').prop('selected',true).trigger('chosen:updated');
	$('input').val('');
});
$('.popup').on('click', 'a.popup-close', function(event) {
	$(this).parents('.popup').hide();
	$('select option[value=""]').prop('selected',true).trigger('chosen:updated');
	$('input').val('');
});

$('a#submit').on('click', function(event) {
	event.preventDefault();
	var data = {
		arModel : $('#car-model').val(),
		dealer : $('#dealer').val(),
		name : $('#name').val(),
		sex : $('span.sex').attr('val'),
		
		phone : $('#phone').val(),
		time : $('#time').val()
	}
	if(data.arModel != '' && data.dealer != '' && data.name != '' && data.sex != '' && data.phone !='' && isPhoneNum(data.phone)){
		
		$('#popup-ok').show();
    	
    }
	var i = 0;
	for (var selector in data) {
		if(data[selector]==''){
			if(!$('.testdrive-form div.line').eq(i).hasClass('must')) return;
			$('.testdrive-form div.line').eq(i).addClass('warning');
		}else{
			$('.testdrive-form div.line').eq(i).removeClass('warning');
		}
		i++;
    };
    if(!isPhoneNum(data.phone)){
    	$('#phone').parents('.line').addClass('warning');
    }
});

//校验手机号是否合法
function isPhoneNum(val){
    var phonenum = val;
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
    if(!myreg.test(phonenum)){ 
        return false; 
    }else{
        return true;
    }
}