myApp.directive('updateTitle', ['$rootScope', '$timeout',
  function($rootScope, $timeout) {
    return {
      link: function(scope, element) {

        var listener = function(event, toState) {

          var title = "Default Title";
          if (toState.params && toState.params.pageTitle) title = toState.params.pageTitle;
	          $timeout(function() {
	            element.text(title);
	          }, 200, false);
        };

        $rootScope.$on('$stateChangeSuccess', listener);
      }
    };
  }
]);
