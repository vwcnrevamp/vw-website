﻿myApp.directive('datePickerRange', function () {
    return {
    	restrict:'AE',
//		replace:true,
    /*	scope:{
			startDateModel: '@',
			endDateModel: '@'
    	},*/
        templateUrl: 'app/view/date_picker_range.html',
		link: {
                pre: function (scope) {
                    console.log('start date model: ' + scope.startDateModel);					
                    console.log('end date model: ' + scope.endDateModel);
                }
            },
  controller : function($scope,$filter,$timeout){ 
  $scope.minStartDate = new Date();
  $scope.maxStartDate = ''; 
  $scope.minEndDate = new Date(); 
  $scope.maxEndDate = '';

  $scope.$watch('startDateModel', function(v){    
    $scope.minEndDate = v;    
  });

  $scope.$watch('endDateModel', function(v){
    $scope.maxStartDate = v;
  });
			  
			          
        }
    };
});

