myApp.service('AuthService',['$http','$window','urlconfig',function($http,$window,urlconfig){
	
	this.authenticateUser=function(user){
		var getAuthenticateUrl=urlconfig.apiURL+"oauth/token";
		return $http({
		  method: 'POST',
				url: getAuthenticateUrl,
				 headers: {'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Basic dm9sa3N3YWdlbjp2b2xrc3dhZ2Vu'}, 
				transformRequest: function(obj) {
					var str = [];
					for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					return str.join("&");
				}, 
				data: user
		  });
	};
	
	

	this.forgotPassword=function(user){	
		var forgotpasswordUrl=urlconfig.apiURL + "forgotpassword";
		return $http({
			method: 'POST',
			url:forgotpasswordUrl,
			cache:false,
			params:{
				email:user.email
				
			}
		});
	};		
   	
	
 }]);