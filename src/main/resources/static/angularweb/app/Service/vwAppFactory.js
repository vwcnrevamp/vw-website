myApp.config(['$httpProvider', function($httpProvider) {

	$httpProvider.defaults.useXDomain = true;   
	delete $httpProvider.defaults.headers.common['X-Requested-With'];

	$httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
	$httpProvider.defaults.headers.put["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
}
]); 


myApp.factory("apiFactory", ['$http','$rootScope','urlconfig',
                             function ($http,$rootScope, urlconfig) { // This service connects to our REST API
	var serviceBase = urlconfig.apiURL;
	var recruitmentType = "L1";
	var recruitmentApplicationId = 0;

	function getlang(){
		language="";
		var absUrlSplit =window.location.href.split('/');  
		var finalurl = absUrlSplit[absUrlSplit.length-1].split('?');
		if(finalurl.length > 1)
		{
			temp = finalurl[finalurl.length-1].split('=');
			language = temp[temp.length-1];
		}
		return language;
	}
	function serializeData( data ) { 
		// If this is not an object, defer to native stringification.
		if ( ! angular.isObject( data ) ) { 
			return( ( data == null ) ? "" : data.toString() ); 
		}

		var buffer = [];

		// Serialize each key in the object.
		for ( var name in data ) { 
			if ( ! data.hasOwnProperty( name ) ) { 
				continue; 
			}

			var value = data[ name ];

			buffer.push(
					encodeURIComponent( name ) + "=" + encodeURIComponent( ( value == null ) ? "" : value )
			); 
		}

		// Serialize the buffer and clean it up for transportation.
		var source = buffer.join( "&" ).replace( /%20/g, "+" ); 
		alert(source);
		return( source ); 
	}
	var transform = function(data){
		return serializeData(data);
	}

	var obj = {};

	obj.languageCode = getlang();
	obj.getLang  = function (q, param) {
		var  language=getlang();
		if(param !== ""){
			param = param + '&lang=' + language;
		}else
		{
			param = param + '?lang=' + language;
		}
		return $http.get(serviceBase + q + param).then(function (results) {
			return results.data;
		});
	};
	obj.getExt = function (q, param) {

		return $http.get(q + param).then(function (results) {
			return results.data;
		});
	};
	obj.getAuth = function (q) {
        if (localStorage.getItem("loginOauthToken")) {
            $http.defaults.headers.common['Authorization'] = 'bearer ' +  localStorage.getItem("loginOauthToken"); 
        }
        return $http.get(serviceBase + q).then(function (results) {
            return results.data;
        });
    };
	
     obj.get= function (q) {
         return $http.get(serviceBase + q).then(function (results) {
             return results.data;
         });
     };
     
	obj.post = function(url,data,callback){
		$http({
			method: 'POST',
			url: serviceBase + url,
			headers: {
				'Content-Type': 'application/json'
			}, 
			data: data
		}).then(function (response){
			callback(response);
		});     
	};


	obj.postjsonupload = function (q, object) {
		return $http.post(serviceBase + q,object, {
			headers: {'Content-Type': 'application/json'}
		}).then(function (results) {
			return results;
		});
	};

	obj.put = function(url,data,callback){
		$http({
			method: 'PUT',
			url: serviceBase + url,
			headers: {
				'Content-Type': 'application/json'
			}, 
			data: data
		})
		.then(function (response){
			callback(null,response);
		});
	};
	
	obj.setRecruitmentType = function(str){
		recruitmentType = str;
    };

    obj.getRecruitmentType =function(){
        return recruitmentType;
    };
    
    obj.getRecruitmentHomeUrl = function(){
    	var url = "#/dealerRecruitment";
		if(getlang()){
			url=url+"?lang="+getlang();
		}
		return url;
    };
    
    obj.getRecruitDetailsPageUrl = function(){    	
    	var url = "#/dealerRecruitmentDetails";
		if(getlang()){
			url=url+"?lang="+getlang();
		}
		return url;
    };
    
    obj.getRecruitRecordUrl = function(){    	
    	var url = "#/recruitRecord";
		if(getlang()){
			url=url+"?lang="+getlang();
		}
		return url;
    };
    
    obj.getRecruitApplyUrl = function(){    	
    	var url = "#/dealerRecruitmentApply";
		if(getlang()){
			url=url+"?lang="+getlang();
		}
		return url;
    };
    
    obj.setRecruitmentApplicationId = function(id){
    	recruitmentApplicationId = id;
    };

    obj.getRecruitmentApplicationId =function(){
        return recruitmentApplicationId;
    };

	return obj;
}]);