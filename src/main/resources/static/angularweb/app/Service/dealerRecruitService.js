myApp.service('dealerRecruitService', function(apiFactory){
	var param = '';
	this.registerDealerRecruit = function(data,callback){
		apiFactory.post('registerdealer', data, callback);
	};
	this.generateVerificationCode = function(mobileNumber,callback) {
		if(mobileNumber){
			param = '?mobileNumber=' + mobileNumber;
		}
		apiFactory.get('getconfirmationcode' + param).then(function(results) {
			callback(results);
		});
	};
	this.getRecruitCityDetails = function(data,callback){
		if(data.province === undefined)
			{
			apiFactory.postjsonupload('getDealerRecruitmentCities'+param, data).then(function(results) {
				callback(results);
			});
			}
		if(data.province)
		{
			if(data.city)
			{
				apiFactory.postjsonupload('getDealerRecruitingCityByCity'+param, data).then(function(results) {
					callback(results);
				});
			}
			else
				{
			apiFactory.postjsonupload('getDealerRecruitingCityByProvince'+param, data).then(function(results) {
				callback(results);
			});
				}
		}
		
		if(data.district)
		{
			apiFactory.postjsonupload('getDealerRecruitingCityByDistrict'+param, data).then(function(results) {
				callback(results);
			});
		}

	};
	
	this.getRecruitCityByAllProvince = function(callback) {
		apiFactory.get('getDealerRecruitingCityDropDownData' + param).then(function(results) {
			callback(results);
		});
	};
	this.getRecruitCityByProvince = function(province,callback) {
		if(province)
			{
			param = '?province='+province;
			}
		apiFactory.get('getDealerRecruitingProvincesCitiesDistrict' + param).then(function(results) {
			callback(results);
		});
	};
	
	this.getApplicationRecordsr = function(data, callback){
		if(data.status) {
			apiFactory.post('getDealerRecruitmentByStatusUserId', data, callback);
			
		} else {
			apiFactory.post('getDealerRecruitmentByUserId', data, callback);
		}
	};
	
	this.addDealerRecruitment = function(data,callback){
		if(data){
			apiFactory.postjsonupload('addDealerRecruitment'+param, data).then(function(results) {
				callback(results);
			});
		}
		
	};
	
	this.getallSourceSites = function(callback) {
	  	apiFactory.getLang('getallSourceSites', param).then(function(results) {
	        callback(results);
	    });
   	};
   	
   	this.downloadForm = function(callback) {
	  	apiFactory.getLang('downloadForm', param).then(function(results) {
	        callback(results);
	    });
   	};
   	
   	this.getRecruitmentById = function(id,callback) {
   		if(id)
		{
		param = '?id='+id;
		}   		
	   	apiFactory.get('getRecruitmentById'+param , id).then(function(results) {
	        callback(results);
	    });
   	};
   	
   	this.updateDealerRecruitment = function(data,callback){
		if(data){
			apiFactory.postjsonupload('updateDealerRecruitment'+param, data).then(function(results) {
				callback(results);
			});
		}
		
	};
	
});
