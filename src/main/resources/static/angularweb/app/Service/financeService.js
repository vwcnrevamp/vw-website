myApp.service('financeService', function(apiFactory){
	var param = '';

  /* 	this.loanCalculate = function (data, callback){
   		apiFactory.post('calculateloan' ,data).then(function(results) {
	        callback(results);
	    });
   	};*/
   	this.loanCalculate = function (data, callback){
		//apiFactory.postjsonupload('calculateloan', data).then(function(results) {
		apiFactory.postjsonupload('calculateFinancialLoan', data).then(function(results) {
	        callback(results);
   	});
   	};
   	
	
	this.getallModelsBySeries = function(series,callback) {
		if(series){
			param = '?series=' + series;
		}
	   	apiFactory.getLang('getcarModelsbyseries' , param).then(function(results) {
	        callback(results);
	    });
   	};
   	this.getModelByCode = function(id,callback) {
		if(id){
			param = '?id=' + id;
		}
	   	apiFactory.get('viewcar' , param).then(function(results) {
	        callback(results);
	    });
   	};
	this.getAllFinancialInstitutions = function(callback) {
	  	apiFactory.getLang('getallFinancialInstitutions' , param).then(function(results) {
	        callback(results);
	    });
   	};
	this.getAllFinancialProductsByInstitution = function(financialInstitution,callback) {
		if(financialInstitution){
			param = '?financialInstitution=' + financialInstitution;
		}
	  	apiFactory.getLang('getallFinancialProductsByInstitute' , param).then(function(results) {
	        callback(results);
	    });
   	};
   	
 	this.allfinancial = function (data, callback){
		//apiFactory.postjsonupload('calculateloan', data).then(function(results) {
		apiFactory.postjsonupload('calculateFinancialLoans', data).then(function(results) {
	        callback(results);
   	});
   	};
   	
   	this.applyloan = function (data, callback){		
		apiFactory.postjsonupload('applyloan', data).then(function(results) {
	        callback(results);
   	});
   	};
});