myApp.service('testDriveService', function(apiFactory){
   	// User CRUD operation
	this.param='';
	this.registerTestDrive = function (data, callback){
		apiFactory.post('createtestdrive', data, callback);
   	};
	/*this.getCarModels = function(callback) {
	  	apiFactory.get('getallcars',param).then(function(results) {
        callback(results);
    });
	};*/
   	this.getCarSeries = function(callback) {
	  	apiFactory.getLang('getallcarseries',this.param).then(function(results) {
        callback(results);
    });
	};
});