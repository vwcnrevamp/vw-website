var myApp=angular.module('vwApp', [
'angulartics',
'angulartics.google.analytics',
'ngGeolocation',
'ngCookies',
'ngRoute',
'ui.router',
'ui.bootstrap',
'ngStorage',
'ngDialog',                                        
'oc.lazyLoad',
'ngResource', 'angular.chosen',
/*ngBaiduMap,*/
'ngSanitize',
'angularUtils.directives.dirPagination'
]);

var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
};

myApp.constant('urlconfig', {

	apiURL: "http://192.168.1.133:9997/volkswagen/",	
	//apiURL: "http://localhost:9090/volkswagen/",
	//mapMarker: "http://api.map.baidu.com/img/markers.png",
	//mapHoldOnImg: "http://static.blog.csdn.net/images/medal/holdon_s.gif"
	// uncomment below for server deployment
	//apiURL: "https://114.215.78.105:8443/volkswagen/",
	mapMarker: "https://api.map.baidu.com/img/markers.png",
	mapHoldOnImg: "https://static.blog.csdn.net/images/medal/holdon_s.gif"

}); 

myApp.config(function($stateProvider, $urlRouterProvider,
		ngDialogProvider,$httpProvider, $ocLazyLoadProvider,$analyticsProvider) {   

	ngDialogProvider.setDefaults({
		className: 'ngdialog-theme-default',
	});
	
	$analyticsProvider.virtualPageviews(false);
	
	$ocLazyLoadProvider.config({
		debug:true,
		events:true,
	});

	$urlRouterProvider.when('','/testDrive');
	$urlRouterProvider.when('/','/testDrive');

	$urlRouterProvider.otherwise('/testDrive');

	$stateProvider
	.state('testDrive', {
		url: '/testDrive',
		templateUrl:(!isMobile.any() )? 'angularweb/app/view/testdrive.html' : 'angularmobile/test.html',
				controller: (!isMobile.any() )? 'testDriveController': 'testMobileController',
						params: {
							series:null
						},
						resolve: {
							loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
								return $ocLazyLoad.load([
								                         'angularweb/app/controller/testDriveController.js',
								                         'angularweb/app/controller/testMobileController.js']);
							}]
						}
	})	
	.state('test', {
		url: '/test',
		templateUrl:(!isMobile.any() )? 'angularweb/app/view/test.html' : 'angularmobile/testdrive_mobile.html',
				controller: (!isMobile.any() )? 'testController': 'testDriveMobileController',
						params: {
					        obj: null
					    },
						resolve: {
							loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
								return $ocLazyLoad.load([
								                         'angularweb/app/controller/testController.js',
								                         'angularweb/app/controller/testDriveMobileController.js']);
							}]
						}
	}) 
	.state('dealer', {
		url: '/dealer',
		templateUrl:(!isMobile.any() )? 'angularweb/app/view/dealer_test.html' : 'angularmobile/dealer_mobile.html',
				controller: (!isMobile.any() )? 'searchController': 'searchDealerMobileController',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
						return $ocLazyLoad.load(['angularweb/app/controller/searchController.js',
						                         'angularweb/app/controller/searchDealerMobileController.js']);
					}]
				}
	})
	.state('searchDealer', {
		url: '/searchDealer',
		templateUrl:(!isMobile.any() )? 'angularweb/app/view/dealer.html' : 'angularmobile/dealer_test.html',
				controller: (!isMobile.any() )? 'searchDealerController': 'searchMobileController',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
						return $ocLazyLoad.load(['angularweb/app/controller/searchDealerController.js',
						                         'angularweb/app/controller/searchMobileController.js']);
					}]
				}
	})
	.state('finance', {
		url: '/finance',
		templateUrl:'angularweb/app/view/finance.html' ,
				controller: 'financeController',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
						return $ocLazyLoad.load('angularweb/app/controller/financeController.js');
					}]
				}
	})
	.state('dealerRecruitment', {
		url: '/dealerRecruitment',
		templateUrl: 'angularweb/app/view/recruit.html' ,
		controller: 'dealerRecruitmentController',
		resolve: {
			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('angularweb/app/controller/dealerRecruitmentController.js');
			}]
		}
	})
	.state('dealerRecruitmentDetails', {
		url: '/dealerRecruitmentDetails',
		templateUrl: 'angularweb/app/view/recruit_details.html' ,
		controller: 'dealerRecruitDetailsController',
		resolve: {
			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('angularweb/app/controller/dealerRecruitDetailsController.js');
			}]
		}
	})
	.state('dealerRecruitmentReg', {
		url: '/dealerRecruitmentReg',
		templateUrl: 'angularweb/app/view/recruit_reg.html' ,
		controller: 'dealerRecruitmentController',
		resolve: {
			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('angularweb/app/controller/dealerRecruitmentController.js');
			}]
		}
	})
	.state('recruitRecord', {
		url: '/recruitRecord',
		templateUrl: 'angularweb/app/view/recruit_record.html' ,
		controller: 'myRecordController',
		resolve: {
			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('angularweb/app/controller/myRecordController.js');
			}]
		}
	})
	.state('dealerRecruitmentApply', {
		url: '/dealerRecruitmentApply',
		templateUrl: 'angularweb/app/view/recruit_apply.html' ,
		controller: 'dealerRecruitApplyController',
		resolve: {
			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('angularweb/app/controller/dealerRecruitApplyController.js');
			}]
		}
	})
	.state('recruitLogin', {
		url: '/recruitLogin',
		templateUrl: 'angularweb/app/view/recruit_login.html' ,
		controller: 'recruitLoginController',
		resolve: {
			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('angularweb/app/controller/recruitLoginController.js');
			}]
		}
	})
	.state('compareCarModel', {
		url: '/compareCarModel',
		templateUrl:'angularweb/app/view/compare.html' ,
				controller: 'carModelComparisionConrtoller',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
						return $ocLazyLoad.load('angularweb/app/controller/carModelComparisionConrtoller.js');
					}]
				}
	})
	.state('applyLoan', {
		url: '/applyLoan',
		templateUrl:'angularweb/app/view/apply_loan.html' ,
				controller: 'applyLoanController',
				params: {
			        obj: null
			    },
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
						return $ocLazyLoad.load('angularweb/app/controller/applyLoanController.js');
					}]
				}
	})
	.state('golf_rline', {
		url: '/gti/r_line14tsi_manual',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BN15FV', pageTitle:'【高尔夫R-Line1.4TSI手动型参数配置】一汽车型>一汽-大众品牌官网'}
	})
	.state('golf_rlineAuto', {
		url: '/gti/r_line14tsi_automatic',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BN15FZ', pageTitle:'【高尔夫R-Line1.4TSI自动型参数配置】一汽车型>一汽-大众品牌官网'}
	})
	.state('golf_gti', {
		url: '/gti/20tsi',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BN19UZ', pageTitle:'【高尔夫GTI 2.0 TSI参数配置】一汽车型>一汽-大众品牌官网'}
	})
	.state('golf_auto', {
		url: '/golf/14tsi_automatic_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BN13AZ', pageTitle:'【高尔夫1.4TSI自动舒适型参数配置】一汽车型>一汽-大众品牌官网'}
	})
    .state('golf_manual', {
	url: '/golf/14tsi_manual_comfortable',
	templateUrl:'angularweb/app/view/config.html' ,
			controller: 'carConfigController',
			params: { modelCode: 'BN13D1', pageTitle:'【高尔夫1.4TSI手动舒适型参数配置】一汽车型>一汽-大众品牌官网'}
    })
   .state('golf_16autoComfort', {
	url: '/golf/16l_automatic_comfortable',
	templateUrl:'angularweb/app/view/config.html' ,
			controller: 'carConfigController',
			params: { modelCode: 'BN13B3', pageTitle:'【高尔夫1.6L自动舒适型参数配置】一汽车型>一汽-大众品牌官网'}
   })
   .state('golf_16autoFashion', {
	url: '/golf/16l_automatic_fashion',
	templateUrl:'angularweb/app/view/config.html' ,
			controller: 'carConfigController',
			params: { modelCode: 'BN12B3', pageTitle:'【高尔夫1.6L自动时尚型参数配置】一汽车型>一汽-大众品牌官网'}
   })
   .state('golf_16manualFash', {
	url: '/golf/16l_manual_fashion',
	templateUrl:'angularweb/app/view/config.html' ,
			controller: 'carConfigController',
			params: { modelCode: 'BN12B1', pageTitle:'【高尔夫1.6L手动时尚型参数配置】一汽车型>一汽-大众品牌官网'}
   
   }).state('Golf_25_anniversary', {
		url: '/golf/25_anniversary',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'GLF000008', pageTitle:'【高尔夫25周年】一汽车型>一汽-大众品牌官网'}
	   
	})
   .state('CC_anniversary', {
		url: '/cc/anniversary',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'CC000007', pageTitle:'【300TSI(118kW) 25周年纪念版】一汽车型>一汽-大众品牌官网'}
	   
	})
	
	.state('New_Jetta_14l_Trendline_Manual_Avantgarde', {
		url: '/newjetta/14l_trendline_manual_avantgarde',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'JETT000001', pageTitle:'【捷达质惠版 1.4L MT Trendline 手动时尚型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Jetta_14l_manual_comfortable', {
		url: '/newjetta/14l_manual_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'JETT000002', pageTitle:'【捷达质惠版 1.4L MT Comfortline 手动舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Jetta_16l_manual_Avantgarde', {
		url: '/newjetta/16l_manual_avantgarde',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'JETT000003', pageTitle:'【捷达质惠版 1.6L MT Trendline 手动时尚型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Jetta_16l_Auto Avantgarde', {
		url: '/newjetta/16l_auto avantgarde',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'JETT000004', pageTitle:'【捷达质惠版 1.6L AT Trendline 自动时尚型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Jetta_16l_manual_comfortable', {
		url: '/newjetta/16l_manual_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'JETT000005', pageTitle:'【捷达质惠版1.6L MT Comfortline 手动舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Jetta_16l16l_automatic_comfortable', {
		url: '/newjetta/16l_automatic_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'JETT000006', pageTitle:'【捷达质惠版 1.6L AT Comfortline 自动舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('Jetta_Edition_Jetta', {
		url: '/jetta/Edition_Jetta',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'JETT000008', pageTitle:'【捷达25周年纪念版】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Polaris_16_manual_Avantgarde', {
		url: '/allnewbora/16_manual_avantgarde',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '1520G1', pageTitle:'【1.6L手动时尚型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Polaris_16_auto_Avantgarde', {
		url: '/allnewbora/16_auto_avantgarde',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '1520G3', pageTitle:'【1.6L自动时尚型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Polaris_16l_manual_comfortable', {
		url: '/allnewbora/16l_manual_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '1521G1', pageTitle:'【1.6L手动舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Polaris_16l_automatic_comfortable', {
		url: '/allnewbora/16l_automatic_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '1521G3', pageTitle:'【1.6L自动舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Polaris_16l_automatic_luxury', {
		url: '/allnewbora/16l_automatic_luxury',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '1522G3', pageTitle:'【1.6L自动豪华型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Polaris_230tsi_manual_comfortable', {
		url: '/allnewbora/230tsi_manual_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '1521FV', pageTitle:'【230TSI (1.4T)手动舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Polaris_230tsi_automatic_comfortable', {
		url: '/allnewbora/230tsi_automatic_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '1521FZ', pageTitle:'【230TSI (1.4T)自动舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Polaris_230tsi_automatic_luxury', {
		url: '/allnewbora/230tsi_automatic_luxury',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '1522FZ', pageTitle:'【230TSI (1.4T)自动豪华型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Polaris_commero_bora', {
		url: '/allnewbora/commero_bora',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NPOL000009', pageTitle:'【宝来25周年纪念版】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('Magoton_18TSI Premier', {
		url: '/mago/18tsi_premier',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '9424B7', pageTitle:'【迈腾 1.8TSI (118kW) 智享豪华型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('Golf_auto_FlagShip', {
		url: '/golf/auto_flagShip',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BN14F5', pageTitle:'【1.4TSI自动旗舰】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('Golf_automatic_deluxe', {
		url: '/golf/automatic_deluxe',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BN14D5', pageTitle:'【1.4TSI自动豪华】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('Golf_Sportsvan1.6LManualComfort', {
		url: '/golfsportsvan/16l_manual_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '0S12B1', pageTitle:'【1.6L手动舒适型】一汽车型>一汽-大众品牌官网'}
		
	})
	.state('Golf_Sportsvan1.6LAuto_Comfort', {
		url: '/golfsportsvan/16l_automatic_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '0S12B3', pageTitle:'【1.6L自动舒适型】一汽车型>一汽-大众品牌官网'}
		
	})
	.state('Golf_Sportsvan180TSI_1.2TAuto_Aggressive', {
		url: '/golfsportsvan/180tsi_automatic_aggressive',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '0S13AZ', pageTitle:'【180TSI (1.2T)自动进取型】一汽车型>一汽-大众品牌官网'}
	
	
	})
	.state('Golf_Sportsvan230TSI_1.4TManual_Aggressive', {
		url: '/golfsportsvan/230tsi_manual_aggressive',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '0S13DV', pageTitle:'【230TSI (1.4T) 手动进取型】一汽车型>一汽-大众品牌官网'}
	 

    })
    .state('Golf_Sportsvan230tsi_automatic_aggressive', {
		url: '/golfsportsvan/230tsi_automatic_aggressive',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '0S13DZ', pageTitle:'【230TSI (1.4T) 自动进取型】一汽车型>一汽-大众品牌官网'}
	  	
	})
	.state('Golf_Sportsvan230tsi_automatic_luxury', {
		url: '/golfsportsvan/230tsi_automatic_luxury',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '0S14DZ', pageTitle:'【230TSI (1.4T) 自动豪华型】一汽车型>一汽-大众品牌官网'}
	   
	
	})
	.state('Golf_Sportsvan280tsi_automatic_luxur', {
		url: '/golfsportsvan/280tsi_automatic_luxur',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '0S13FZ', pageTitle:'【280TSI (1.4T) 自动豪华型】一汽车型>一汽-大众品牌官网'}
	   
	
	})
	.state('Golf_Sportsvan280tsi_automatic_admiral', {
		url: '/golfsportsvan/280tsi_automatic_admiral',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '0S14FZ', pageTitle:'【280TSI (1.4T) 自动旗舰型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Jetta230TSI_Anniversary_Edition',{
		url: '/nJetta/230tsi_anniversary_edition',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NJET000010', pageTitle:'【230TSI (1.4T) 25周年纪念版】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('N_SAGI_GLI/147kWSedan',{
		url: '/n_sagi_gli/147kwsedan',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NSGI000001', pageTitle:'【2.0T 147kW三厢】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_jetta_sedan', {
		url: '/n_jeeta_rl/sedan',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NJRL000001', pageTitle:'【280TSI然后为110kW】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Magotan_280Charming', {
		url: '/n_mago/280charming',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NMAG000001', pageTitle:'【280TSI DSG 舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Magotan_280style', {
		url: '/n_mago/280style',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NMAG000002', pageTitle:'【280TSI DSG 领先型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Magotan_330Charming', {
		url: '/n_mago/330Charming',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NMAG000003', pageTitle:'【330TSI DSG 舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Magotan_330LeadingStyle', {
		url: '/n_mago/330LeadingStyle',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NMAG000004', pageTitle:'【330TSI DSG 领先型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Magotan_330tsi_dsg_luxury', {
		url: '/newmagotan/330tsi_dsg_luxury',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NMAG000005', pageTitle:'【330TSI DSG 豪华型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Magotan_330tsi_distinguished_type', {
		url: '/newmagotan/330tsi_distinguished_type',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NMAG000006', pageTitle:'【330TSI DSG 尊贵型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Magotan_380tsi_dsg_luxury', {
		url: '/newmagotan/380tsi_dsg_luxury',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NMAG000007', pageTitle:'【380TSI DSG 豪华型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Magotan_380tsi_distinguished_type', {
		url: '/newmagotan/380tsi_distinguished_type',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NMAG000008', pageTitle:'【380TSI DSG 尊贵型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('New_Magotan_380tsi_dsg_admiral', {
		url: '/newmagotan/380tsi_dsg_admiral',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'NMAG000009', pageTitle:'【380TSI DSG 旗舰型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('Magotan_14TSI', {
		url: '/mago/14tsi',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'MGTN000001', pageTitle:'【迈腾 1.4TSI (96kW) 舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	
	.state('Mogoton_18tsi_comfortable', {
		url: '/magotan/18tsi_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'MGTN000002', pageTitle:'【迈腾 1.8TSI (118kW) 智享舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('Mogoton_18tsi_enjoy_leading', {
		url: '/magotan/18tsi_enjoy_leading',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'MGTN000003', pageTitle:'【迈腾 1.8TSI (118kW) 智享领先型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('Mogoton_18tsi_enjoy_luxury', {
		url: '/magotan/18tsi_enjoy_luxury',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'MGTN000004', pageTitle:'【迈腾 1.8TSI (118kW) 智享豪华型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('Mogoton_20tsi_enjoy_luxury', {
		url: '/magotan/20tsi_enjoy_luxury',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'MGTN000005', pageTitle:'【迈腾2.0TSI (147kW) 智享豪华型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('Mogoton_20tsi_enjoy_distinguish', {
		url: '/magotan/20tsi_enjoy_distinguish',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'MGTN000006', pageTitle:'【迈腾2.0TSI (147kW) 智享尊贵型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('Mogoton_20tsi_enjoy_flagship', {
		url: '/magotan/20tsi_enjoy_flagship',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'MGTN000007', pageTitle:'【迈腾2.0TSI (147kW) 旗舰型】一汽车型>一汽-大众品牌官网'}
	   
	})
	
	.state('Mogoton_Anniversary _ommemorative', {
		url: '/magotan/Anniversary _ommemorative',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'MGTN000008', pageTitle:'【迈腾25周年纪念版】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('CC_18TSI_Distinguished type', {
		url: '/cc/18tsi_Distinguished type',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '9911A3', pageTitle:'【CC 1.8TSI(118kW) 尊贵型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('CC_18TSI_luxury', {
		url: '/cc/18tsi_luxury',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '9912A3', pageTitle:'【CC 1.8TSI(118kW) 豪华型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('CC_20TSI_Distinguished type', {
		url: '/cc/20tsi_distinguished_type',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '9911B4', pageTitle:'【CC 2.0TSI(147kW) 尊贵型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('CC_20TSI_luxury', {
		url: '/cc/20tsi_luxury',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '9912B4', pageTitle:'【CC 2.0TSI(147kW) 豪华型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('CC_20TSI_Extreme_type', {
		url: '/cc/20tsi_extreme_type',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '9915B4', pageTitle:'【CC 2.0TSI(147kW) 至尊型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('CC_30_V6FSI', {
		url: '/cc/30_v6fsi',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: '9914C4', pageTitle:'【CC 3.0 V6 FSI(184kW) 3.0 V6】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('Jetta_14autoSpor', {
		url: '/jetta/14autosport',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BS24A7', pageTitle:'【捷达质惠版 1.4TSI DSG Sportline 自动运动版】一汽车型>一汽-大众品牌官网'}
	   
	}).state('N_Sagitar_16l_manual_avantgarde', {
		url: '/newsagitar/16l_manual_avantgarde',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BK22B1', pageTitle:'【1.6L 手动时尚型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('N_Sagitar_16l_auto_avantgarde', {
		url: '/newsagitar/16l_auto_avantgarde',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BK22B3', pageTitle:'【1.6L 自动时尚型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('N_Sagitar_16l_manual_comfortable', {
		url: '/newsagitar/16l_manual_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BK23B1', pageTitle:'【新速腾1.6L手动舒适型参数配置】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('N_Sagitar_16l_auto_comfortable', {
		url: '/newsagitar/16l_automatic_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BK23B3', pageTitle:'【新速腾1.6L自动舒适型参数配置】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('N_Sagitar_230tsi_manual_comfortable', {
		url: '/newsagitar/230tsi_manual_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BK23J1', pageTitle:'【230TSI(1.4T) 手动舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('N_Sagitar_230tsi_dsg_comfortable', {
		url: '/newsagitar/230tsi_dsg_comfortable',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BK23J5', pageTitle:'【230TSI(1.4T) DSG舒适型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('N_Sagitar_230tsi_manual_luxury', {
		url: '/newsagitar/230tsi_manual_luxury',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BK24J1', pageTitle:'【230TSI(1.4T) 手动豪华型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('N_Sagitar_230tsi_dsg_luxury', {
		url: '/newsagitar/230tsi_dsg_luxury',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BK24J5', pageTitle:'【230TSI(1.4T) DSG豪华型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('N_Sagitar_280tsi_dsg_admiral', {
		url: '/newsagitar/280tsi_dsg_admiral',
		templateUrl:'angularweb/app/view/config.html' ,
				controller: 'carConfigController',
				params: { modelCode: 'BK24LZ', pageTitle:'【280TSI(1.4T) 旗舰型】一汽车型>一汽-大众品牌官网'}
	   
	})
	.state('compare', {
		url: '/compare/models',
		templateUrl:'angularweb/app/view/compare.html' ,
			controller: 'carModelComparisionConrtoller',
			resolve: {
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load('angularweb/app/controller/applyLoanController.js');
				}]
			}
	}).state('compareResult', {
		url: '/compare/result/:model1/:series1/:model2/:series2',
		templateUrl:'angularweb/app/view/compare.html' ,
			controller: 'carModelComparisionConrtoller',
			resolve: {
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load('angularweb/app/controller/applyLoanController.js');
				}]
			}
	});
});

