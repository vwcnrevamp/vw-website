angular.module('vwApp').controller('applyLoanController',['$http','$scope','$cookies','$rootScope','$location','$filter','$window','$localStorage','$state','$cookieStore','$timeout','$geolocation','homeService','financeService','localizationService',function($http,$scope,$cookies,$rootScope,$location,$filter,$window,$localStorage, $state,$cookieStore,$timeout,$geolocation,homeService,financeService,localizationService) {
	$scope.applyLoan = {
			car_series:"",
			car_model:"",
			car_name:"",
			financial_institutions:"",
			bank_name:"",
			financial_product:"",
			product_name:"",
			total_payment:"",
			down_payment:"",
			loan_tenure:"",
			interest:"",
			created_by:""
	};
	$scope.translation = [];
	$scope.provinces =[];
	$scope.temp_province = [] ;
	$scope.final_province = [];
	$scope.cities =[];   
	$scope.dealers = [];
	$scope.translate = function(){	
		localizationService.getBundle(function(data){
			$scope.translation = data;
		});			   	 
	};	
	$scope.translate();
	if(localStorage.getItem("currentUser") != null && localStorage.getItem("currentUser") != "undefined" && localStorage.getItem("currentUser") != ""){
		var user_details = localStorage.getItem("currentUser");
		$scope.applyLoan.created_by=user_details.id;
	}
	var loanfinancePage = JSON.stringify($state.params.obj);
	if(loanfinancePage != "null"){
	loanfinancePage = JSON.parse(loanfinancePage);			
	 localStorage.setItem("loanfinancePage", loanfinancePage);	 
     $scope.applyLoan.total_payment = loanfinancePage.rr_price;
     $scope.applyLoan.loan_tenure = loanfinancePage.loan_tenure;
     $scope.applyLoan.down_payment = loanfinancePage.down_payment_amount;
     $scope.applyLoan.interest = loanfinancePage.interest_amount;
     $scope.applyLoan.financial_product = loanfinancePage.financial_product_code;
     $scope.applyLoan.product_name = loanfinancePage.financial_product_name;
     $scope.applyLoan.bank_name = loanfinancePage.financial_institution_name;
     $scope.applyLoan.car_model = loanfinancePage.modal_Code;
     $scope.applyLoan.car_name = loanfinancePage.model_name;
     $scope.applyLoan.car_series = loanfinancePage.series_code;
     $scope.applyLoan.financial_institutions = loanfinancePage.financial_Institution_code;     
	}
	$scope.getAllCityProvinceByDealer = function () {
		
		homeService.getDealerCityProvinceNames(function (response){
			if(response.status.status==200){
				$scope.provinces_cities = response.entities;				
				var prv= $scope.provinces_cities;				
				$scope.provinces = [];
				$scope.filteredProvinces = [];				
				for (var i = 0; i < prv.length; i++) {					
					if($scope.filteredProvinces.indexOf(prv[i].province_code) < 0) {						
						$scope.provinces.push(prv[i]);
						$scope.filteredProvinces.push(prv[i].province_code);
					}
				}								
				
				//$scope.applyLoan.province_code = $scope.provinces[0].province_code;
				if($scope.applyLoan.province_code){
					$scope.changeCityList();
				}
				
			}
		}); 
	};	
	$scope.getAllCityProvinceByDealer();
	
	$scope.changeCityList = function() {			
		$scope.populateCityForSelectedProvince();
		if($scope.cities){			
			$scope.applyLoan.dealer_city = $scope.cities[0].city_name; // assigning the first city as default			
			$scope.changeDealerList();			
		}		
	};
	
	
	$scope.populateCityForSelectedProvince = function() {		
		var allCities = $scope.provinces_cities;		
		$scope.cities = [];	
		$scope.filterCityCodes = [];
		for (var i = 0; i < allCities.length; i++) {
			if (allCities[i].province_name === $scope.applyLoan.dealer_province) {				
				if($scope.filterCityCodes.indexOf(allCities[i].city_code) < 0){	
					$scope.filterCityCodes.push(allCities[i].city_code);
					$scope.cities.push(allCities[i]);
				}			
			}
		}		
	};
	
	$scope.changeDealerList = function (){
		$scope.dealers = [];		
		if($scope.provinces_cities.length > 0){			
			for ( var i = 0 ; i < $scope.provinces_cities.length ; i++ ){				
				if($scope.provinces_cities[i].city_name == $scope.applyLoan.dealer_city){					
					$scope.dealers.push($scope.provinces_cities[i]);					
				}
			}			
			$scope.applyLoan.dealer = $scope.dealers[0].id;				
		}
	};
	
	
	$('.select-box select').chosen({
		disable_search:true,
		width: '100%'
	});
	
	function isPhoneNum(val){
		var phonenum = val;
		var myreg = /^(((1[0-9]{0})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{10})$/; 
		/*var myreg = /^[0-9]*$/;*/
		if(!myreg.test(phonenum)){ 
			return false; 
		}else{
			return true;
		}
	}
	
	$scope.submitApplyLoan = function (){
		var objArr = [];
		$('p.warning').each(function(index, el) {
			objArr[index] = $(this).parents('.select-box').find('select');
			if(index==3 || index==5) {
				objArr[index] = $(this).prev('input');

			}

			if(objArr[index].val()=='') {
				$(this).parent('div').addClass('warning');
			}
			else{
				if(objArr[index].attr('type')=='tel'){
					if(!isPhoneNum(objArr[index].val())){
						$(this).parent('div').addClass('warning');
						return;
					}
				}
				$(this).parent('div').removeClass('warning');
			}
		});		
		if(!$scope.applyLoan.dealer){
	    	$('#dealer').parents('.line').addClass('warning');
	    } else {
	    	$('#dealer').parents('.line').removeClass('warning');
	    }		
		
		if(!$scope.applyLoan.dealer_province){
	    	$('#province_name').parents('.select-box').addClass('warning');
	    } else {
	    	$('#province_name').parents('.select-box').removeClass('warning');
	    }
		
		if(!$scope.applyLoan.dealer_city){
	    	$('#city_name').parents('.select-box').addClass('warning');
	    } else {
	    	$('#city_name').parents('.select-box').removeClass('warning');
	    }
		
		if(!$scope.applyLoan.expected_purchase_time){
	    	$('#expected_purchase_time').parents('.line').addClass('warning');
	    } else {
	    	$('#expected_purchase_time').parents('.line').removeClass('warning');
	    }
		
		if(!$scope.applyLoan.title){
	    	$('#title').parents('.select-box').addClass('warning');
	    } else {
	    	$('#title').parents('.select-box').removeClass('warning');
	    }


		if($('div.warning').length==0){
			financeService.applyloan( $scope.applyLoan , function(response){				
			if(response.data.status.status == 200){		
				$scope.applyLoan={
						name:"",
						title:"",
						dealer_province:"",
						dealer_city:"",
						dealer:"",
						phone:"",
						expected_purchase_time:"",
						total_payment : loanfinancePage.rr_price,
			     loan_tenure : loanfinancePage.loan_tenure,
			     down_payment : loanfinancePage.down_payment_amount,
			     interest : loanfinancePage.interest_amount,
			     financial_product : loanfinancePage.financial_product_code,
			     product_name : loanfinancePage.financial_product_name,
			     bank_name : loanfinancePage.financial_institution_name,
			     car_model : loanfinancePage.modal_Code,
			     car_name : loanfinancePage.model_name,
			     car_series : loanfinancePage.series_code,
			     financial_institutions : loanfinancePage.financial_Institution_code    
				}
			$('#reg-ok').popup({
				content: '申请成功!'
	        });
			}
			});
		}
	};
	
	/*$('a.submit').on('click', function(event) {
		event.preventDefault();
		var objArr = [];
		$('p.warning').each(function(index, el) {
			objArr[index] = $(this).parents('.select-box').find('select');
			if(index==3 || index==5) {
				objArr[index] = $(this).prev('input');

			}

			if(objArr[index].val()=='') {
				$(this).parent('div').addClass('warning');
			}
			else{
				if(objArr[index].attr('type')=='tel'){
					if(!isPhoneNum(objArr[index].val())){
						$(this).parent('div').addClass('warning');
						return;
					}
				}
				$(this).parent('div').removeClass('warning');
			}
		});


		if($('div.warning').length==0){
			$('#reg-ok').popup({
				content: '申请成功!'
	        });	
		}
	});*/
}]);











