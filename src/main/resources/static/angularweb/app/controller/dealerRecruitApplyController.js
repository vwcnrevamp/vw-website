angular.module('vwApp').controller('dealerRecruitApplyController',
		['$scope','localizationService','dealerRecruitService','homeService','$window','apiFactory','$timeout','$state',function($scope, localizationService,dealerRecruitService,homeService,$window,apiFactory,$timeout,$state) {
	
	$scope.province ="";	
	$scope.city="";
	$scope.checkbox= false;
	$scope.default_field=true;
	$scope.is_edit = false;
	$scope.is_submit=false;
	$scope.recruit_response = {};
	$scope.recruit = {
		selected_province:"",
		selected_city:"",
		selected_district:"",
		recruitment_type:"",		
		declared_city:"",		
		created_by:""
	};
	$scope.temp_province = [] ;
	$scope.provinces =[];
	$scope.translate = function(){	
		localizationService.getBundle(function(data){
			$scope.translation = data;
		});			   	 
	};
	$scope.translate();
	
	$scope.logoutClicked = function() {
		homeService.logoutUser();		
	};
	
	$scope.recruitmentHomeUrl = apiFactory.getRecruitmentHomeUrl();
	$scope.recruitDetailsPageUrl = apiFactory.getRecruitDetailsPageUrl();
	$scope.recruitApplyPageUrl = apiFactory.getRecruitApplyUrl();
	
	$scope.recruitmentType = apiFactory.getRecruitmentType();	
	
	$scope.setRecruitmentType = function (val) {		
		apiFactory.setRecruitmentType(val);
	};
	
	$scope.getNavClass = function(val){
		var navClass = "";
		if ($scope.recruitmentType == val) {
			navClass = "active";
		}
		return navClass;		
	};
	
	$scope.myRecordClicked = function(){
		$state.go('recruitRecord');
	};
	
	$scope.tab1=false;
	$scope.tab2=false;
	$scope.tab3=false;
	$scope.id=0;
	
	$scope.goToTab = function (n){			
		if(n==1){			
			$scope.tab1=true;
			$scope.tab2=false;
			$scope.tab3=false;			
		} else if(n==2){			
			$('.setp-1').hide().next('.setp-2').show();	
			/*$('select').chosen({
				disable_search:true,
				width:'100%'
			});*/
			$scope.tab1=false;
			$scope.tab2=true;
			$scope.tab3=false;			
		} else if(n==3) {			
			$('.setp-2').hide().next('.setp-3').show();	
			$scope.tab1=false;
			$scope.tab2=false;
			$scope.tab3=true;	
		}		
	};
	
	$scope.getRecruitmentdetailsById = function(){			
		$scope.id=apiFactory.getRecruitmentApplicationId().toString();		
		dealerRecruitService.getRecruitmentById($scope.id,function (response){
			if(response.status.status == 200){			
				$scope.recruit = response.entity;
				$scope.is_edit = true;
				$scope.recruit_response = $scope.recruit;			
				if($scope.recruit.form_status =="SUBMITTED"){
					$scope.is_submit = true;
				} else {
					$scope.is_submit = false;
				}
				apiFactory.setRecruitmentApplicationId("");
				
				if(typeof($scope.recruit_response) != 'undefined' &&				
						typeof($scope.recruit_response.province_code) != 'undefined'){					
					$scope.recruit.province_code = $scope.recruit_response.province_code;		
					console.log("Province from db ==="+  $scope.recruit.province_code);
				}
				if(typeof($scope.recruit_response) != 'undefined' &&
						typeof($scope.recruit_response.city_code) != 'undefined'){			
					$scope.recruit.city_code = $scope.recruit_response.city_code; // setting the city_code from the db response
					console.log("City from db ==="+  $scope.recruit.city_code);
				}
				if(typeof($scope.recruit_response) != 'undefined' &&
						typeof($scope.recruit_response.district_code) != 'undefined'){			
					$scope.recruit.district_code = $scope.recruit_response.district_code; // setting the district from the db response
					console.log("district from db ==="+  $scope.recruit.district_code);	
				}
			}			
		});
	};
	
	$scope.getAllProvincesAndCities = function () {		
		homeService.getAllprovincesAndcities(function (response){
			if(response.status.status==200){
				$scope.provinces_cities = response.entities;				
				var prv= $scope.provinces_cities;
				$scope.provinces = [];
				$scope.filteredProvinces = [];				
				for (var i = 0; i < prv.length; i++) {					
					if($scope.filteredProvinces.indexOf(prv[i].province_code) < 0) {						
						$scope.provinces.push(prv[i]);
						$scope.filteredProvinces.push(prv[i].province_code);
					}
				}				
				if($scope.provinces && $scope.provinces.length > 0){
					if(!$scope.recruit){
						$scope.recruit = {};
					}
					if(!$scope.recruit.province_code){
						$scope.recruit.province_code = $scope.provinces[0].province_code;
					}					
											
					if($scope.recruit.province_code){
						$scope.populateCityForSelectedProvince();			
					}
				}
			}
		}); 
	};
	
	$scope.changeCityList = function() {
		$scope.recruit.city_code = null;
		$scope.populateCityForSelectedProvince();				
	};
	
	$scope.populateCityForSelectedProvince = function() {		
		var allCities = $scope.provinces_cities;		
		$scope.cities = [];	
		$scope.filterCityCodes = [];
		for (var i = 0; i < allCities.length; i++) {
			if (allCities[i].province_code === $scope.recruit.province_code) {				
				if($scope.filterCityCodes.indexOf(allCities[i].city_code) < 0){	
					$scope.filterCityCodes.push(allCities[i].city_code);
					$scope.cities.push(allCities[i]);
				}			
			}
		}
		if($scope.cities){		
			if(!$scope.recruit.city_code){
				$scope.recruit.city_code = $scope.cities[0].city_code; // assigning the first city as default
			}
			if($scope.recruit.city_code){
				$scope.getDistrictListForSelectedCity();
			}
		}
	};
	
	$scope.changeDistrictList = function(){	
		$scope.recruit.district_code = null;
		$scope.getDistrictListForSelectedCity();
	};
	
	$scope.getDistrictListForSelectedCity = function(){		
		if($scope.recruit && $scope.recruit.city_code){
			homeService.getDistrictByCity($scope.recruit.city_code, function (response){
				if(response.status.status==200){
					$scope.districts = response.entities;		
					console.log("district data=");	
					console.log($scope.districts);	
				}
			}); 
		}
		if($scope.districts){	
			if(!$scope.recruit.district_code){
				$scope.recruit.district_code = $scope.districts[0].code; // assigning the first district as default
			}
		}
		console.log("district code=" + $scope.recruit.district_code);
		
/*		var allDistricts = $scope.provinces_cities;
		$scope.districts = [];	
		$scope.filteredDistrictCodes = [];
		for (var i = 0; i < allDistricts.length; i++) {
			if (allDistricts[i].city_code === $scope.recruit.city_code) {	
				if($scope.filteredDistrictCodes.indexOf(allDistricts[i].district_code) < 0){	
					$scope.filteredDistrictCodes = [allDistricts[i].district_code];
					$scope.districts.push(allDistricts[i]);
				}			
			}
		}
		if($scope.districts){	
			if(typeof($scope.recruit_response) != 'undefined' &&
					typeof($scope.recruit_response.district_code) != 'undefined'){			
				$scope.recruit.district_code = $scope.recruit_response.district_code; // setting the district from the db response
					
			} else {
				$scope.recruit.district_code = $scope.cities[0].district_code; // assigning the first district as default
			}
		}*/
					
	};
	
	$scope.getallSourceSites = function(){
		dealerRecruitService.getallSourceSites(function (response){
			if(response.status.status==200){
				$scope.source_site = response.entities;	
			}
		}); 
	};
	
	$scope.loadDropDown = function (){
		$scope.getallSourceSites();
		$scope.getAllProvincesAndCities();		
	};
	
	$scope.initialization = function (){		
		if(apiFactory.getRecruitmentApplicationId()){
			$scope.getRecruitmentdetailsById();			
			$timeout(function() {
			$scope.goToTab(2);
			},300);			
		} else {	
			$scope.recruit = homeService.getSelectedRecruitingCity();
			if($scope.recruit) {
				$scope.recruit.created_by = homeService.getCurrentUser().id;
			}			
			$scope.goToTab(1);
		}
		$scope.loadDropDown();
	};	
	$scope.initialization();		
	
		
	$scope.submitRecruitApply = function (type){		
		var objArr = [];		
		if($scope.is_edit){
			if(type == 'submit'){
				$('.setp-2').find('p.warning').each(function(index, el) {
					objArr[index] = $(this).prev('input');
					if(index==12 || index==5 || index==6 || index==7) {
						objArr[index] = $(this).parents('.select-box').find('select');
					}
					if(objArr[index].val()=='') {
						$(this).parent('div').addClass('warning');
					}
					else{
						$(this).parent('div').removeClass('warning');
					}

					if(index==3){
						if(objArr[index].val()=='') return;
						if(!checkTel(objArr[index].val())){
							$(this).parent('div').addClass('warning').find('p.warning').text('电话格式不正确');
							
						}else{
							$(this).parent('div').removeClass('warning');
						}	
					}
					if(index==4){
						if(objArr[index].val()=='') return;
						if(!chkEmail(objArr[index].val())){
							$(this).parent('div').addClass('warning').find('p.warning').text('邮箱格式不正确');
							
						}else{
							$(this).parent('div').removeClass('warning');
						}	
					}
					
				});
				if(!$scope.recruit.main_site_source){				
			    	$('#main_site_source').parents('.select-box').addClass('warning');
			    } else {
			    	$('#main_site_source').parents('.select-box').removeClass('warning');
			    }
				if($('div.warning').length==0){	
					$scope.recruit.id=$scope.id; 
					$scope.recruit.form_status ="SUBMITTED";
					dealerRecruitService.updateDealerRecruitment($scope.recruit, function(response){					
						if(response.data.status.status==200){						
							$scope.recruit = response.data.entity;
							$scope.is_submit=true;
							$('div.setp').find('li').eq(2).addClass('active').siblings('li').removeClass('active');
							$('.setp-2').hide().next('.setp-3').show();	
						}
					});			
				}
			} else {			
					$scope.recruit.id=$scope.id; 
					$scope.recruit.form_status ="SAVED";
					dealerRecruitService.updateDealerRecruitment($scope.recruit, function(data){
						if(data.status.status==200){
							$scope.is_submit=false;
							angular.element('#save-ok').popup({
								content:'<h3>保存成功</h3>\
					                <p>您可以至[我的申请记录]中查看/编辑申请信息</p>\
					                <a class="btn-back">继续编辑</a>\
					                <a class="btn-record">我的申请记录</a>'
							});
							angular.element('.popup a.btn-back').on('click', function(event) {				
								$(this).parents('.popup').hide();
							});	
							angular.element('.popup a.btn-record').on('click', function(event) {				
								$(this).parents('.popup').hide();
								/*$('div.setp').find('li').eq(2).addClass('active').siblings('li').removeClass('active');
								$('.setp-2').hide().next('.setp-3').show();	*/
								$state.go("recruitRecord");
							});
						}						
					});	
						
				
			}		
		} else {		
		if(type == 'submit'){			
			$('.setp-2').find('p.warning').each(function(index, el) {
				objArr[index] = $(this).prev('input');
				if(index==12 || index==5 || index==6 || index==7) {
					objArr[index] = $(this).parents('.select-box').find('select');
				}
				if(objArr[index].val()=='') {
					$(this).parent('div').addClass('warning');
				}
				else{
					$(this).parent('div').removeClass('warning');
				}

				if(index==3){
					if(objArr[index].val()=='') return;
					if(!checkTel(objArr[index].val())){
						$(this).parent('div').addClass('warning').find('p.warning').text('电话格式不正确');
						
					}else{
						$(this).parent('div').removeClass('warning');
					}	
				}
				if(index==4){
					if(objArr[index].val()=='') return;
					if(!chkEmail(objArr[index].val())){
						$(this).parent('div').addClass('warning').find('p.warning').text('邮箱格式不正确');
						
					}else{
						$(this).parent('div').removeClass('warning');
					}	
				}
				
			});
			if(!$scope.recruit.main_site_source){				
		    	$('#main_site_source').parents('.select-box').addClass('warning');
		    } else {
		    	$('#main_site_source').parents('.select-box').removeClass('warning');
		    }
			if($('div.warning').length==0){	
				$scope.recruit.form_status ="SUBMITTED";
				dealerRecruitService.addDealerRecruitment($scope.recruit, function(response){					
					if(response.data.status.status==200){						
						$scope.recruit = response.data.entity;
						$scope.is_submit=true;
						$('div.setp').find('li').eq(2).addClass('active').siblings('li').removeClass('active');
						$('.setp-2').hide().next('.setp-3').show();	
					}
				});			
			}
		} else {			
				$scope.recruit.form_status ="SAVED";
				dealerRecruitService.addDealerRecruitment($scope.recruit, function(data){					
					if(data.status.status==200){
						$scope.is_submit=false;						
					}	
					angular.element('#save-ok').popup({
						content:'<h3>保存成功</h3>\
			                <p>您可以至[我的申请记录]中查看/编辑申请信息</p>\
			                <a class="btn-back">继续编辑</a>\
			                <a class="btn-record">我的申请记录</a>'
					});
					angular.element('.popup a.btn-back').on('click', function(event) {				
						$(this).parents('.popup').hide();
					});	
					angular.element('.popup a.btn-record').on('click', function(event) {				
						$(this).parents('.popup').hide();
						/*$('div.setp').find('li').eq(2).addClass('active').siblings('li').removeClass('active');
						$('.setp-2').hide().next('.setp-3').show();	*/
						$state.go("recruitRecord");				
					});
				});	
					
			
		}
	}
	};
	

	
	 /*$('a.btn-save').on('click', function(event) {
			event.preventDefault();
			//$('#save-ok').show();
			$('#save-ok').popup({
				content:'<h3>保存成功</h3>\
	                <p>您可以至[我的申请记录]中查看/编辑申请信息</p>\
	                <a href="#" class="btn-back">继续编辑</a>\
	                <a href="#" class="btn-record">我的申请记录</a>'
			});

			$('.popup a.btn-back').on('click', function(event) {
				event.preventDefault();
				$(this).parents('.popup').hide();
			});	
		});
		
		var $setp = $('div.setp');
		$('.recruit_apply a.btn-agree').on('click', function(event) {
			event.preventDefault();
			$setp.find('li').eq(1).addClass('active').siblings('li').removeClass('active');
			$('.setp-1').hide().next('.setp-2').show();
			$('select').chosen({
				disable_search:true,
				width:'100%'
			});
		});

		
		$('.recruit_apply a.btn-submit').on('click', function(event) {
			event.preventDefault();

			var objArr = [];

			$('.setp-2').find('p.warning').each(function(index, el) {
				objArr[index] = $(this).prev('input');
				if(index==12 || index==5 || index==6 || index==7) {
					objArr[index] = $(this).parents('.select-box').find('select');
				}
				if(objArr[index].val()=='') {
					$(this).parent('div').addClass('warning');
				}
				else{
					$(this).parent('div').removeClass('warning');
				}

				if(index==3){
					if(objArr[index].val()=='') return;
					if(!checkTel(objArr[index].val())){
						$(this).parent('div').addClass('warning').find('p.warning').text('电话格式不正确');
						
					}else{
						$(this).parent('div').removeClass('warning');
					}	
				}
				if(index==4){
					if(objArr[index].val()=='') return;
					if(!chkEmail(objArr[index].val())){
						$(this).parent('div').addClass('warning').find('p.warning').text('邮箱格式不正确');
						
					}else{
						$(this).parent('div').removeClass('warning');
					}	
				}
				
			});
			if($('div.warning').length==0){
				$setp.find('li').eq(2).addClass('active').siblings('li').removeClass('active');
				$('.setp-2').hide().next('.setp-3').show();	
			}

			
		});*/


}]);









