angular.module('vwApp').controller('testDriveMobileController',['$http','$scope','$cookies','$rootScope','$location','$filter','$window','$localStorage','$state','$cookieStore','$timeout','$geolocation','homeService','testDriveService','localizationService',function($http,$scope,$cookies,$rootScope,$location,$filter,$window,$localStorage, $state,$cookieStore,$timeout,$geolocation,homeService,testDriveService,localizationService) {
	maps = [];
	$scope.testdrive = {};
	$scope.province ="";	
	var BASEDATA = [];
	var index =0;
	var dealerdata =[];
	var bdata=[];
	$scope.cit="";
	$scope.dealer_id = "";
	$scope.selctGender = "";
	$scope.maps =[];
	$scope.provinces =[];
	$scope.city = {city_code:'500002', city_name: 'Beijing', longitude:'116.404', latitude: '39.915', province_code: '10001', province_name: 'Beijing'}; // get the default city details using API
	$scope.province = {province_code: '10001', province_name: 'Beijing', city_code:'500002', city_name: 'Beijing', longitude:'116.404', latitude: '39.915'} ;// default province
	$scope.temp_province = [] ;
	$scope.final_province = [];
	$scope.cities =[];
	$scope.dealerTypes =[];
	$scope.carSeries = [];
	$scope.selectedmodel ="";
	$scope.privacyPolicyAccept =true;
	var latitude = 116.404;
	 var longitude = 39.915;
	$scope.class="clause radio selected";
	$scope.translate = function(){	
		localizationService.getBundle(function(data){
			$scope.translation = data;
		});			   	 
	};
	//$scope.translate();
	/*Baidu Map*/

	$scope.offlineOpts = {
			retryInterval: 10000,
			txt: 'Offline Mode'
	};

	function showlocation(callback , errorCallback) {
		// One-shot position request.
		//navigator.geolocation.getCurrentPosition(callback);
		navigator.geolocation.getCurrentPosition(callback,
			    errorCallback);
	}	
	$('select').chosen({
		disable_search:true,
	});
	/*$scope.setMapOptions = function () {		 
		var city = $scope.city;
		console.log("Selected city>>>>  ");
		console.log(city);
		$scope.mapOptions =  {
				center: {
					longitude: city.longitude,
					latitude: city.latitude
				},
				zoom: 5,
				city: city.city_name,
				markers: [{
					longitude: city.longitude,
					latitude: city.latitude,
					icon: 'angularweb/assets/images/icon01.jpg',
					title: 'Where',
					content: 'Put description here'
				}]
		};
	};
	$scope.setMapOptions();*/
	$scope.carSeries = function()
	{
		testDriveService.getCarSeries(function (response){
			if(response.status.status==200){
				$scope.carSeries = response.entities;
				$scope.searchResult = dealerdata;
				$scope.searchResultCount = dealerdata.length;				
				//$('#car-model').trigger("chosen:updated");
				if($state.params && $state.params.series != null){
					$scope.testdrive.series_code = $state.params.series.series_code;
				}
			}
		});  
	};
	//$scope.carSeries();
	
	$('a#btn-map').on('click',function(event) {
		event.preventDefault();
		
		var city = $("#city").val();
		if(city){
			//$scope.mapLoaded(maps);			
				$('.testdrive-mobile-form').hide();
				$('#popup-map').show();								
		}else{
			alert('请选择城市');
		}
		
	});
	
	$('#popup-map').on('click','.popup-map-top a', function(event) {
		event.preventDefault();
		$('.testdrive-mobile-form').show();
		$('#popup-map').hide();
	});
	
	$scope.getAllcityprovince = function () {
		
		homeService.getAllprovincesAndcities(function (response){
			if(response.status.status==200){
				$scope.provinces_cities = response.entities;
				//alert(angular.toJson($scope.provinces_cities));
				var prv= $scope.provinces_cities;
				//$scope.searchResult = dealerdata;
				//$scope.searchResultCount = dealerdata.length;
				for (var j = 0; j < prv.length; j++) {
					if($scope.temp_province.indexOf(prv[j].province_name) < 0) {
						$scope.temp_province.push(prv[j].province_name);
						$scope.provinces.push(prv[j]);
					}
				}			
					$scope.testdrive.province_code = $scope.provinces[0].province_code;					
				if($scope.testdrive.province_code){
					$scope.changeCityList();
				}
			}
		}); 
	};
	//$scope.getAllcityprovince();

	$scope.dealerInfo = function() {
		//load all the cities and provinces
		$scope.translate();
		$scope.getAllcityprovince();
		$scope.carSeries();
		// detect user location and set default city and province and		
		var setUserLocation = function (position){			
			// get user city using Baidu API using the coordinates
			var cityName = null;
			if(position){				
				homeService.getExternal(position.coords.latitude, position.coords.longitude, function (response){
					if(response && response.status == 0){
						if(response.result.addressComponent){
							cityName = response.result.addressComponent.city;
							
						}
						
					}
				});
			}
			if(null != cityName) {
				cityName = 'Beijing';
			}			
			$scope.setCity(cityName);
	
		};
		var setDefaultLocation = function (error){			
			var defaultcity = 'Beijing'; //
			if(localStorage.getItem("city_name") != "undefined" && localStorage.getItem("city_name") != ""){				
				$scope.setCity(localStorage.getItem("city_name"));
			} else {		
				$scope.setCity(defaultcity);// get the default city details using API
			}						
		};
		
		showlocation(setUserLocation, setDefaultLocation);	
		//$scope.setMapOptions();
		
	};
	$scope.setCity = function (cityName){		
		homeService.getCityByName(cityName, function (response){				
			if(response.status.status == 200){
				var cityObj = response.entity;
				console.log(response);
				console.log(cityObj);
				$scope.setSelectedCityProvince(cityObj.code);
			}
		});		
	};
	
	$scope.mapOptions =  {
			center: {
				longitude: longitude,
				latitude: latitude
			},
			zoom: 12,
			city: 'Beijing',
			markers: [{
				longitude: longitude,
				latitude: latitude,
				icon: 'angularweb/assets/images/icon01.jpg',
				title: 'Where',
				content: 'Put description here'
			}]
	};
	
	$scope.setSelectedCityProvince = function (city_code) {		
		var allCities = $scope.provinces_cities;
		
		for (var i = 0; i < allCities.length; i++) {
			if (allCities[i].city_code === city_code) {				
					$scope.testdrive.city_code = allCities[i].city_code; // assigning the first city as default							
				$scope.city = allCities[i];
				break;
			}
		}
		var selCity = $scope.city;		
		$scope.searchDealer();
		$scope.setSelectedProvince(selCity.province_code);
	};
	
	$scope.setSelectedProvince = function (province_code) {
		var uniqueProvinces = $scope.provinces;
		
		for (var i = 0; i < uniqueProvinces.length; i++) {
			if (uniqueProvinces[i].province_code === province_code) {
				$scope.testdrive.province_code = uniqueProvinces[i].province_code;
				$scope.province = uniqueProvinces[i];
				break;
			}
		}
		
		$scope.populateCityForSelectedProvince();
	};	
	
	$scope.changeCityList = function() {		
		$scope.populateCityForSelectedProvince();
		if($scope.cities){
			$scope.city = $scope.cities[0];			
			$scope.testdrive.city_code = $scope.cities[0].city_code; // assigning the first city as default			
			//$scope.mapLoaded(maps);
			//$scope.searchDealer();			
		}		
	};
	
	
	$scope.populateCityForSelectedProvince = function() {		
		var allCities = $scope.provinces_cities;
		console.log("Selected Province>>>>  ");
		console.log($scope.testdrive.province_code);
		$scope.cities = [];
		homeService.getDealerByProvince($scope.testdrive.province_code,function (response){
			if(response.status.status==200){
				dealerdata = response.entities;
			}
		});
		for (var i = 0; i < allCities.length; i++) {
			if (allCities[i].province_code === $scope.testdrive.province_code) {
				console.log("Adding  city >>>>  " + i);
				console.log(allCities[i]);
				$scope.cities.push(allCities[i]);
			}
		}
	};
	
	$scope.searchDealer = function (){		
		homeService.getdealerCityProvincefilter($scope.testdrive.city_code, function (response){
			if(response.status.status==200){
				dealerdata = response.entities;				
				$scope.dealers =dealerdata;							
				if($scope.dealers){					
					if(localStorage.getItem("dealer_id") != "undefined" && localStorage.getItem("dealer_id") != ""){						
						for (var i = 0; i < $scope.dealers.length; i++) {							
							if($scope.dealers[i].id == localStorage.getItem("dealer_id")){								
								$scope.testdrive.dealer_id = $scope.dealers[i].id;	
								break;
							}							
						}
						if(dealerdata[dealerdata.length - 1].id  != localStorage.getItem("dealer_id")){
							$scope.testdrive.dealer_id = dealerdata[0].id;
						}
					} else {
					$scope.testdrive.dealer_id = $scope.dealers[0].id;		
					}
					}else{
						$scope.testdrive.dealer_id="";	
					}
				$scope.searchResult = dealerdata;
				if($scope.cities){					
					for ( var j = 0; j < $scope.cities.length ; j++){						
						if($scope.cities[j].city_code == $scope.testdrive.city_code){
							$scope.city = $scope.cities[j];							
							break;
						}						
					}
				}				
					$scope.mapLoaded(maps);	
				
											
			}
			else
			{
				$scope.mapLoaded(maps);
			}
		}); 
	};	
/*	$('#province').bind('change', function() 
			{
		$scope.cities=[];
		var data = "<option value=''></option>";
		var province = $(this).find('option:checked').val();
		var filteredprv = $scope.provinces;
		homeService.getDealerByProvince(province,function (response){
			if(response.status.status==200){
				dealerdata = response.entities;
			}
		});
		for (var i = 0; i < filteredprv.length; i++) {

			if (filteredprv[i].province_code === province) {
				$scope.cities.push(filteredprv[i]);
			}
		}
		for (var i = 0; i < $scope.cities.length; i++) {
			data = data + "<option value='"+$scope.cities[i].city_code+"'>"+$scope.cities[i].city_name+"</option>"
		}
		$('#search_city').chosen({
			disable_search:true,
		});
		$('#city').html(data);
		$('#city').trigger("chosen:updated");

			}) ;*/
	
/*	$('#city').bind('change', function() 
			{
		var data = "<option value=''></option>";
		$scope.dealerTypes= [];
		var selectcity = $(this).find('option:checked').val();
		$scope.cit = selectcity;
		homeService.getdealerCityProvincefilter(selectcity, function (response){
			if(response.status.status==200){
				dealerdata = response.entities;
				$scope.dealers =dealerdata;
				for (var i = 0; i < dealerdata.length; i++) {
					if($scope.dealerTypes.indexOf(dealerdata[i].id) < 0) {
						$scope.dealerTypes.push(dealerdata[i].id);
						data = data + "<option value='"+dealerdata[i].id+"'>"+dealerdata[i].full_name+"</option>"
					}
				}
				$('#dealer').html(data);
				$('#dealer').trigger("chosen:updated");
				$scope.searchResult = dealerdata;
				$scope.type = response.entities;
				$scope.searchResultCount = $scope.searchResult.length;
				$scope.mapLoaded(maps);
			}
			else
			{
				$scope.mapLoaded(maps);
			}
		});   
			});*/
	/*$('#dealer').bind('change', function()
			{
		var selectcity = $(this).find('option:checked').val();
		$scope.dealer_id = selectcity;
			});*/
	//$scope.dealerInfo();
	//dealerData = $scope.dealers ;

	/*$scope.mapOptions =  {
			center: {
				longitude: longitude,
				latitude: latitude
			},
			zoom: 12,
			city: 'Beijing',
			markers: [{
				longitude: longitude,
				latitude: latitude,
				icon: 'angularweb/assets/images/icon01.jpg',
				title: 'Where',
				content: 'Put description here'
			}]
	};*/
	//$scope.dealerInfo();
	$scope.mapLoaded = function(map) {		
		maps = map;
		var city = $scope.city;
		var marker = function (position){
		map.centerAndZoom(new BMap.Point(position.coords.latitude,position.coords.longitude),10);
			//map.centerAndZoom(new BMap.Point(position.coords.longitude,position.coords.latitude),12);
			//map.centerAndZoom(new BMap.Point(116.404, 39.915));
			map.enableScrollWheelZoom();  
			map.addControl(new BMap.NavigationControl()); 
			map.clearOverlays();  
			addMarker(map);
		};
		var defaultMarker = function (error){
			//	map.centerAndZoom(new BMap.Point(position.coords.latitude,position.coords.longitude),5);  
			map.centerAndZoom(new BMap.Point(latitude,longitude),10);			
			map.enableScrollWheelZoom();  
			map.addControl(new BMap.NavigationControl()); 
			map.clearOverlays();  
			addMarker(map);
		};		
		showlocation(marker, defaultMarker);

	};

	/*$scope.mapLoaded = function(map) {		
		maps = map;
		$scope.dealerInfo();
	};*/

	/*$scope.reLoadMap = function(map) {		
		var city = $scope.city;
		console.log("In mapLoaded=======");
		console.log(city);
		if(city && city.latitude && city.longitude){
			console.log("In if=======");
			map.centerAndZoom(new BMap.Point(city.latitude, city.longitude), 10);
			//map.centerAndZoom(city.city_name, 10);
		} else {
			//
			console.log("In else=======");
			map.centerAndZoom("Beijing", 10);
		}
		
		//map.centerAndZoom("香港特别行政区", 10);
		//map.centerAndZoom("Hongkong", 10);
		map.enableScrollWheelZoom();  
		map.addControl(new BMap.NavigationControl()); 
		map.clearOverlays();		
		addMarker(map);

	};*/
	//adding marker to map
	//adding marker to map
	function addMarker(map){
		for (var i = 0; i <dealerdata.length; i++) {
			if(dealerdata[i].id == $scope.testdrive.dealer_id){
			var json = dealerdata[i];
			var p0 = json.lattitude;
			var p1 = json.longitude;
			var point = new BMap.Point(p1, p0);
			var myIcon = new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25), {
				offset: new BMap.Size(10, 25),
				imageOffset: new BMap.Size(0, 0 - i * 25)
			});
			var marker = new BMap.Marker(point, { icon: myIcon });
			var iw = createInfoWindow(i);
			var label = new BMap.Label(json.full_name, { "offset": new BMap.Size("http://static.blog.csdn.net/images/medal/holdon_s.gif".lb - "http://static.blog.csdn.net/images/medal/holdon_s.gif".x + 10, -20) });
			marker.setLabel(label);
			map.addOverlay(marker);
			label.setStyle({
				color : "#f00", 
				fontSize : "10px", 
				backgroundColor :"0.05",
				border :"0", 
				fontWeight :"bold" 
			});
			(function() {
				var index = i;
				var _iw = createInfoWindow(i);
				var _marker = marker;
				_marker.addEventListener("click", function() {
					this.openInfoWindow(_iw);
				});
				_iw.addEventListener("open", function() {
					_marker.getLabel().hide();
				})
				_iw.addEventListener("close", function() {
					_marker.getLabel().show();
				})
				label.addEventListener("click", function() {
					_marker.openInfoWindow(_iw);
				})
				if (!!json.isOpen) {
					label.hide();
					_marker.openInfoWindow(_iw);
				}
			})()
			}
		}
	}
	//Create a marker popup window
	function createInfoWindow(i) {
		var json = dealerdata[i];
		var iw = new BMap.InfoWindow("<p style='font-size:18px;'>"+(i+1)+"."+dealerdata[i]['full_name']+"</p>" +
				"<p style='font-size:16px;height:28px;line-height:28px;'>"+dealerdata[i]['address']+"</p>"+
				"<div style='border-top:1px solid #dee1e3;margin-top:15px;padding-top:24px;overflow: hidden;'><div width = '60%'; style='float:left;border-right:1px solid #dee1e3;padding-right:20px;'><p style='height:28px;line-height:28px;font-size16px;width:160px;'>销售热线:<span style='color:#1597d9;'>"+dealerdata[i]['sales_hotline']+"</span></p><a href='https://localhost:8443/examples/mp.html' style='color:#1597d9;margin-top:30px;display:block;background:url(images/icon-arrow1.png) center left no-repeat;padding-left:24px;'>进入销售商首页</a><a href='#/testDrive' style='color:#ffffff;background-color:#019ada;width:148px;height:42px;border-radius:3px;text-align:center;margin-top:30px;display:block;line-height:42px;font-size:18px;'>预约试驾</a></div><div style='float:left;padding-left:20px;'><img src='angularweb/assets/images/dealer-popup-img.jpg' /><div class='smsDiv' style='margin-top:12px;overflow: hidden;'><input type='text' placeholder='短信获取经销商信息' style='float:left;border:1px solid #dee1e3;background-color:#ffffff;width:229px;height:44px;-webkit-appearance:none;line-height:44px;border-radius:3px;padding:0px 12px;'><a href='#' style='color:#ffffff;background-color:#019ada;width:78px;height:42px;border-radius:3px;text-align:center;display:block;line-height:42px;font-size:18px;float:left;margin-left:10px;'>发送</a></div></div></div>");
		return iw;
	}
	//Create a Iconi
	function createIcon(json) {
		var icon = new BMap.Icon("http://dev.baidu.com/wiki/static/map/API/img/ico-marker.gif", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowAnchor:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})  
		return icon; 
	}



	$(".testdrive-mobile-form span.sex").on('click', function(event) {
		event.preventDefault();
		var kids = $( event.target ).children().context.id;
		$scope.selctGender = kids;
		if($(this).hasClass('selected')) return;
		$(this).addClass('selected').siblings('span.sex').removeClass('selected')
	});
	/*$(".testdrive-mobile-form span.radio").unbind().click(function(){
		if($(this).hasClass('selected')) {
			$(this).removeClass('selected')
		}else{
			$(this).addClass('selected')
		}
	});*/
	$scope.pageAcceptForm = function (bol){		
		if($scope.class === 'clause radio selected'){			
			$scope.class="clause radio ";
			$scope.privacyPolicyAccept =false;
		} else {			
			$scope.class="clause radio selected"
				$scope.privacyPolicyAccept =true;
		}
	};
	$('.line.clause').on('click', 'a', function(event) {
		event.preventDefault();
		$('#popup-clause').show();
	});
	$('#popup-ok').on('click', 'a.btn', function(event) {
		$(this).parents('#popup-ok').hide();
		$('select option[value=""]').prop('selected',true).trigger('chosen:updated');
		$('input').val('');
	});
	$('.popup').on('click', 'a.popup-close', function(event) {
		$('#popup-clause').hide();
	});

	function isPhoneNum(val){
		var phonenum = val;
		var myreg = /^(((1[0-9]{0})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{10})$/; 
		/*var myreg = /^[0-9]*$/;*/
		if(!myreg.test(phonenum)){ 
			return false; 
		}else{
			return true;
		}
	}
	function isAlphabet(namec) {
	    var isValid = false;
	    var regex = /^[a-zA-Z ]+$/;
	    isValid = regex.test(namec);
	    return isValid;
	}
	
	$scope.submitTestDrive = function (data){
		var i = 0;
		for (var selector in data) {
			if(data[selector]==''){
				if(!$('.testdrive-form div.line').eq(i).hasClass('must')) return;
				$('.testdrive-form div.line').eq(i).addClass('warning');
			}else{
				$('.testdrive-form div.line').eq(i).removeClass('warning');
			}
			i++;
	    };
	    if(data.name){
		    if(!isAlphabet(data.name)){
		    	$('#name').parents('.line').addClass('warning');
		    } else {
		    	$('#name').parents('.line').removeClass('warning');
		    }
		    } else {
		    	$('#name').parents('.line').addClass('warning');
		    }
	    if(!isPhoneNum(data.mobile_no) || data.mobile_no.isEmpty){
	    	$('#phone').parents('.line').addClass('warning');
	    }else{
	    	$('#phone').parents('.line').removeClass('warning');
		}
	    if(!data.series_code){	    	
	    	$('#car-model').parents('.line').addClass('warning');
	    } else {
	    	$('#car-model').parents('.line').removeClass('warning');	    	 
	    }
	    if(!data.dealer_id){
	    	$('#dealer').parents('.line').addClass('warning');
	    } else {
	    	$('#dealer').parents('.line').removeClass('warning');
	    }
		var PolicyAccept = $scope.privacyPolicyAccept;		
		if(PolicyAccept)
			{
		if(data.series_code != '' && data.dealer_id != '' && data.name != ''  && data.mobile_no !='' && isPhoneNum(data.mobile_no) && isAlphabet(data.name)){
			$scope.data= $scope.testdrive;
			$scope.data.name = data.name;
			$scope.data.dealer_id = data.dealer_id;
			$scope.data.dealer_city_code = data.city_code;
			$scope.data.dealer_province_code = data.province_code;
			$scope.data.updated_by = 1;
			$scope.data.series_code = data.series_code;
			$scope.data.plan_buy_time = data.plan_buy_time;
			$scope.data.mobile_no = data.mobile_no;
			$scope.data.gender = $scope.selctGender;
			$scope.selectedmodel = $("#car-model option:selected").text();
			$("#series_name").innerHTML=$scope.selectedmodel;
			if($scope.selctGender=="")
			{
				$scope.data.gender = $scope.translation.testDrive.Male;
			}
			testDriveService.registerTestDrive($scope.data,function (response){
				if(response.data.status.status==200)
				{
					$scope.testdrive = {};
					$scope.getAllcityprovince();
					$scope.class="clause radio selected";
					$('#popup-ok').show();
				}
			
			});
		
	    }
		/*var i = 0;
		for (var selector in data) {
			if(data[selector]==''){
				if(!$('.testdrive-form div.line').eq(i).hasClass('must')) return;
				$('.testdrive-form div.line').eq(i).addClass('warning');
			}else{
				$('.testdrive-form div.line').eq(i).removeClass('warning');
			}
			i++;
	    };
	    if(!isPhoneNum(data.mobile_no) || data.mobile_no.isEmpty){
	    	$('#phone').parents('.line').addClass('warning');
	    }
	    if(!isAlphabet(data.name)){
	    	$('#name').parents('.line').addClass('warning');
	    }*/
			}
		else
			{
				//alert("accept terms and conditions");
				alert($scope.translation.testDrive.termsAndCondition);
			}
	
	};

/*	$('button#submit').unbind().click(function(){
		var data = {
			arModel : $('#car-model').val(),
			dealer : $('#dealer').val(),
			name : $('#name').val(),
			sex : $('span.sex').attr('val'),
			province : $('#province').val(),
			city : $('#city').val(),
			phone : $('#phone').val(),
			time : $('#time').val(),
			planByTime : $('#timeofpurchase').val()
		}
		
		if(data.arModel != '' && data.dealer != '' && data.name != ''  && data.phone !='' && isPhoneNum(data.phone) && isAlphabet(data.name)){
			$scope.data= $scope.testdrive;
			$scope.data.name = data.name;
			$scope.data.dealer_id = data.dealer;
			$scope.data.dealer_city_code = data.city;
			$scope.data.dealer_province_code = data.province;
			$scope.data.updated_by = 1;
			$scope.data.series_code = data.arModel;
			$scope.data.plan_buy_time = data.planByTime;
			$scope.data.mobile_no = data.phone;
			$scope.data.gender = $scope.selctGender;
			$scope.selectedmodel = $("#car-model option:selected").text();
			if($scope.selctGender=="")
			{
				$scope.data.gender = $scope.translation.testDrive.Male;
			}
			testDriveService.registerTestDrive($scope.data,function (response){
				if(response.data.status.status==200)
				{

					$('#popup-ok').show();
				}
			
			});
		
	    }
		var i = 0;
		for (var selector in data) {
			if(data[selector]==''){
				if(!$('.testdrive-mobile-form div.line').eq(i).hasClass('must')) return;
				$('.testdrive-mobile-form div.line').eq(i).addClass('warning');
			}else{
				$('.testdrive-mobile-form div.line').eq(i).removeClass('warning');
			}
			i++;
	    };
	    if(!isPhoneNum(data.phone) || data.phone.isEmpty){
	    	$('#phone').parents('.line').addClass('warning');
	    }
	    if(!isAlphabet(data.name)){
	    	$('#name').parents('.line').addClass('warning');
	    }
	});*/
/*	$scope.privacy = function()
	{
		$('#popup-clause').show();
	};*/
	$scope.privacyhide =function()
	{
		$('#popup-clause').hide();
	};
}]);