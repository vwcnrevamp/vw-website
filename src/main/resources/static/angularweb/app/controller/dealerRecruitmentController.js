angular.module('vwApp').controller('dealerRecruitmentController',
		['$scope','localizationService','dealerRecruitService','apiFactory', '$state', 'homeService',
		  function($scope, localizationService,dealerRecruitService, apiFactory, $state, homeService) {
	
	$scope.province ="";	
	$scope.city="";
	$scope.checkbox= false;
	$scope.recruitReg = {};
	$scope.isDisabled = false;
	$scope.class="get-code";
	$scope.translate = function(){	
		localizationService.getBundle(function(data){
			$scope.translation = data;
		});			   	 
	};
	$scope.translate();	
		
	$scope.recruitDetailsPageUrl = apiFactory.getRecruitDetailsPageUrl();
	
	$scope.isUserLoggedIn = homeService.isUserLoggedIn();	
	$scope.recruitDetailsPageUrl = apiFactory.getRecruitDetailsPageUrl();
	$scope.recruitmentHomeUrl = apiFactory.getRecruitmentHomeUrl();
		
	$scope.registerClicked = function() {
		$state.go('dealerRecruitmentReg');
	};
	
	$scope.loginClicked = function() {
		$state.go('recruitLogin');
	};
	
	
	$scope.logoutClicked = function() {
		homeService.logoutUser();		
	};
	
	$scope.myRecordClicked = function(){
		$state.go('recruitRecord');
	};
		
	$scope.setRecruitmentType = function (val) {
		apiFactory.setRecruitmentType(val);
	};	
	
	
	$('.recruit-reg .clause span.check').on('click', function(event) {
		event.preventDefault();
		if($(this).hasClass('checked')){
			$(this).removeClass('checked')
			$scope.checkbox= false;
		}else{
			$(this).addClass('checked')
			$scope.checkbox= true;
		}
		
	});
	
	function isPhoneNum(val){		
		var phonenum = val;
		var myreg = /^(((1[0-9]{0})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{10})$/; 
		/*var myreg = /^[0-9]*$/;*/
		if(!myreg.test(phonenum)){ 
			return false; 
		}else{
			return true;
		}
	}
	
	$scope.reg= function(){		
		//$('#reg-ok').show();
		if(!isPhoneNum($scope.recruitReg.account) || $scope.recruitReg.account.isEmpty){
			$('#tel').parents('.line').addClass('warning');
	    }	
		if($scope.recruitReg.verification_code === undefined){
			$('input#code').parent('.line').addClass('warning');
		}
		if($scope.recruitReg.captcha === undefined)
		{
		$('input#captcha').parent('.line').addClass('warning');
		}
		var password1 = $('input#password1').val(),
		password2 = $('input#password2').val();
		if(password1=='' || password1.length<6){
			$('input#password1').parent('.line').addClass('warning');
		}else{
			$('input#password1').parent('.line').removeClass('warning');
		}

		if(password1 != password2){
			$('input#password2').parent('.line').addClass('warning');
		}else{
			$('input#password2').parent('.line').removeClass('warning');
		}
		$scope.recruitReg.agreed_tc = $scope.checkbox;
		if($('div.warning').length==0){	
		dealerRecruitService.registerDealerRecruit($scope.recruitReg,function(response){
			if(response.data.status.status=200)
				{
				$('#reg-ok').show();
				}
			
		});
		}
	};
	$scope.popupClose = function(){
		/*	localStorage.removeItem('loginpageorgin');
			localStorage.removeItem('registerOrgin');*/
			$('div#reg-ok').hide();
			$state.go('recruitLogin');
			
		};
	/*$scope.sendVerficationCode = function() {
		if(!$scope.isDisabled){
			var counter = 6;
			span = document.getElementById("get-code");
			var actual = span.innerHTML;
			var check = ValidCaptcha();
			if(check){
				if($scope.recruitReg.account != undefined){
					dealerRecruitService.generateVerificationCode($scope.recruitReg.account,function(response){
						console.log("verification code sent to your phone");
					});
					setInterval(function() {
						counter--;
						if (counter >= 0) {
							$scope.isDisabled = true;
							if($('#get-code').hasClass('no-click')==false) {
								$('#get-code').addClass('no-click')
							}
							span.innerHTML = counter+ " seconds" ;
						}
						if (counter === 0) {
							alert('sorry, out of time');
							clearInterval(counter);
							span.innerHTML = actual;
							$('#code').parents('.line').addClass('warning');
							if($('#get-code').hasClass('no-click')) {
								$('#get-code').removeClass('no-click')
							}
							$scope.isDisabled = false;
						}
					}, 1000);
				}
				else
				{
					$('#tel').parents('.line').addClass('warning');
				}
				
			}
			else
			{
				$scope.captcha = "";
				$scope.drawcaptcha();
				$('#captcha').parents('.line').addClass('warning');
			}
			
		}
		
	};
*/
	 function ValidCaptcha(){
	        var str1 = $scope.txtCaptcha;
	        var str2 = $scope.recruitReg.captcha;
	        if (str1 == str2)
	        { return true;    
	        }
	        else
	        {    
	        return false;
	        }
	        
	    }
	$scope.sendVerficationCode = function() {
		if(!$scope.isDisabled){
			var counter = 60;
			span = document.getElementById("get-code");
			var actual = span.innerHTML;
			var check = ValidCaptcha();
			if($scope.recruitReg.account != undefined){
				if(check){
					dealerRecruitService.generateVerificationCode($scope.recruitReg.account,function(response){
						console.log("verification code sent to your phone");
					});
					setInterval(function() {
						counter--;
						if (counter >= 0) {
							$scope.isDisabled = true;
							if($('#get-code').hasClass('no-click')==false) {
								$('#get-code').addClass('no-click')
							}
							span.innerHTML = counter+ " seconds" ;
						}
						if (counter === 0) {
							clearInterval(counter);
							span.innerHTML = actual;
							$('#code').parents('.line').addClass('warning');
							if($('#get-code').hasClass('no-click')) {
								$('#get-code').removeClass('no-click')
							}
							$scope.isDisabled = false;
						}
					}, 1000);
				}
				else
				{
					$scope.captcha = "";
					$scope.drawcaptcha();
					$('#captcha').parents('.line').addClass('warning');
				}
			}
			else
			{
				$('#tel').parents('.line').addClass('warning');
			}

		}

	};
	$scope.keypress= function(){
		angular.element('input').parent('.line').removeClass('warning');
	};
	$scope.drawcaptcha = function()
		{
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for( var i=0; i < 4; i++ ){
	        text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	    
	    $scope.txtCaptcha =text;
	    angular.element('#txtCaptcha').text(text);
	    //$('#txtCaptcha').text(text);
		};
		
}]);










