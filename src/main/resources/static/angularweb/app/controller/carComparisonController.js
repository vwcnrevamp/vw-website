angular.module('vwApp').controller('carModelComparisionConrtoller',
		['$scope','$sce','urlconfig','$location','$window','$state','localizationService','apiFactory', '$stateParams','homeService','carService','$rootScope','$analytics',
		  function($scope, $sce, urlconfig,$location,$window,$state, localizationService, apiFactory, $stateParams,homeService, carService,$rootScope,$analytics)  {		
		
			$analytics.pageTrack('/compare/models');
	$scope.translate = function(){	
		localizationService.getBundle(function(data){
			$scope.translation = data;
			
		});			   	 
	};
	$scope.translate();	
	
	$scope.allSeriesAndModels 	= {};
	$scope.AllSeries		 	= {};
	$scope.mcount 				= 0;
	$scope.selectedModels 		= [];
	$scope.compareResults 		= {};
	$scope.noOfCarsToCompare	= 2;
	$scope.carBoxes				= [1, 2];	
	$scope.showResult			= false;
	$scope.hideSameValues		= false;
	$scope.totalTRcount 		= 5;	
	
		
	$scope.getAllSeriesAndModels = function (){
		console.log("IN getAllSeriesAndModels ------");	
		
		carService.getAllCarSeriesAndModels( function (response){
			if(response.status.status == 200){					
				console.log("iN rrrrrrrrrrrrr >>>>>");
				$scope.allSeriesAndModels = response.entities;
				
				//console.log($scope.allSeriesAndModels);		
				console.log($state.params);
				//check if the page is loaded by clicking the compare result url in the SMS
				if($state.params && $state.params.model1 && $state.params.series1 && $state.params.model2 && $state.params.series2){
					console.log($state.params.series1);
					//var series_code1 = $state.params.series1;
					//var series_code2 = $state.params.series2;
					//var model_code1 = $state.params.model1;
					//var model_code2 = $state.params.model2;
					var allSeriesModels = response.entities;
					console.log(allSeriesModels);
					for(var i=0; i< allSeriesModels.length; i++) {
						console.log($state.params.series2);
						var series = allSeriesModels[i];
						if(series.code == $state.params.series1) {
							console.log("In if 111");
							var modelList1 = series.listModel;			
							$scope.findSelectedModel(modelList1, $state.params.model1);
						}	
						if(series.code == $state.params.series2) {
							console.log("In if 222");
							var modelList2 = series.listModel;			
							$scope.findSelectedModel(modelList2, $state.params.model2);
						}
					}
					if($scope.selectedModels.length == 2){						
						$scope.getCardata();
					}
				}
			}
		});
	};
	$scope.findSelectedModel = function(modelList, model_code){
		for(var i=0; i< modelList.length; i++) {	
			if(modelList[i].code == model_code){
				$scope.selectedModels.push(modelList[i]);
			}
		}
	};
		
	$scope.getAllSeriesAndModels();
	
	$scope.getModelsForSeries = function (series_code) {
		var modelList = [];
		var allSeriesModels = $scope.getAllSeriesAndModels;
		for(var i=0; i< allSeriesModels.length; i++) {
			var series = allSeriesModels[i];
			if(series.code == series_code) {
				modelList = series.listModel;
			}			
		}
		return modelList;
		
	};
	
	$scope.getModelCount = function(){
		$scope.mcount += 1;
		return $scope.mcount;
	};
	
	$scope.addSelectedModel = function(model){
		$scope.selectedModels.push(model);
	};
	
	$scope.getSeriesLiClass = function (index) {		
		var liClass= "col-xs-6 col-md-4 col-lg-3";
		if($scope.allSeriesAndModels[index].s_selected){
			liClass += " selected seriesHeight";
		}		
		return liClass;
	};
	
	$scope.onSeriesClicked = function (index, event){
		if(event) {
			event.preventDefault();
		}
		for(var i=0; i< $scope.allSeriesAndModels.length; i++) {			
			if(i==index && !$scope.allSeriesAndModels[i].s_selected) {
				$scope.allSeriesAndModels[i].s_selected = true;
			} else {
				$scope.allSeriesAndModels[i].s_selected = false;
			}
		} 
	};	
	
	$scope.getSelectCarModelClass = function(){
		var className = "two";
		if($scope.noOfCarsToCompare == 3){
			className = "three";
		}
		return className;
	};
	
	$scope.getModelImage = function(selModel) {
		var imgUrl = selModel.imageUrl;
		if(null == imgUrl){
			imgUrl = selModel.series_code+".png";
		}
		return imgUrl;
		
	};
	
	$scope.getBlankCarBoxes = function(){	
		$scope.carBoxes = [];
		if($scope.noOfCarsToCompare == 2){
			if($scope.selectedModels.length == 0){
				$scope.carBoxes = [1,2];
			} else if($scope.selectedModels.length == 1){
				$scope.carBoxes = [1];
			}
			
		} else if($scope.noOfCarsToCompare == 3){
			
			 if($scope.selectedModels.length == 2){
				$scope.carBoxes = [1];
			 } 
		}
		return $scope.carBoxes;	
	};
	
	$scope.addCarToCompare = function(){
		$analytics.eventTrack($scope.translation.mEventName.pcAddSeriesCForComparison, {  category: $scope.translation.mEventName.pcModelsMain, label: $scope.translation.eventName.select }); 
		$scope.noOfCarsToCompare = 3;
		$scope.showResult = false;
		$scope.compareResults 	= {};
	};
	
	$scope.resetCarToCompare = function(){
		$scope.noOfCarsToCompare = 2;
	};
	
	$scope.showCompareResult = function (){
		return $scope.showResult;		
	};
	
	$scope.removeSelectedCar = function(selModel, event){
		if(event) {
			event.preventDefault();
		}
		var selModelsTemp = [];
		for(var i=0; i< $scope.selectedModels.length; i++) {			
			if($scope.selectedModels[i].code != selModel.code) {
				selModelsTemp.push($scope.selectedModels[i]);
			}
		}
		$scope.selectedModels = selModelsTemp;
		$scope.resetCarToCompare();
		if($scope.selectedModels.length >1) {
			$scope.getCardata();
			
		} else {// reset compare result
			$scope.compareResults 		= {};
			$scope.showResult = false;
		}
	};
	
	$scope.showAddCar = function (){
		if($scope.selectedModels.length ==2 && $(window).width()>640){			
			return true;			
		}
		return false;
	};
	$scope.getResultColspan = function(){
		var colspan=2;
		if($scope.selectedModels.length ==3) {			
			colspan=3;
		}
		return colspan;
	};
	
	$scope.getCardata = function(){
		console.log("IN getCardata ------");	
		var model1, model2, model3 = null;
		model1 = $scope.selectedModels[0].code;
		model2 = $scope.selectedModels[1].code;
		if($scope.selectedModels.length == 3){
			model3 = $scope.selectedModels[2].code;
		}
		carService.getCarComparisonModelsDetails(model1, model2, model3, function (response){
			
			if(response.status.status == 200){					
				console.log("iN getCardata >>>>>");
				$scope.compareResults = response.entity;
				$scope.showResult = true;
				
				console.log($scope.compareResults);				
			}
		});		
	};
	
	$scope.onModelClicked = function (selModel, seriexIdx){
		
		if($scope.selectedModels.length > 0){			
			var modelAlreadyInQueue = false;
			for(var i=0; i< $scope.selectedModels.length; i++) {	
				
				// check if the selected model is already in compare queue
				if($scope.selectedModels[i].code == selModel.code) {
					modelAlreadyInQueue = true;					
				}
			}
			//if the selected model is not in compare queue, add it
			if(!modelAlreadyInQueue){				
				$scope.selectedModels.push(selModel);
			} else {
				if(!$rootScope.isMobile){
					$analytics.eventTrack($scope.translation.pcEventName.pcSameSeries, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.select }); 
				} else {
					$analytics.eventTrack($scope.translation.mEventName.mSameSeries, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.select }); 
				}				
				$('#popup-sendCode').popup({
					content: '车型已在对比队列!'
				});
			}
		} else {
			$scope.selectedModels = [];
			$scope.selectedModels.push(selModel);
		}
		if($scope.selectedModels.length >1) {			
			$scope.getCardata();			
		} else {// reset compare result
			$scope.compareResults 		= {};
		}
		if($scope.selectedModels.length == 1){
			if(!$rootScope.isMobile){
				$analytics.eventTrack($scope.translation.pcEventName.pcAModel, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.select }); 
			} else {
				$analytics.eventTrack($scope.translation.mEventName.mAModel, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.select }); 
			}
		} else if ($scope.selectedModels.length == 2){
			if($scope.selectedModels[0].series_code != $scope.selectedModels[1].series_code){
				if(!$rootScope.isMobile){
					$analytics.eventTrack($scope.translation.pcEventName.pcOtherSeries, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.select }); 
				} else {
					$analytics.eventTrack($scope.translation.mEventName.mOtherSeries, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.select }); 
				}	
			} else {
				if(!$rootScope.isMobile){
					$analytics.eventTrack($scope.translation.pcEventName.pcSameSeries, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.select }); 
				} else {
					$analytics.eventTrack($scope.translation.mEventName.mSameSeries, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.select }); 
				}	
			}
			if(!$rootScope.isMobile){
				$analytics.eventTrack($scope.translation.pcEventName.pcSeriesB, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.select }); 
			} else {
				$analytics.eventTrack($scope.translation.mEventName.mSeriesB, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.select }); 
			}
		} else if ($scope.selectedModels.length == 3){
			if(!$rootScope.isMobile){
				$analytics.eventTrack($scope.translation.pcEventName.pcSeriesC, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.select }); 
			}
		}
		
		$scope.onSeriesClicked(seriexIdx);
		
	};
	
	$scope.showCompareResult = function (){
		return $scope.showResult;		
	};
	
	$scope.renderHtml = function(html_code){
		return $sce.trustAsHtml(html_code);
	};
	
	$scope.hideSameClicked = function () {
		$scope.hideSameValues = !$scope.hideSameValues;		
	};
	
	$scope.hideSame = function(data){
		if($scope.selectedModels.length == 2) {
			if($scope.hideSameValues && (data.car1.value == data.car2.value)){
				return true;
			}
		} else if($scope.selectedModels.length == 3){
			if($scope.hideSameValues && (data.car1.value == data.car2.value) && (data.car1.value == data.car3.value)){
				return true;
			}
		}
		return false;
	};
	
	$scope.hideSameChkboxClass = function () {
		var hclass = "hide-same";
		if($scope.hideSameValues){
			hclass += "  active";
		}
		return hclass;
	};
	
	$scope.displayCar3 = function(){
		if($scope.selectedModels.length == 3){
			return true;
		}
		return false;
	};
	
	$scope.getTrClass = function(){
		var className =  "bg";
		if($scope.totalTRcount % 2 == 1){
			className = "";
			$scope.totalTRcount -= 1;
		}
		return className;
	};
	
	$scope.registerForTestDrive = function (series_code,index){
		if(index == 0){
			if(!$rootScope.isMobile){
				$analytics.eventTrack($scope.translation.pcEventName.pcSeriesATestDrive, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.select }); 
			} else {
				$analytics.eventTrack($scope.translation.mEventName.mSeriesATestDrive, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.select }); 
			}
		} else if (index == 1){
			if(!$rootScope.isMobile){
				$analytics.eventTrack($scope.translation.pcEventName.pcSeriesBTestDrive, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.select }); 
			} else {
				$analytics.eventTrack($scope.translation.mEventName.mSeriesBTestDrive, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.select }); 
			}
		} else if (index == 2){
			if(!$rootScope.isMobile){
				$analytics.eventTrack($scope.translation.pcEventName.pcSeriesCTestDrive, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.select }); 
			}
		}
		var series = {"series_code": series_code};
		$state.go('testDrive', {series:series});
	};
	/*$('.models-content').on('click', function(event) {
		alert('clicked');
		event.preventDefault();
		var h = $(this).next('.this-style').height();
		if($(this).parent('li').hasClass('selected')){
			$(this).parent('li').removeClass('selected').removeAttr('style').siblings('li').removeClass('selected');
		}else{ 
			$('#models-list li').removeClass('selected').removeAttr('style');
			$(this).parent('li').addClass('selected').css('height',110+h);
		}
		
	});*/	
	
	
	$(window).resize(function(event) {
		$('#models-list li').each(function(index, el) {
			if($(this).find('.this-style').is(':visible')){
				$(this).height(110+$(this).find('.this-style').height())
			}
		});
	});

	$scope.onViewCarDetails = function (model){
		if(model.url){
			return model.url;
		} else {
			//alert('The model Url is null');
			return false;
		}
		
	};
	
/*	$('.style-content').on('click', function(event) {
		
		event.preventDefault();

		var $selectedModels = $('#selected-models li.car-box');
		var html = $(this).html();

		//循环对比列表 Robin List
		for (var i = 0; i < $selectedModels.length; i++) {

			//如果为空添加内容并跳出for循环   If it is empty and add content jump for loop
			if(!$selectedModels.eq(i).hasClass('selected')){

				//判断车型是否已在队列 Determining whether models have been in the queue
				if($(this).hasClass('selected')) {
					//alert('车型已在对比队列!'); Models have been queued for comparing
					$('#popup-sendCode').popup({
						content: '车型已在对比队列!'
					});
					return;
				}

				//添加到对比队列 Add to Compare queue
				$(this).addClass('selected');
				$selectedModels.eq(i).addClass('selected').html('<a href="javascript:;" class="del"></a>'+html+'<a href="#" class="btn-testdrive">预约试驾</a><br /><a href="#" class="btn-view-car">查看车型详情</a>').attr('car-id',$(this).attr('car-id')).animateCss('zoomIn');

				//对比队列绑定删除事件
				$selectedModels.eq(i).find('a.del').on('click', function(event) {
					event.preventDefault();
					var carIndex = $(this).parent('li').index();
					
					var $thisUl = $(this).parents('ul');

					//移除对比列表
					$(this).parent('.selected').remove();

					//移除车型列表已选标记
					$('.style-content[car-id='+$(this).parent('li').attr('car-id')+']').removeClass('selected');

					//移除对比列表对应车型内容
					$('table.details tr').each(function(index, el) {
						$(this).find('td').eq(carIndex).remove();
					});
		
					var len = $thisUl.find('.car-box').length;
					//对比列表少于2,则添加一个
					if(len<2){
						$thisUl.find('.car-box').removeClass('show').after('<li class="car-box"></li>');

					}
					
					
					var len = $('#selected-models .selected').length;
					if(len==2){
						$('#selected-models ul, #result-box').removeClass().addClass('two');
						$('#selected-models').find('.add').show();
					}else if(len==1){
						$(this).parent('li').removeClass('selected').removeAttr('car-id').html('');	
						$('#selected-models').find('ul').removeClass().addClass('two').find('.add').hide();
						$('#models-list').show();
						$('#compare-result').hide();
					}

				});

				//获取对比队列车型参数信息
				var len = $('#selected-models li.selected').length;

				if(len > 1){
					if(len == 2 && $(window).width()>640){
						$selectedModels.parents('.selected-models').find('.add').show();
					}
					
					$('#selected-models li.selected').addClass('show');


					//获取车型基本参数数据并显示
					var carId=[];
					for (var i = 0; i < len; i++) {
						carId[i] = Number($selectedModels.eq(i).attr('car-id'));//车id
						
					}
					//console.log(carId);
					getCardata(carId);


				}

				//跳出for循环
				break;
				
			}
		}
	});*/	

/*	//获取车型数据,测试数据,实际数据后台处理
	function getCardata(arr) {
		var resultHtml;
		if(arr.length==2) {
			resultHtml = '<table class="details"><tr><td class="config_title"><a id="jbcs" name="jbcs"></a>基本参数</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>48FPK数字液晶仪表（选配）</p><p>Dynaudio丹拿高级环绕音响带12扬声器（选配）</p><p>“MIB Standard Navi 导航包”：含MIB Standard Navi 导航+RVC倒车影像系统+后排USB接口+动感车顶鲨鱼鳍天线+后备箱12V电源（选配）</p><p>“FPK数字液晶仪表包”：含FPK数字液晶仪表(选○8选装包时须同时选○10选装包)（选配）</p><p>“丹拿音响包”：含Dynaudio丹拿高级环绕音响带12扬声器（选配）</p><span>说明</span></td><td><p>48FPK数字液晶仪表（选配）</p><p class="m11">Dynaudio丹拿高级环绕音响带12扬声器（选配）</p><p class="m7">“MIB Standard Navi 导航包”：含MIB Standard Navi 导航+RVC倒车影像系统+后排USB接口+动感车顶鲨鱼鳍天线+后备箱12V电源（选配）</p><p>“FPK数字液晶仪表包”：含FPK数字液晶仪表(选○8选装包时须同时选○10选装包)（选配）</p><p>“丹拿音响包”：含Dynaudio丹拿高级环绕音响带12扬声器（选配）</p><span>说明</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><img src="images/compare/test1.jpg" alt="" class="pic"><p>涡轮增压</p><span>发动机型号</span></td><td><img src="images/compare/test1.jpg" alt="" class="pic"><p>涡轮增压</p><span>发动机型号</span></td></tr><tr><td><p><img src="images/compare/icon2.png" alt=""></p><span>panaroma电动全景天窗带遮阳帘</span></td><td><p><img src="images/compare/icon2.png" alt=""></p><span>panaroma电动全景天窗带遮阳帘</span></td></tr><tr class="bg"><td><p><img src="images/compare/icon3.png" alt=""></p><span>跑车式豪华无框车门</span></td><td><p><img src="images/compare/icon3.png" alt=""></p><span>跑车式豪华无框车门</span></td></tr><tr><td><p><img src="images/compare/icon4.png" alt=""></p><span>17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"</span></td><td><p><img src="images/compare/icon4.png" alt=""></p><span>17寸合金轮毂"Lakeville"</span></td></tr><tr><td class="config_title"><a id="wgpz" name="wgpz"></a>外观配置</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td class="config_title"><a id="nspz" name="nspz"></a>内饰配置</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td class="config_title"><a id="aqkj" name="aqkj"></a>安全科技</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td class="config_title"><a id="sskj" name="sskj"></a>舒适科技</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr></table>';
			$('#result-box').removeClass().addClass('two');

		}else if(arr.length==3){
			resultHtml = '<table class="details"><tr><td class="config_title"><a id="jbcs" name="jbcs"></a>基本参数</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><img src="images/compare/test1.jpg" alt="" class="pic"><p>涡轮增压</p><span>发动机型号</span></td><td><img src="images/compare/test1.jpg" alt="" class="pic"><p>涡轮增压</p><span>发动机型号</span></td><td><img src="images/compare/test1.jpg" alt="" class="pic"><p>涡轮增压</p><span>发动机型号</span></td></tr><tr><td><p><img src="images/compare/icon2.png" alt=""></p><span>panaroma电动全景天窗带遮阳帘</span></td><td><p><img src="images/compare/icon2.png" alt=""></p><span>panaroma电动全景天窗带遮阳帘</span></td><td><p><img src="images/compare/icon2.png" alt=""></p><span>panaroma电动全景天窗带遮阳帘</span></td></tr><tr class="bg"><td><p><img src="images/compare/icon3.png" alt=""></p><span>跑车式豪华无框车门</span></td><td><p><img src="images/compare/icon3.png" alt=""></p><span>跑车式豪华无框车门</span></td><td><p><img src="images/compare/icon3.png" alt=""></p><span>跑车式豪华无框车门</span></td></tr><tr><td><p><img src="images/compare/icon4.png" alt=""></p><span>17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"17寸合金轮毂"Lakeville"</span></td><td><p><img src="images/compare/icon4.png" alt=""></p><span>17寸合金轮毂"Lakeville"</span></td><td><p><img src="images/compare/icon4.png" alt=""></p><span>17寸合金轮毂"Lakeville"</span></td></tr><tr><td class="config_title"><a id="wgpz" name="wgpz"></a>外观配置</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td class="config_title"><a id="nspz" name="nspz"></a>内饰配置</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td class="config_title"><a id="aqkj" name="aqkj"></a>安全科技</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td class="config_title"><a id="sskj" name="sskj"></a>舒适科技</td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr><tr class="bg"><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td><td><p>4812x1855x1417</p><span>长x宽x高[mm]</span></td></tr></table>';
			$('#result-box').removeClass().addClass('three');
		}

		$('#result-box').html(resultHtml);

		$('#models-list').hide();
		$('#compare-result').show();
	}


	$('#selected-models .add').on('click', function(event) {
		event.preventDefault();
		$(this).hide();
		$(this).parents('ul').removeClass('two').addClass('three').find('li').eq(1).after('<li class="car-box"></li>')
		$('#models-list').show();
		$('#compare-result').hide();
	});

	$('a.hide-same').on('click', function(event) {
		event.preventDefault();
		if($(this).hasClass('active')){
			$(this).removeClass('active')
		}else{
			$(this).addClass('active')
		}
	});*/


	var flag=false;
	var $SM = $('#selected-models'), 
		$navBox = $('#compare-result .nav-box');
	$(window).scroll(function(event) {
		
		//console.log(window.scrollY);
		if($(this).scrollTop() > $SM.height()+88 && !$SM.hasClass('fixed')){
			flag=true;
			//$('#compare').css('padding-top',$SM.height()+88);

			$SM.addClass('fixed').width($('.mainbody').width());
			$navBox.addClass('fixed').css({'top':$SM.height(), 'width':$('.mainbody').width()});
			
		}else if($SM.hasClass('fixed') && $(this).scrollTop() < $SM.height()+88){
			flag=false;
			$SM.removeClass('fixed').removeAttr('style');
			$navBox.removeClass('fixed').removeAttr('style');
			//$('#compare').removeAttr('style');
		}
	});

	$(window).resize(function(event) {
		if(!$SM.hasClass('fixed')) return;
		$('#compare').css('padding-top',$SM.height()+88);

		$SM.addClass('fixed').width($('.mainbody').width());
		$navBox.addClass('fixed').css({'top':$SM.height(), 'width':$('.mainbody').width()});
	});

	var $mobileNav=$(".m_anchorlink"), $pcNav = $(".anchorlink");
	
	$(window).scroll(function(){		
		if($(window).width()<=640 && $scope.selectedModels.length >1){			
			var top = $(this).scrollTop();
			var s = [];
			for(i=0;i<$mobileNav.size();i++){					
				var sid = $mobileNav.eq(i).attr("href");				
				var _top = $(sid).offset().top-143;
				var b = _top - top;
				if(b<0) b=$mobileNav.size()-i;
				s.push(b);
			}
			//console.log(s);
			var f = s.indexOf(Math.min.apply(Math, s));			
				if(f == 0){
					$analytics.eventTrack($scope.translation.mEventName.mBasicParameters, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.browse }); 
				}			
				if(f == 1){
					$analytics.eventTrack($scope.translation.mEventName.mExteriorConfiguration, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.browse }); 
				}			
				if(f == 2){
					$analytics.eventTrack($scope.translation.mEventName.mInteriorConfiguration, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.browse }); 
				}			
				if(f == 3){
					$analytics.eventTrack($scope.translation.mEventName.mSafetyTechnology, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.browse }); 
				}		
				if(f == 4){
					$analytics.eventTrack($scope.translation.mEventName.mComfortScienceAndTechnology, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.browse }); 
				}
			
			
			$mobileNav.eq(f).addClass("active").parents('li').siblings('li').find('a').removeClass('active');
			var text = $(".nav-mobile a.active").html();
			$('.btn-select').html(text);
		}else if($scope.selectedModels.length >1){			
			var top = $(this).scrollTop();
			var s = [];
			for(i=0;i<$pcNav.size();i++){				
				var sid = $pcNav.eq(i).attr("href");				
				var _top = $(sid).offset().top-233;
				var b = _top - top;
				if(b<0) b=$pcNav.size()-i;
				s.push(b);
			}
			//console.log(s);
			var f = s.indexOf(Math.min.apply(Math, s));//console.log(f)		
			if(f == 0){
				$analytics.eventTrack($scope.translation.pcEventName.pcBasicParameters, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.browse }); 
			}		
			if(f == 1){
				$analytics.eventTrack($scope.translation.pcEventName.pcExteriorConfiguration, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.browse }); 
			}				
			if(f == 2){
				$analytics.eventTrack($scope.translation.pcEventName.pcInteriorConfiguration, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.browse }); 
			}		
			if(f == 3){
				$analytics.eventTrack($scope.translation.pcEventName.pcSafetyTechnology, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.browse }); 
			}	
			if(f == 4){
				$analytics.eventTrack($scope.translation.pcEventName.pcComfortScienceAndTechnology, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.browse }); 
			}
			$pcNav.eq(f).addClass("active").siblings('a').removeClass('active');
		}
		
	});

	$('.nav-mobile a.btn-select').on('click', function(event) {
		event.preventDefault();
		var $navBox = $('.nav-box');
		if($($navBox).hasClass('show')){
			$navBox.removeClass('show').animate({height: '88px'},200);
		}else{
			$navBox.addClass('show').animate({height: '338px'},200);
		}
		
	});

	
	function scrollOffset(top,time){
		$('html,body').stop().animate({scrollTop:top},time);
	}
	$('#compare-result .nav a').on('click', function(event) {
		event.preventDefault();
		if($(this).hasClass('active')) return;
		$(this).addClass('active').siblings('a').removeClass('active');

		var $navBox = $('.nav-box');
		$navBox.removeClass('show').css('height','88px')
		
		var sid = $(this).attr("href");
		var top = $(sid).offset().top;
		var topY;
		
		if(top<=334){
			topY=0;
		}else{
			
			if(flag){
				topY = top-233;console.log('true');
			}else{
				topY = top-560;console.log('false');
			}
			
		}
		scrollOffset(topY,0);
		return false;
		

	});

	$('.nav-mobile ul li a').on('click', function(event) {
		event.preventDefault();
		$(this).addClass('active').parent('li').siblings('li').find('a').removeClass('active');
		$('a.btn-select').html($(this).html());
		var $navBox = $('.nav-box');
		$navBox.removeClass('show').css('height','88px')
		
		var sid = $(this).attr("href");
		var top = $(sid).offset().top;
		var topY;
		
		if(top<=373){
			topY=0;
		}else{
			
			if(flag){
				topY = top-143;console.log('true');
			}else{
				topY = top-460;console.log('false');
			}
			
		}
		scrollOffset(topY,0);
		return false;
		
	});

	

	$('.send-result').on('click', 'a.submit', function(event) {
		event.preventDefault();
		var text;
		if(isPhoneNum($('input#tel').val())){
			text='信息已发送至您的手机';
		}else{
			text='手机号码格式不正确';
		};
		$('#popup-sendCode').popup({
			content: text
		});
		//$('#popup-sendCode').show().find('p').text(text);
	});
	
	$scope.phoneTrigger = function (){		
		$analytics.eventTrack($scope.translation.mEventName.mInputMobilePhoneNumberCC, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.editProfile });
	};

	// $('.popup').on('click', 'a.btn-close', function(event) {
	// 	event.preventDefault();
	// 	$(this).parents('.popup').hide()
	// });
	$scope.smscompare = function()
	{
		$analytics.eventTrack($scope.translation.mEventName.mEnterThePhoneNumberToSendCC, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.select }); 
		$scope.data = {};
		$scope.data.phoneNumber = $scope.compare.tel;
		model1 = $scope.selectedModels[0].code;
		model2 = $scope.selectedModels[1].code;
		series1 = $scope.selectedModels[0].series_code;
		series2 = $scope.selectedModels[1].series_code;
		var str = $scope.translation.sms.carCompare;
		ul = urlconfig.apiURL+"#/compare/result/"+model1 +"/"+series1+"/"+model2+"/"+series2;
		str = str + ul;
		console.log(ul);
		$scope.data.msg=str;
	//	$window.location.href =ul;
		homeService.sendSms($scope.data,function(response){
		});
		};
		
		
		$scope.goToBasic = function (){
			if(!$rootScope.isMobile){
				$analytics.eventTrack($scope.translation.pcEventName.pcBasicParameters, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.browse }); 
			} else {
				$analytics.eventTrack($scope.translation.mEventName.mBasicParameters, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.browse }); 
			}	
		};
		
		$scope.goToAppear = function (){
			if(!$rootScope.isMobile){
				$analytics.eventTrack($scope.translation.pcEventName.pcExteriorConfiguration, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.browse }); 
			} else {
				$analytics.eventTrack($scope.translation.mEventName.mExteriorConfiguration, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.browse }); 
			}			
		};
		
		$scope.goTointerior = function (){
			if(!$rootScope.isMobile){
				$analytics.eventTrack($scope.translation.pcEventName.pcInteriorConfiguration, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.browse }); 
			} else {
				$analytics.eventTrack($scope.translation.mEventName.mInteriorConfiguration, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.browse }); 
			}
		};
		
		$scope.goToSafety = function (){
			if(!$rootScope.isMobile){
				$analytics.eventTrack($scope.translation.pcEventName.pcSafetyTechnology, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.browse }); 
			} else {
				$analytics.eventTrack($scope.translation.mEventName.mSafetyTechnology, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.browse }); 
			}
		};
		
		$scope.goToComfort = function (){
			if(!$rootScope.isMobile){
				$analytics.eventTrack($scope.translation.pcEventName.pcComfortScienceAndTechnology, {  category: $scope.translation.pcEventName.pcModelsMain, label: $scope.translation.eventName.browse }); 
			} else {
				$analytics.eventTrack($scope.translation.mEventName.mComfortScienceAndTechnology, {  category: $scope.translation.mEventName.mModelsMain, label: $scope.translation.eventName.browse }); 
			}
		};
		
		

	
}]);










