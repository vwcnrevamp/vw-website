angular.module('vwApp').controller('searchDealerController',['$http','$scope','$cookies','$rootScope','$location','$filter','$window','$localStorage','$state','$cookieStore','$timeout','$geolocation','homeService','testDriveService','localizationService','$analytics',function($http,$scope,$cookies,$rootScope,$location,$filter,$window,$localStorage, $state,$cookieStore,$timeout,$geolocation,homeService,testDriveService,localizationService,$analytics) {

	
		$analytics.pageTrack('/searchDealer');
		dealerdata =[];
		maps = [];
		var map = new BMap.Map("dealer-map"); 
		var markers = [];
		/**
		 * All the provinces and cities
		 */
		$scope.provinces_cities = [];
		$scope.provinces =[];
		$scope.cities =[];
		$scope.dealerTypes =[];
		var map_address="Beijing";
		$scope.city = {city_code:'500002', city_name: 'Beijing', latitude:'116.404', longitude: '39.915', province_code: '10001', province_name: 'Beijing'}; // get the default city details using API
		$scope.province = {province_code: '10001', province_name: 'Beijing', city_code:'500002', city_name: 'Beijing', longitude:'116.404', latitude: '39.915'} ;// default province
		$scope.dealerType = {};	
		
		$scope.temp_province = [];
		
		var lat;
		var lang;
		$scope.location = true;
		$scope.latde=""; 
		$scope.langd="";
		$scope.check = true;
		var opts = {
				width : 590,     // 信息窗口宽度
				height: 300,     // 信息窗口高度
				//title : "信息窗口" , // 信息窗口标题
				enableMessage:true//设置允许信息窗发送短息
			};
		$scope.translate = function(){	
			localizationService.getBundle(function(data){
				$scope.translation = data;
			});			   	 
		};
		$scope.translate();
		
		
		$scope.getDealerTypeData = function() {			
			homeService.getAlldealerType(function (response){
				if(response.status.status==200){
					$scope.dealerTypes = response.entities;	
				}
			});
		};
		
		
		$scope.searchDealer = function () {	
			$analytics.eventTrack($scope.translation.pcEventName.pcCity, {  category: $scope.translation.pcEventName.pcQuery_main, label: $scope.translation.eventName.select });
			$scope.searchResult =[];
			var selectPrv, selectCity, selectDType = null;
			console.log("In search Dealer ======  ");
			if($scope.province) {
				selectPrv = $scope.province.province_code; 
				console.log(selectPrv);
			}
			
			
			if($scope.city) {
				localStorage.setItem("city_name",$scope.city.city_name);
				selectCity = $scope.city.city_code;
				console.log(selectCity);
				$scope.check = false;
				if($scope.city){
				lat = $scope.city.latitude;
				lang = $scope.city.longitude;
				map_address=$scope.city.city_name;				
				console.log(lat+ "  " +lang);				
				}
			}
			if($scope.dealerType) {
				selectDType = $scope.dealerType.code;
				console.log(selectDType);
				
			}			
			console.log("=======================  ");			
			homeService.getdealerCityProvincedealerTypefilter(selectPrv, selectCity, selectDType, 
				function (response){
					if(response.status.status==200 && response.entities){						
						dealerdata = response.entities;						
						$scope.searchResult = dealerdata;
						$scope.searchResultCount = $scope.searchResult.length;	
						$scope.mapdata();
					}
			}); 
		};
		
		$scope.dealerTypeTrigger = function (){
			$analytics.eventTrack($scope.translation.pcEventName.pcDealerType, {  category: $scope.translation.pcEventName.pcQuery_main, label: $scope.translation.eventName.select });
		};
		
		
		$scope.getAllcityprovince = function () {
			   // 创建Map实例
			$scope.getDealerTypeData();							
			$scope.province = {province_code: '10001', province_name: 'Beijing', city_code:'500002', city_name: 'Beijing', longitude:'116.404', latitude: '39.915'} ;// default province
			homeService.getAllprovincesAndcities(function (response){
				if(response.status.status==200){
					$scope.provinces_cities = response.entities;
					var prv= $scope.provinces_cities;
					for (var j = 0; j < prv.length; j++) {
						if($scope.temp_province.indexOf(prv[j].province_name) < 0) {
							$scope.temp_province.push(prv[j].province_name);
							$scope.provinces.push(prv[j]);
						}
					}	
					if($scope.provinces && $scope.provinces.length){
						$scope.province = $scope.provinces[0];
						$scope.changeCityList();
					}
				}
			}); 
		};
		
		function addClickHandler(content,marker){		
			marker.addEventListener("click",function(e){			
				openInfo(content,e);
				}
			);
		}
		
		function openInfo(content,e){		
			var p = e.target;
			var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
			var infoWindow = new BMap.InfoWindow(content,opts);  // 创建信息窗口对象 
			map.openInfoWindow(infoWindow,point); //开启信息窗口
		}
		
		$scope.dealerDataId = function (id){	
			$analytics.eventTrack($scope.translation.pcEventName.pcSelectDealerInformation, {  category: $scope.translation.pcEventName.pcQuery_main, label: $scope.translation.eventName.select });
			if(id) {			
				var m = markers[id];			
				var point = new BMap.Point(m.dealer.longitude, m.dealer.lattitude);
				var infoWindow = new BMap.InfoWindow(m.content,opts);  // 创建信息窗口对象 
				map.openInfoWindow(infoWindow,point); //开启信息窗口					
			}		
		};	
		
		$scope.mapdata = function()
		{		
			//map.centerAndZoom(new BMap.Point(lat, lang), 11); 
			map.centerAndZoom(map_address, 11);		
			map.enableScrollWheelZoom(true);
			map.clearOverlays();		
			var ind = 0;
			for (var i = 0; i <dealerdata.length; i++) {			
				var json = dealerdata[i];
				var deta = JSON.stringify(json);
				var p0 = json.lattitude;
				var p1 = json.longitude;
				var point = new BMap.Point(p1, p0);
				var myIcon = new BMap.Icon("https://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25), {
					offset: new BMap.Size(10, 25),
					imageOffset: new BMap.Size(0, 0 - i * 25)
				});
				var marker = new BMap.Marker(point, { icon: myIcon });			
				var label = new BMap.Label(json.full_name, { "offset": new BMap.Size("http://static.blog.csdn.net/images/medal/holdon_s.gif".lb - "http://static.blog.csdn.net/images/medal/holdon_s.gif".x + 10, -20) });
				marker.setLabel(label);
				map.addOverlay(marker);
				label.setStyle({
					color : "#f00", 
					fontSize : "10px", 
					backgroundColor :"0.05",
					border :"0", 
					fontWeight :"bold" 
				});		
				
				var sContent = "<p style='font-size:18px;'>"+(i+1)+"."+dealerdata[i]['full_name']+"</p>" +
				"<p style='font-size:16px;height:28px;line-height:28px;'>"+dealerdata[i]['address']+"</p>"+
				"<div style='border-top:1px solid #dee1e3;margin-top:15px;padding-top:24px;overflow: hidden;'>" +
				"<div style='float:left;border-right:1px solid #dee1e3;padding-right:20px;'><p style='height:28px;line-height:28px;font-size:16px;width:190px;'>" +
				"销售热线:<span style='color:#1597d9;'>"+dealerdata[i]['sales_hotline']+"</span></p><a href='#' style='color:#1597d9;margin-top:30px;display:block;background:url(angularweb/assets/images/icon-arrow1.png) center left no-repeat;padding-left:24px;font-size:18px;text-decoration: underline;'>" +
						"进入经销商首页</a><a href='#' style='color:#ffffff;background-color:#019ada;width:148px;height:42px;border-radius:3px;text-align:center;margin-top:30px;display:block;line-height:42px;font-size:18px;' onclick='sendDealerId("+dealerdata[i].id+")'>预约试驾</a></div><div style='float:left;padding-left:20px;'>" +
						"<img src='angularweb/assets/images/dealer-popup-img.jpg' /><div class='smsDiv' style='margin-top:12px;text-align:center;height:64px; line-height:44px;color:#1597d9;font-size:18px;position: relative;'><input type='tel' id='tel"+i+"' onchange= 'popupPhoneTriggered()' maxlength='11' placeholder='短信获取经销商信息' style='float:left;border:1px solid #dee1e3;background-color:#ffffff;width:229px;height:44px;-webkit-appearance:none;line-height:44px;border-radius:3px;padding:0px 12px;'>" +
						"<a  style='color:#ffffff;background-color:#019ada;width:78px;height:42px;border-radius:3px;text-align:center;display:block;line-height:42px;font-size:18px;float:left;margin-left:10px;' onclick='sendInfo("+i+","+deta+")'>发送</a><p class='warning'></p></div></div></div>";
				addClickHandler(sContent,marker);
				ind++;
				markers[dealerdata[i].id] = { dealer : dealerdata[i], content : sContent};
			
		}
		
		
		//Create a Icon
		function createIcon(json) {		
			var icon = new BMap.Icon("http://dev.baidu.com/wiki/static/map/API/img/ico-marker.gif", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowAnchor:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})  
			return icon; 
		}
		};
		
		
		//load all the cities and provinces
		//$scope.getAllcityprovince();
		
		$scope.showlocation = function (callback , errorCallback) {
			// One-shot position request.
			//navigator.geolocation.getCurrentPosition(callback);
			navigator.geolocation.getCurrentPosition(callback,
				    errorCallback);
		};	
		
		
		$scope.changeCityList = function() {
			$analytics.eventTrack($scope.translation.pcEventName.pcProvince, {  category: $scope.translation.pcEventName.pcQuery_main, label: $scope.translation.eventName.select });
			$scope.populateCityForSelectedProvince();
			if($scope.cities){
				$scope.city = $scope.cities[0]; // assigning the first city as default							
				$scope.searchDealer();								
			}		
		};
		
		
		$scope.populateCityForSelectedProvince = function() {
			var allCities = $scope.provinces_cities;						
			console.log("Selected Province>>>>  ");
			console.log($scope.province);
			$scope.cities = [];
			for (var i = 0; i < allCities.length; i++) {
				if (allCities[i].province_code === $scope.province.province_code) {
					console.log("Adding  city >>>>  " + i);
					console.log(allCities[i]);
					$scope.cities.push(allCities[i]);
				}
			}
		};
		
		$scope.setSelectedCityProvince = function (city_code) {
			var allCities = $scope.provinces_cities;
			
			for (var i = 0; i < allCities.length; i++) {
				if (allCities[i].city_code === city_code) {
					$scope.city = allCities[i];
					localStorage.setItem("city_name",$scope.city.city_name);	
					break;
				}
			}
			
			var selCity = $scope.city;
			$scope.searchDealer();
			$scope.setSelectedProvince(selCity.province_code);
		};
		
		$scope.setSelectedProvince = function (province_code) {
			var uniqueProvinces = $scope.provinces;
			
			for (var i = 0; i < uniqueProvinces.length; i++) {
				if (uniqueProvinces[i].province_code === province_code) {
					$scope.province = uniqueProvinces[i];
					break;
				}
			}
			
			$scope.populateCityForSelectedProvince();
		};	
		
			
		$scope.setCity = function (cityName){
			homeService.getCityByName(cityName, function (response){				
				if(response.status.status == 200){
					var cityObj = response.entity;
					console.log(response);
					console.log(cityObj);
					$scope.setSelectedCityProvince(cityObj.code);
				}
			});		
		};
		
				
		$scope.getBgClass = function (row){
			if(row%2 == 1){
				return "li-bg";
			} else {
				return "";
			}
		};
		
		
		$('.line.clause').on('click', 'a', function(event) {
			event.preventDefault();
			$('#popup-clause').show();
		});

		$('#popup-ok').on('click', 'a.ok', function(event) {
			$(this).parents('#popup-ok').hide();
			$('select option[value=""]').prop('selected',true).trigger('chosen:updated');
			$('input').val('');
		});
		$('.popup').on('click', 'a.popup-close', function(event) {
			$(this).parents('.popup').hide();
			$('select option[value=""]').prop('selected',true).trigger('chosen:updated');
			$('input').val('');
		});
		


}]);