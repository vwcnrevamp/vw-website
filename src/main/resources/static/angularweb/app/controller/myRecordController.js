angular.module('vwApp').controller('myRecordController',
		['$scope','$state', 'localizationService','dealerRecruitService','homeService','testDriveService','apiFactory','$localStorage', function($scope,$state, localizationService,dealerRecruitService,homeService,testDriveService,apiFactory,$localStorage) {
			
	$scope.pageno = 1; // initialize page no to 1
	
	$scope.itemsPerPage = 3; //this could be a dynamic value from a drop down
	
			
	$scope.translate = function(){	
		localizationService.getBundle(function(data){
			$scope.translation = data;
		});			   	 
	};
	$scope.translate();
	
	$scope.isUserLoggedIn = homeService.isUserLoggedIn();	
	
	if($scope.isUserLoggedIn){
		$scope.currentUser = homeService.getCurrentUser();
	} else {
		$state.go('recruitLogin');
	};
	
	$scope.registerClicked = function() {
		$state.go('dealerRecruitmentReg');
	};
	
	$scope.loginClicked = function() {
		$state.go('recruitLogin');
	};
	
	$scope.logoutClicked = function() {
		homeService.logoutUser();		
	};
		
	$scope.tabs = [{ "text": "tabAllRecord", "selected": true, "status": "ALL"},
	               { "text": "tabSubmitted", "selected": false, "status": "SUBMITTED"},
	               { "text": "tabSaved", "selected": false, "status": "SAVED"},
	               { "text": "tabExpired", "selected": false, "status": "EXPIRED"}];
	
	
	$scope.recruitmentHomeUrl = apiFactory.getRecruitmentHomeUrl();
	$scope.recruitDetailsPageUrl = apiFactory.getRecruitDetailsPageUrl();
	$scope.recruitApplyPageUrl = apiFactory.getRecruitApplyUrl();
	
	$scope.setRecruitmentType = function (val) {		
		apiFactory.setRecruitmentType(val);
	};
	
	
	$scope.getTabClass = function (tab){
		if(tab.selected == true){
			return "active";
		} else {
			return "";
		}		
	};
	
	$scope.selectTab = function(index) {
		for(var i = 0 ; i< $scope.tabs.length; i++){
			if(i == index){
				$scope.tabs[i].selected = true;
				$scope.selectedStatus = $scope.tabs[i].status;
				$scope.getMyRecords($scope.pageno);
			} else {
				$scope.tabs[i].selected = false;
			}
		}
	};
	
	$scope.getTrClass = function(index) {
		if(index % 2 == 0) {
			return "bg";			
		}
		else {
			return "";
		}
		
	};
	$scope.intMyRecordPage = function(){
		$scope.applicationRecord={};
    	$scope.applicationRecord.applicationRecords = [{"id": 1,  "province": "Shanxi", "city": "Shuozhou", "district": "Urban", "submittedDateStr": null, "status": "SUBMITTED", "uniqueCode":"5000012001160912"},
    	                                               {"id": 7, "province": "10001", "city" : "500002", "district": "Urban", "status":"DRAFT", "submittedDate":1473960088000,  "submittedDateStr":null, "uniqueCode": null},                                             
    	                                               
    	                                              {"id":2,  "province": "Shanxi", "city": "Linfen", "district": "Urban", "submittedDateStr": "2016.09.15", "status": "SAVED", "uniqueCode":null},
    	                         					  {"id":3,  "province": "Liaoning", "city": "Shenyang", "district": "市区(优先：皇姑区)", "submittedDateStr": "2016.07.20", "status": "EXPIRED", "uniqueCode":null}];
    	$scope.totalRecordCount=4;                        	
    };
	
	$scope.getMyRecords = function (pageNo){
		//In practice this should be in a factory.
		$scope.currentPage = pageNo;
		$scope.data = {};
		$scope.data.rowsPerPage = $scope.itemsPerPage;
		$scope.data.page = pageNo;
		$scope.data.user_id = $scope.currentUser.id; 
		if($scope.selectedStatus != 'ALL') {
			$scope.data.status = $scope.selectedStatus;
		}
		//$scope.intMyRecordPage();
		dealerRecruitService.getApplicationRecordsr($scope.data,function (response){
			console.log(response);
			if(response.data.status.status == 200){
				console.log(response.data.entity);
				$scope.applicationRecord = response.data.entity; 		
				$scope.applicationRecord.applicationRecords = response.data.entity.applicationRecords;
				$scope.applicationRecord.totalRecordCount = response.data.entity.totalRecordCount;
				$scope.totalRecordCount=response.data.entity.totalRecordCount;
				console.log($scope.totalRecordCount);
				console.log($scope.applicationRecord.applicationRecords);
			}
		});
		
	};
	$scope.getMyRecords($scope.pageno);
	
	$scope.getCityPoint = function(rec){
		return rec.province + "/" +rec.city;		
	};
	
    $scope.getStatusStr = function(rec) {
    	console.log("In Get Status ----");
    	var statusStr = "";
    	if(rec.status == 'SUBMITTED'){
    		statusStr = $scope.translation.myRecord.tabSubmitted;
    	} else if(rec.status == 'SAVED') {
    		statusStr = $scope.translation.myRecord.tabSaved;
    	} else if(rec.status == 'EXPIRED') {
    		statusStr = $scope.translation.myRecord.tabExpired;
    	}
    	console.log(statusStr);
    	return statusStr ;
    };
    
    $scope.getRemarks = function(rec) {
    	if(rec.status == 'SUBMITTED'){
    		return $scope.translation.myRecord.remarkSubmitted + " : " + rec.uniqueCode;
    	} else if(rec.status == 'SAVED') {
    		return $scope.translation.myRecord.remarkSaved;
    	} else if(rec.status == 'EXPIRED') {
    		return $scope.translation.myRecord.remarkExpired;
    	} else {
    		return "";
    	}
    };
    
    $scope.onEditViewBtnClick = function (rec) {
    	apiFactory.setRecruitmentApplicationId(rec.id);
    };	
	

}]);










