angular.module('vwApp').controller('testMobileController',['$http','$scope','$cookies','$rootScope','$location','$filter','$window','$localStorage','$state','$cookieStore','$timeout','$geolocation','homeService','testDriveService','localizationService','$analytics',function($http,$scope,$cookies,$rootScope,$location,$filter,$window,$localStorage, $state,$cookieStore,$timeout,$geolocation,homeService,testDriveService,localizationService,$analytics) {
	$analytics.pageTrack('/testDrive');
	maps = [];
	$scope.testdrive = {};
	$scope.province ="";	
	var map = new BMap.Map("dealer-map");
	var BASEDATA = [];
	var index =0;
	var dealerdata =[];
	var bdata=[];
	var map_address="Beijing";
	$scope.cit="";
	$scope.dealer_id = "";
	$scope.selctGender = "";
	$scope.maps =[];
	$scope.provinces =[];
	$scope.city = {city_code:'500002', city_name: 'Beijing', longitude:'116.404', latitude: '39.915', province_code: '10001', province_name: 'Beijing'}; // get the default city details using API
	$scope.province = {province_code: '10001', province_name: 'Beijing', city_code:'500002', city_name: 'Beijing', longitude:'116.404', latitude: '39.915'} ;// default province
	$scope.temp_province = [] ;
	$scope.final_province = [];
	$scope.cities =[];
	$scope.dealerTypes =[];
	$scope.carSeries = [];
	$scope.selectedmodel ="";
	$scope.privacyPolicyAccept =true;
	var lat = 116.404;
	 var lang = 39.915;
	$scope.class="clause radio selected";
	$scope.translate = function(){	
		localizationService.getBundle(function(data){
			$scope.translation = data;
		});			   	 
	};
	//$scope.translate();
	/*Baidu Map*/

	$scope.offlineOpts = {
			retryInterval: 10000,
			txt: 'Offline Mode'
	};

	
	$('select').chosen({
		disable_search:true,
	});
	
	$scope.carSeries = function()
	{
		testDriveService.getCarSeries(function (response){
			if(response.status.status==200){
				$scope.carSeries = response.entities;
				$scope.searchResult = dealerdata;
				$scope.searchResultCount = dealerdata.length;				
				//$('#car-model').trigger("chosen:updated");
			}
		});  
	};
	//$scope.carSeries();
	
	$scope.carModelTrigger = function (){		
		$analytics.eventTrack($scope.translation.mEventName.mSelectSeries, {  category: $scope.translation.mEventName.mTestDrive_appointment, label: $scope.translation.eventName.select });
	};
	
	$('a#btn-map').on('click',function(event) {
		event.preventDefault();
		
		var city = $("#city").val();
		if(city){
			//$scope.mapLoaded(maps);			
				//$('.testdrive-mobile-form').hide();
				$('#popup-map').show();								
		}else{
			alert('请选择城市');
		}
		
	});
	
	$('#popup-map').on('click','.popup-map-top a', function(event) {
		event.preventDefault();
		$('.testdrive-mobile-form').show();
		$('#popup-map').hide();
	});
	
$scope.getAllcityprovince = function () {
		
		homeService.getAllprovincesAndcities(function (response){
			if(response.status.status==200){
				$scope.provinces_cities = response.entities;
				//alert(angular.toJson($scope.provinces_cities));
				var prv= $scope.provinces_cities;
				//$scope.searchResult = dealerdata;
				//$scope.searchResultCount = dealerdata.length;
				for (var j = 0; j < prv.length; j++) {
					if($scope.temp_province.indexOf(prv[j].province_name) < 0) {
						$scope.temp_province.push(prv[j].province_name);
						$scope.provinces.push(prv[j]);
					}
				}				
				if(localStorage.getItem("city_name") != "undefined" && localStorage.getItem("city_name") != "" && localStorage.getItem("city_name") != null){
					$scope.setCity(localStorage.getItem("city_name"));
				} else {
				$scope.testdrive.province_code = $scope.provinces[0].province_code;
				if($scope.testdrive.province_code){
					$scope.changeCityList();
				}
				}
			}
		}); 
	};	
	//$scope.getAllcityprovince();

	$scope.dealerInfo = function() {
		//load all the cities and provinces
		$scope.translate();
		$scope.getAllcityprovince();
		$scope.carSeries();
		// detect user location and set default city and province and		
		
		//$scope.setMapOptions();
		
	};
	$scope.dealerInfo();
	
	$scope.setCity = function (cityName){		
		homeService.getCityByName(cityName, function (response){				
			if(response.status.status == 200){
				var cityObj = response.entity;
				console.log(response);
				console.log(cityObj);
				$scope.setSelectedCityProvince(cityObj.code);
			}
		});		
	};	
	
	$scope.setSelectedCityProvince = function (city_code) {		
		var allCities = $scope.provinces_cities;
		
		for (var i = 0; i < allCities.length; i++) {
			if (allCities[i].city_code === city_code) {				
					$scope.testdrive.city_code = allCities[i].city_code; // assigning the first city as default						
				$scope.city = allCities[i];
				break;
			}
		}
		var selCity = $scope.city;		
		$scope.searchDealer();
		$scope.setSelectedProvince(selCity.province_code);
	};
	
	$scope.setSelectedProvince = function (province_code) {
		var uniqueProvinces = $scope.provinces;
		
		for (var i = 0; i < uniqueProvinces.length; i++) {
			if (uniqueProvinces[i].province_code === province_code) {
				$scope.testdrive.province_code = uniqueProvinces[i].province_code;
				$scope.province = uniqueProvinces[i];
				break;
			}
		}
		
		$scope.populateCityForSelectedProvince();
	};	
	
	$scope.changeCityList = function() {	
		$analytics.eventTrack($scope.translation.mEventName.mSelectDealerProvinces, {  category: $scope.translation.mEventName.mTestDrive_appointment, label: $scope.translation.eventName.select });
		$scope.populateCityForSelectedProvince();
		if($scope.cities){
			$scope.city = $scope.cities[0];				
			$scope.testdrive.city_code = $scope.cities[0].city_code; // assigning the first city as default				
			$scope.searchDealer();			
		}		
	};
	
	
	$scope.populateCityForSelectedProvince = function() {		
		var allCities = $scope.provinces_cities;
		console.log("Selected Province>>>>  ");
		console.log($scope.testdrive.province_code);
		$scope.cities = [];
		homeService.getDealerByProvince($scope.testdrive.province_code,function (response){
			if(response.status.status==200){
				dealerdata = response.entities;
			}
		});
		for (var i = 0; i < allCities.length; i++) {
			if (allCities[i].province_code === $scope.testdrive.province_code) {
				console.log("Adding  city >>>>  " + i);
				console.log(allCities[i]);
				$scope.cities.push(allCities[i]);
			}
		}
	};
	
	$scope.searchDealer = function (){	
		$analytics.eventTrack($scope.translation.mEventName.mSelectDealerCity, {  category: $scope.translation.mEventName.mTestDrive_appointment, label: $scope.translation.eventName.select });
		homeService.getdealerCityProvincefilter($scope.testdrive.city_code, function (response){
			if(response.status.status==200){
				dealerdata = response.entities;
				$scope.dealers =dealerdata;	
				if($scope.dealers){					
					if(localStorage.getItem("dealer_id") != "undefined" && localStorage.getItem("dealer_id") != "" && localStorage.getItem("dealer_id") != null){						
						for (var i = 0; i < dealerdata.length; i++) {							
							if(dealerdata[i].id == localStorage.getItem("dealer_id")){								
								$scope.testdrive.dealer_id = dealerdata[i].id;								
								break;
							}
							if(dealerdata[dealerdata.length - 1].id  != localStorage.getItem("dealer_id")){
								if(dealerdata && dealerdata.length > 0){
									$scope.testdrive.dealer_id = dealerdata[0].id;
									}
								}
							}
							
						} else {
							if($scope.dealers && $scope.dealers.length > 0){
						$scope.testdrive.dealer_id = $scope.dealers[0].id;	
							}	
					}
				}else{
				$scope.testdrive.dealer_id="";	
				}
				$scope.searchResult = dealerdata;	
				if($scope.cities){					
					for ( var j = 0; j < $scope.cities.length ; j++){						
						if($scope.cities[j].city_code == $scope.testdrive.city_code){
							$scope.city = $scope.cities[j];	
							if($scope.city){
							lat = $scope.city.latitude;
							lang = $scope.city.longitude;
							map_address=$scope.city.city_name;
							} else {
								
								 lat = 116.404;
								  lang = 39.915;	
								  map_address="Beijing";
							}
							break;
						}						
					}
				}				
				$scope.mapdata();							
			}
			else
			{
				$scope.mapdata();
			}
		}); 
	};
	
	$scope.dealerDataId = function (id){		
		if(id) {			
			$scope.mapdata();
			localStorage.removeItem("city_name");
			localStorage.removeItem("dealer_id");
		}		
	};	
	
	
	$scope.mapdata = function()
	{		
		//map.centerAndZoom(new BMap.Point(lat, lang), 12); 	
		map.centerAndZoom(map_address,12);
		//map.centerAndZoom($scope.city.city_name, 10);
		//map.setCurrentCity("Beijing");          // 设置地图显示的城市 此项是必须设置的
		map.enableScrollWheelZoom(true);
		map.clearOverlays();
		//alert(dealerdata.length);
		for (var i = 0; i <dealerdata.length; i++) {			
			var json = dealerdata[i];
			var p0 = json.lattitude;
			var p1 = json.longitude;
			var point = new BMap.Point(p1, p0);
			var myIcon = new BMap.Icon("https://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25), {
				offset: new BMap.Size(10, 25),
				imageOffset: new BMap.Size(0, 0 - i * 25)
			});
			var marker = new BMap.Marker(point, { icon: myIcon });
			var iw = createInfoWindow(i);
			var label = new BMap.Label(json.full_name, { "offset": new BMap.Size("http://static.blog.csdn.net/images/medal/holdon_s.gif".lb - "http://static.blog.csdn.net/images/medal/holdon_s.gif".x + 10, -20) });
			marker.setLabel(label);
			map.addOverlay(marker);
			label.setStyle({
				color : "#f00", 
				fontSize : "10px", 
				backgroundColor :"0.05",
				border :"0", 
				fontWeight :"bold" 
			});
			(function() {
				var index = i;
				var _iw = createInfoWindow(i);
				var _marker = marker;
				_marker.addEventListener("click", function() {
					this.openInfoWindow(_iw);
				});
				_iw.addEventListener("open", function() {
					_marker.getLabel().hide();
				})
				_iw.addEventListener("close", function() {
					_marker.getLabel().show();
				})
				label.addEventListener("click", function() {
					_marker.openInfoWindow(_iw);
				})
				if (!!json.isOpen) {
					label.hide();
					_marker.openInfoWindow(_iw);
				}
			})()		
	}
	//Create a marker popup window
	function createInfoWindow(i) {
		var opts = {
				width : 550,     // 信息窗口宽度
				height: 300,     // 信息窗口高度
				//title : "信息窗口" , // 信息窗口标题
				enableMessage:true//设置允许信息窗发送短息
			};
       
		var json = dealerdata[i];
		 var deta = JSON.stringify(json);
		var content = "<p style='font-size:18px;text-align:center;'>"+(i+1)+"."+dealerdata[i]['full_name']+"</p>" +
		"<p style='font-size:16px;line-height:28px;text-align:center;'>地址:"+dealerdata[i]['address']+"</p>"+
		"<div style='border-top:1px solid #dee1e3;text-align:center;mragin-top:10px;padding: 10px 0;'><p>销售热线:<span style='color:#1597d9;'>"+dealerdata[i]['phone']+"</span></p>" +
		"<a href='javascript:;' value='"+dealerdata[i]['full_name']+"' class='btn-dealer-info' style='color:#ffffff;background-color:#019ada;width:140px;height:42px;border-radius:3px;text-align:center;display:block;line-height:42px;font-size:18px;margin:0 auto;margin-top:15px;' onclick='sendSelectedDealerId("+dealerdata[i].id+")'>选择此经销商</a></div>";
		var iw = new BMap.InfoWindow(content, opts);
		
		return iw;
	}
	//Create a Icon
	function createIcon(json) {
		var icon = new BMap.Icon("http://dev.baidu.com/wiki/static/map/API/img/ico-marker.gif", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowAnchor:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})  
		return icon; 
	}
	};
	
	$scope.selectDealer = function (){		
		if(localStorage.getItem("selected_dealer_id") && $scope.dealers.length > 0){
			var selected_dealer_id = localStorage.getItem("selected_dealer_id");
			for(var i = 0 ; i < $scope.dealers.length ; i++){
				if($scope.dealers[i].id == selected_dealer_id){
					$scope.testdrive.dealer_id = $scope.dealers[i].id;
					localStorage.removeItem("selected_dealer_id");
					$('#popup-map').hide();	
					break;
				}				
			}
		}
	};
	
	
	
	//Create a Iconi
	function createIcon(json) {
		var icon = new BMap.Icon("http://dev.baidu.com/wiki/static/map/API/img/ico-marker.gif", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowAnchor:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})  
		return icon; 
	}



	$(".testdrive-mobile-form span.sex").on('click', function(event) {
		event.preventDefault();
		var kids = $( event.target ).children().context.id;
		$scope.selctGender = kids;
		if($(this).hasClass('selected')) return;
		$(this).addClass('selected').siblings('span.sex').removeClass('selected')
	});
	/*$(".testdrive-mobile-form span.radio").unbind().click(function(){
		if($(this).hasClass('selected')) {
			$(this).removeClass('selected')
		}else{
			$(this).addClass('selected')
		}
	});*/
	$scope.pageAcceptForm = function (bol){		
		$analytics.eventTrack($scope.translation.mEventName.mPrivacyPolicyStatement, {  category: $scope.translation.mEventName.mTestDrive_appointment, label: $scope.translation.eventName.browse });
		if($scope.class === 'clause radio selected'){			
			$scope.class="clause radio ";
			$scope.privacyPolicyAccept =false;
		} else {			
			$scope.class="clause radio selected"
				$scope.privacyPolicyAccept =true;
		}
	};
	$('.line.clause').on('click', 'a', function(event) {
		event.preventDefault();
		$('#popup-clause').show();
	});
	$('#popup-ok').on('click', 'a.btn', function(event) {
		$(this).parents('#popup-ok').hide();
		//$('select option[value=""]').prop('selected',true).trigger('chosen:updated');
		//$('input').val('');
	});
	$('.popup').on('click', 'a.popup-close', function(event) {
		$('#popup-clause').hide();
	});

	function isPhoneNum(val){
		var phonenum = val;
		var myreg = /^(((1[0-9]{0})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{10})$/; 
		/*var myreg = /^[0-9]*$/;*/
		if(!myreg.test(phonenum)){ 
			return false; 
		}else{
			return true;
		}
	}
	function isAlphabet(namec) {
	    var isValid = false;
	    var regex = /^[a-zA-Z ]+$/;
	    isValid = regex.test(namec);
	    return isValid;
	}
	
	$('#dealer').parents('.dealer').on('change' , 'select.dealer', function(event){		
		$analytics.eventTrack($scope.translation.mEventName.mSelectDealer, {  category: $scope.translation.mEventName.mTestDrive_appointment, label: $scope.translation.eventName.select });
	});
	
	$scope.nameTrigger = function(){
		$analytics.eventTrack($scope.translation.mEventName.mEnterYourName, {  category: $scope.translation.mEventName.mTestDrive_appointment, label: $scope.translation.eventName.editProfile });
	};
	
	$scope.phoneTrigger = function(){		
		$analytics.eventTrack($scope.translation.mEventName.mEnterYourPhoneNumber, {  category: $scope.translation.mEventName.mTestDrive_appointment, label: $scope.translation.eventName.editProfile });
	};	
	
	$('.sex').parents('.line').on('click' , 'span.sex', function(event){			
		$analytics.eventTrack($scope.translation.mEventName.mGenderSelection, {  category: $scope.translation.mEventName.mTestDrive_appointment, label: $scope.translation.eventName.editProfile });
	});		
	
	$scope.purchaseTrigger = function (){		
		$analytics.eventTrack($scope.translation.mEventName.mExpectedTimeOfPurchase, {  category: $scope.translation.mEventName.mTestDrive_appointment, label: $scope.translation.eventName.select });
	};	
	
	$scope.submitTestDrive = function (data){
		$analytics.eventTrack($scope.translation.mEventName.mSubmit, {  category: $scope.translation.mEventName.mTestDrive_appointment, label: $scope.translation.eventName.submit });
		var i = 0;
		for (var selector in data) {
			if(data[selector]==''){
				if(!$('.testdrive-form div.line').eq(i).hasClass('must')) return;
				$('.testdrive-form div.line').eq(i).addClass('warning');
			}else{
				$('.testdrive-form div.line').eq(i).removeClass('warning');
			}
			i++;
	    };
	    if(data.name){
		    if(!isAlphabet(data.name)){
		    	$('#name').parents('.line').addClass('warning');
		    } else {
		    	$('#name').parents('.line').removeClass('warning');
		    }
		    } else {
		    	$('#name').parents('.line').addClass('warning');
		    }
	    if(!isPhoneNum(data.mobile_no) || data.mobile_no.isEmpty){
	    	$('#phone').parents('.line').addClass('warning');
	    }else{
	    	$('#phone').parents('.line').removeClass('warning');
		}
	    if(!data.series_code){	    	
	    	$('#car-model').parents('.line').addClass('warning');
	    } else {
	    	$('#car-model').parents('.line').removeClass('warning');	    	 
	    }
	    if(!data.dealer_id){
	    	$('#dealer').parents('.line').addClass('warning');
	    } else {
	    	$('#dealer').parents('.line').removeClass('warning');
	    }
		var PolicyAccept = $scope.privacyPolicyAccept;		
		if(PolicyAccept)
			{
		if(data.series_code != '' && data.dealer_id != '' && data.name != ''  && data.mobile_no !='' && isPhoneNum(data.mobile_no) && isAlphabet(data.name)){
			$scope.data= $scope.testdrive;
			$scope.data.name = data.name;
			$scope.data.dealer_id = data.dealer_id;
			$scope.data.dealer_city_code = data.city_code;
			$scope.data.dealer_province_code = data.province_code;
			$scope.data.updated_by = 1;
			$scope.data.series_code = data.series_code;
			$scope.data.plan_buy_time = data.plan_buy_time;
			$scope.data.mobile_no = data.mobile_no;
			$scope.data.gender = $scope.selctGender;
			$scope.selectedmodel = $("#car-model option:selected").text();
			$("#series_name").innerHTML=$scope.selectedmodel;
			if($scope.selctGender=="")
			{
				$scope.data.gender = $scope.translation.testDrive.Male;
			}
			testDriveService.registerTestDrive($scope.data,function (response){
				if(response.data.status.status==200)
				{
					$analytics.eventTrack($scope.translation.mEventName.mComplete, {  category: $scope.translation.mEventName.mTestDrive_booking, label: $scope.translation.eventName.submit });
					$scope.testdrive = {};
					$scope.getAllcityprovince();
					$scope.class="clause radio selected";
					$('#popup-ok').show();
				}
			
			});
		
	    }
		/*var i = 0;
		for (var selector in data) {
			if(data[selector]==''){
				if(!$('.testdrive-form div.line').eq(i).hasClass('must')) return;
				$('.testdrive-form div.line').eq(i).addClass('warning');
			}else{
				$('.testdrive-form div.line').eq(i).removeClass('warning');
			}
			i++;
	    };
	    if(!isPhoneNum(data.mobile_no) || data.mobile_no.isEmpty){
	    	$('#phone').parents('.line').addClass('warning');
	    }
	    if(!isAlphabet(data.name)){
	    	$('#name').parents('.line').addClass('warning');
	    }*/
			}
		else
			{
				//alert("accept terms and conditions");
				alert($scope.translation.testDrive.termsAndCondition);
			}
	
	};

/*	$('button#submit').unbind().click(function(){
		var data = {
			arModel : $('#car-model').val(),
			dealer : $('#dealer').val(),
			name : $('#name').val(),
			sex : $('span.sex').attr('val'),
			province : $('#province').val(),
			city : $('#city').val(),
			phone : $('#phone').val(),
			time : $('#time').val(),
			planByTime : $('#timeofpurchase').val()
		}
		
		if(data.arModel != '' && data.dealer != '' && data.name != ''  && data.phone !='' && isPhoneNum(data.phone) && isAlphabet(data.name)){
			$scope.data= $scope.testdrive;
			$scope.data.name = data.name;
			$scope.data.dealer_id = data.dealer;
			$scope.data.dealer_city_code = data.city;
			$scope.data.dealer_province_code = data.province;
			$scope.data.updated_by = 1;
			$scope.data.series_code = data.arModel;
			$scope.data.plan_buy_time = data.planByTime;
			$scope.data.mobile_no = data.phone;
			$scope.data.gender = $scope.selctGender;
			$scope.selectedmodel = $("#car-model option:selected").text();
			if($scope.selctGender=="")
			{
				$scope.data.gender = $scope.translation.testDrive.Male;
			}
			testDriveService.registerTestDrive($scope.data,function (response){
				if(response.data.status.status==200)
				{

					$('#popup-ok').show();
				}
			
			});
		
	    }
		var i = 0;
		for (var selector in data) {
			if(data[selector]==''){
				if(!$('.testdrive-mobile-form div.line').eq(i).hasClass('must')) return;
				$('.testdrive-mobile-form div.line').eq(i).addClass('warning');
			}else{
				$('.testdrive-mobile-form div.line').eq(i).removeClass('warning');
			}
			i++;
	    };
	    if(!isPhoneNum(data.phone) || data.phone.isEmpty){
	    	$('#phone').parents('.line').addClass('warning');
	    }
	    if(!isAlphabet(data.name)){
	    	$('#name').parents('.line').addClass('warning');
	    }
	});*/
/*	$scope.privacy = function()
	{
		$('#popup-clause').show();
	};*/
	$scope.privacyhide =function()
	{
		$('#popup-clause').hide();
	};
}]);