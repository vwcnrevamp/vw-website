angular.module('vwApp').controller('searchMobileController',['$http','$scope','$cookies','$rootScope','$location','$filter','$window','$localStorage','$state','$cookieStore','$timeout','$geolocation','homeService','localizationService','$analytics',function($http,$scope,$cookies,$rootScope,$location,$filter,$window,$localStorage, $state,$cookieStore,$timeout,$geolocation,homeService,localizationService,$analytics) {

	$analytics.pageTrack('/searchDealer');
	var dealerdata =[];
	maps = [];
	var markers = [];
	/**
	 * All the provinces and cities
	 */
	var map_address = "Beijing";
	$scope.provinces_cities = [];
	$scope.provinces =[];
	$scope.cities =[];
	$scope.dealerTypes =[];
	var map = new BMap.Map("dealer-map");
	$scope.city = {city_code:'500002', city_name: 'Beijing', longitude:'116.404', latitude: '39.915', province_code: '10001', province_name: 'Beijing'}; // get the default city details using API
	$scope.province = {province_code: '10001', province_name: 'Beijing', city_code:'500002', city_name: 'Beijing', longitude:'116.404', latitude: '39.915'} ;// default province
	$scope.dealerType = {};	
	$scope.selectedClass="";
	$scope.temp_province = [];
	$scope.deal = -1;
	$scope.location = true;
	var lat = 116.404;
	 var lang = 39.915;	
	$scope.offlineOpts = {
			retryInterval: 10000,
			txt: 'Offline Mode'
	};
	var opts = {
			width : 590,     // 信息窗口宽度
			height: 300,     // 信息窗口高度
			//title : "信息窗口" , // 信息窗口标题
			enableMessage:true//设置允许信息窗发送短息
		};
	$scope.translate = function(){	
		localizationService.getBundle(function(data){
			$scope.translation = data;
		});			   	 
	};
	$scope.translate();

	$('select').chosen({
		disable_search:true,
	});
	
	$('a#btn-map').on('click',function(event) {
		event.preventDefault();
		
		var city = $scope.city.city_name;		
		if(city){
			$('#popup-map').show();
			//$('.dealer-mobile-form').hide();
			//map.centerAndZoom(city,11); //跳转城市地图

		}else{
			$('#popup-location').show();
		}
		
	});
	
	$('#btn-map-menu').on('click', function(event) {
		event.preventDefault();
		var obj = $(this).parent('.map-menu');
		if(obj.hasClass('open')){
			obj.removeClass('open');
		}else{
			obj.addClass('open');
		}
		
	});
	
	$('#popup-map').on('click','.popup-map-top a', function(event) {
		event.preventDefault();
		$('.dealer-mobile-form').show();		
		$('#popup-map').hide();
	});
	
	
	$scope.getAllcityprovince = function () {
		
		homeService.getAllprovincesAndcities(function (response){
			if(response.status.status==200){
				$scope.provinces_cities = response.entities;
				var prv= $scope.provinces_cities;
				$scope.searchResult = dealerdata;
				$scope.searchResultCount = dealerdata.length;
				for (var j = 0; j < prv.length; j++) {
					if($scope.temp_province.indexOf(prv[j].province_name) < 0) {
						$scope.temp_province.push(prv[j].province_name);
						$scope.provinces.push(prv[j]);
					}
				}	
				$scope.province = $scope.provinces[0];				
				if($scope.province){										
					$scope.changeCityList();
				}
			}
		}); 
	};
	
	$scope.changeCityList = function() {		
		$analytics.eventTrack($scope.translation.mEventName.mProvince, {  category: $scope.translation.mEventName.mQuery_main, label: $scope.translation.eventName.select });
		$scope.populateCityForSelectedProvince();
		if($scope.cities){
			$scope.city = $scope.cities[0]; // assigning the first city as default			
			//localStorage.setItem("city_name",$scope.cities[0].city_name);			
			$scope.searchDealer();
		}		
	};
	
	
	$scope.populateCityForSelectedProvince = function() {
		var allCities = $scope.provinces_cities;
		console.log("Selected Province>>>>  ");
		console.log($scope.province);
		$scope.cities = [];
		for (var i = 0; i < allCities.length; i++) {
			if (allCities[i].province_code === $scope.province.province_code) {
				console.log("Adding  city >>>>  " + i);
				console.log(allCities[i]);
				$scope.cities.push(allCities[i]);
			}
		}
	};
	
	$scope.setSelectedCityProvince = function (city_code) {
		var allCities = $scope.provinces_cities;
		
		for (var i = 0; i < allCities.length; i++) {
			if (allCities[i].city_code === city_code) {
				$scope.city = allCities[i];							
				break;
			}
		}
		var selCity = $scope.city;	
		$scope.searchDealer();
		$scope.setSelectedProvince(selCity.province_code);
	};
	
	$scope.setSelectedProvince = function (province_code) {
		var uniqueProvinces = $scope.provinces;
		
		for (var i = 0; i < uniqueProvinces.length; i++) {
			if (uniqueProvinces[i].province_code === province_code) {
				$scope.province = uniqueProvinces[i];
				break;
			}
		}
		
		$scope.populateCityForSelectedProvince();
	};
	
	$scope.getDealerTypeData = function() {
		
		homeService.getAlldealerType(function (response){
			if(response.status.status==200){				
				$scope.dealerTypes = response.entities;				
			}
		});
	};
	
	$scope.dealerInfo = function() {
		//load all the cities and provinces
		$scope.translate();
		$scope.getAllcityprovince();
		$scope.getDealerTypeData();
		// detect user location and set default city and province and 
		
		
		
		//showlocation(setUserLocation, setDefaultLocation);	
		//$scope.setMapOptions();
		
	};
	$scope.dealerInfo(); 
	
	$scope.setCity = function (cityName){
		homeService.getCityByName(cityName, function (response){				
			if(response.status.status == 200){
				var cityObj = response.entity;				
				console.log(response);
				console.log(cityObj);
				$scope.setSelectedCityProvince(cityObj.code);
			}
		});		
	};
	
	
	
	
	
	$scope.searchDealer = function () {
		$analytics.eventTrack($scope.translation.mEventName.mCity, {  category: $scope.translation.mEventName.mQuery_main, label: $scope.translation.eventName.select });
		$scope.searchResult = [];
		var selectPrv, selectCity, selectDType = null;
		console.log("In search Dealer ======  ");
		if($scope.province) {
			selectPrv = $scope.province.province_code; 
			console.log(selectPrv);
		}
		if($scope.city) {
			selectCity = $scope.city.city_code;
			//localStorage.setItem("city_name",$scope.city.city_name);			
			if($scope.city){
				map_address = $scope.city.city_name;
				lat = $scope.city.latitude;
				lang = $scope.city.longitude;
				console.log(lat+ "  " +lang);				
				}
		}
		if($scope.dealerType) {
			selectDType = $scope.dealerType.code;
			console.log(selectDType);
		}	
		
		console.log("=======================  ");
		
		homeService.getdealerCityProvincedealerTypefilter(selectPrv, selectCity, selectDType, 
				function (response){
					if(response.status.status==200 && response.entities){
						dealerdata = response.entities;
						$scope.searchResult = dealerdata;
						$scope.searchResultCount = $scope.searchResult.length;
						$scope.mapdata();
					}				
			}); 
	};
	
	$scope.dealerTypeTrigger = function (){
		$analytics.eventTrack($scope.translation.mEventName.mDealerType, {  category: $scope.translation.mEventName.mQuery_main, label: $scope.translation.eventName.select });
	};
	
	function addClickHandler(content,marker){		
		marker.addEventListener("click",function(e){
			$analytics.eventTrack($scope.translation.mEventName.mSelectDealerNumer, {  category: $scope.translation.mEventName.mQuery_main, label: $scope.translation.eventName.select });
			openInfo(content,e);
			}
		);
	}
	
	function openInfo(content,e){		
		var p = e.target;
		var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
		var infoWindow = new BMap.InfoWindow(content,opts);  // 创建信息窗口对象 
		map.openInfoWindow(infoWindow,point); //开启信息窗口
	}
	
	$scope.dealerDataId = function (id){	
		$analytics.eventTrack($scope.translation.mEventName.mGoHere, {  category: $scope.translation.mEventName.mQuery_main, label: $scope.translation.eventName.select });
		if(id) {		
			$('#popup-map').show();			
			var m = markers[id];			
			var point = new BMap.Point(m.dealer.longitude, m.dealer.lattitude);
			var infoWindow = new BMap.InfoWindow(m.content,opts);  // 创建信息窗口对象 
			map.openInfoWindow(infoWindow,point); //开启信息窗口					
		}		
	};
	
	$scope.dealerInfoDataId = function (id){	
		$analytics.eventTrack($scope.translation.mEventName.mGoHere, {  category: $scope.translation.mEventName.mQuery_main, label: $scope.translation.eventName.select });
		if(id) {					
			$scope.closePopup();
			var m = markers[id];			
			var point = new BMap.Point(m.dealer.longitude, m.dealer.lattitude);
			var infoWindow = new BMap.InfoWindow(m.content,opts);  // 创建信息窗口对象 
			map.openInfoWindow(infoWindow,point); //开启信息窗口					
		}		
	};
	
	$scope.mapdata = function()
	{		
		//map.centerAndZoom(new BMap.Point(lat, lang), 11); 
		map.centerAndZoom(map_address, 11);		
		map.enableScrollWheelZoom(true);
		map.clearOverlays();		
		var ind = 0;
		for (var i = 0; i <dealerdata.length; i++) {			
			var json = dealerdata[i];
			var deta = JSON.stringify(json);
			var p0 = json.lattitude;
			var p1 = json.longitude;
			var point = new BMap.Point(p1, p0);
			var myIcon = new BMap.Icon("https://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25), {
				offset: new BMap.Size(10, 25),
				imageOffset: new BMap.Size(0, 0 - i * 25)
			});
			var marker = new BMap.Marker(point, { icon: myIcon });			
			var label = new BMap.Label(json.full_name, { "offset": new BMap.Size("http://static.blog.csdn.net/images/medal/holdon_s.gif".lb - "http://static.blog.csdn.net/images/medal/holdon_s.gif".x + 10, -20) });
			marker.setLabel(label);
			map.addOverlay(marker);
			label.setStyle({
				color : "#f00", 
				fontSize : "10px", 
				backgroundColor :"0.05",
				border :"0", 
				fontWeight :"bold" 
			});		
			
			var sContent = "<p style='font-size:18px;text-align:center;'>"+(i+1)+"."+dealerdata[i]['full_name']+"</p>" +
			"<p style='font-size:16px;line-height:28px;text-align:center;'>地址:"+dealerdata[i]['address']+"</p>"+
			"<div style='border-top:1px solid #dee1e3;text-align:center;mragin-top:10px;padding: 10px 0;'><p>销售热线:<a color='#019ada' href='tel:"+dealerdata[i]['phone']+"'>"+dealerdata[i]['phone']+"</a></p><a href='javascript:;' value='"+dealerdata[i]['full_name']+"' class='btn-dealer-info' style='color:#ffffff;background-color:#019ada;width:140px;height:42px;border-radius:3px;text-align:center;display:block;line-height:42px;font-size:18px;margin:0 auto;margin-top:15px;' onclick='showDealerInfo("+i+","+deta+")' id='"+i+"' did='"+(i+1)+"'>更多信息</a></div>";
			addClickHandler(sContent,marker);
			ind++;
			markers[dealerdata[i].id] = { dealer : dealerdata[i], content : sContent};
		
	}
	
	
	//Create a Icon
	function createIcon(json) {		
		var icon = new BMap.Icon("http://dev.baidu.com/wiki/static/map/API/img/ico-marker.gif", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowAnchor:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})  
		return icon; 
	}
	};
	
	/*$scope.mapdata = function()
	{		
		//alert(lat + "  " + lang);
		//map.centerAndZoom(new BMap.Point(lat, lang), 12); 	
		//alert(map_address);
		map.centerAndZoom(map_address,12);
		//map.setCurrentCity("Beijing");          // 设置地图显示的城市 此项是必须设置的
		map.enableScrollWheelZoom(true);		
		for (var i = 0; i <dealerdata.length; i++) {
			var json = dealerdata[i];
			var p0 = json.lattitude;
			var p1 = json.longitude;
			var point = new BMap.Point(p1, p0);
			var myIcon = new BMap.Icon("https://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25), {
				offset: new BMap.Size(10, 25),
				imageOffset: new BMap.Size(0, 0 - i * 25)
			});
			var marker = new BMap.Marker(point, { icon: myIcon });
			var iw = createInfoWindow(i);
			var label = new BMap.Label(json.full_name, { "offset": new BMap.Size("http://static.blog.csdn.net/images/medal/holdon_s.gif".lb - "http://static.blog.csdn.net/images/medal/holdon_s.gif".x + 10, -20) });
			marker.setLabel(label);
			map.addOverlay(marker);
			label.setStyle({
				color : "#f00", 
				fontSize : "10px", 
				backgroundColor :"0.05",
				border :"0", 
				fontWeight :"bold" 
			});
			(function() {
				var index = i;
				var _iw = createInfoWindow(i);
				var _marker = marker;
				_marker.addEventListener("click", function() {
					this.openInfoWindow(_iw);
				});
				_iw.addEventListener("open", function() {
					_marker.getLabel().hide();
				})
				_iw.addEventListener("close", function() {
					_marker.getLabel().show();
				})
				label.addEventListener("click", function() {
					_marker.openInfoWindow(_iw);
				})
				if (!!json.isOpen) {
					label.hide();
					_marker.openInfoWindow(_iw);
				}
			})()
		}
	
	//Create a marker popup window
		function createInfoWindow(i) {
			var opts = {
					width : 550,     // 信息窗口宽度
					height: 300,     // 信息窗口高度
					//title : "信息窗口" , // 信息窗口标题
					enableMessage:true//设置允许信息窗发送短息
				};
	       
			var json = dealerdata[i];
			var deta = JSON.stringify(json);			 
			 var content = "<p style='font-size:18px;text-align:center;'>"+(i+1)+"."+dealerdata[i]['full_name']+"</p>" +
				"<p style='font-size:16px;line-height:28px;text-align:center;'>地址:"+dealerdata[i]['address']+"</p>"+
				"<div style='border-top:1px solid #dee1e3;text-align:center;mragin-top:10px;padding: 10px 0;'><p>销售热线:<a color='#019ada' href='tel:"+dealerdata[i]['phone']+"'>"+dealerdata[i]['phone']+"</a></p><a href='javascript:;' value='"+dealerdata[i]['full_name']+"' class='btn-dealer-info' style='color:#ffffff;background-color:#019ada;width:140px;height:42px;border-radius:3px;text-align:center;display:block;line-height:42px;font-size:18px;margin:0 auto;margin-top:15px;' onclick='showDealerInfo("+i+","+deta+")' id='"+i+"' did='"+(i+1)+"'>更多信息</a></div>";
			var content = "<p style='font-size:18px;'>"+(i+1)+"."+dealerdata[i]['full_name']+"</p>" +
			"<p style='font-size:16px;height:28px;line-height:28px;'>"+dealerdata[i]['address']+"</p>"+
			"<div style='border-top:1px solid #dee1e3;margin-top:15px;padding-top:24px;overflow: hidden;'>" +
			"<div style='float:left;border-right:1px solid #dee1e3;padding-right:20px;'><p style='height:28px;line-height:28px;font-size:16px;width:190px;'>" +
			"销售热线:<span style='color:#1597d9;'>"+dealerdata[i]['sales_hotline']+"</span></p><a href='#' style='color:#1597d9;margin-top:30px;display:block;background:url(angularweb/assets/images/icon-arrow1.png) center left no-repeat;padding-left:24px;font-size:18px;text-decoration: underline;'>" +
					"进入经销商首页</a><a href='#' style='color:#ffffff;background-color:#019ada;width:148px;height:42px;border-radius:3px;text-align:center;margin-top:30px;display:block;line-height:42px;font-size:18px;' onclick='sendDealerId("+dealerdata[i].id+")'>预约试驾</a></div><div style='float:left;padding-left:20px;'>" +
					"<img src='angularweb/assets/images/dealer-popup-img.jpg' /><div style='margin-top:12px;text-align:center;height:64px; line-height:44px;color:#1597d9;font-size:18px;position: relative;'><input type='tel' id='tel"+i+"' maxlength='11' placeholder='短信获取经销商信息' style='float:left;border:1px solid #dee1e3;background-color:#ffffff;width:229px;height:44px;-webkit-appearance:none;line-height:44px;border-radius:3px;padding:0px 12px;'>" +
					"<a  style='color:#ffffff;background-color:#019ada;width:78px;height:42px;border-radius:3px;text-align:center;display:block;line-height:42px;font-size:18px;float:left;margin-left:10px;' onclick='sendInfo("+i+","+deta+")'>发送</a><p class='warning'></p></div></div></div>";
			var iw = new BMap.InfoWindow(content, opts);
			
			return iw;
		}
	//Create a Icon
	function createIcon(json) {
		var icon = new BMap.Icon("http://dev.baidu.com/wiki/static/map/API/img/ico-marker.gif", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowAnchor:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})  
		return icon; 
	}
	};*/
	
	$scope.selectDealer = function (){
		$analytics.eventTrack($scope.translation.mEventName.mMoreInformation, {  category: $scope.translation.mEventName.mDealer_float, label: $scope.translation.eventName.select });
		$scope.sn = localStorage.getItem("i");
		$scope.obj = localStorage.getItem("obj");		
		$scope.obj = JSON.parse($scope.obj);
		var $info = $('#map-dealer-info');
		//var html = '<p class="name">'+sn+'.'+obj.full_name+'</p><p class="address">地 址:'+obj.address+'</p><p class="phone">销售热线:<a href="tel:'+obj.phone+'">'+obj.phone+'</a></p><div class="icon"></div>'+dealerOtherInfo();
		$info.addClass('show').find('.info').show();
	};
	
	
	
	function dealerOtherInfo(){
		var html = '<div class="other"><div class="distance">距离<span>0</span>公里<a href="javascript:;" class="btn-go">到这去</a></div><a href="#" class="btn-testdrive">预约试驾</a><div class="code"><input type="tel" maxlength="11" id="tel" placeholder="短信获取经销商信息"><a href="javascript:;" class="btn-send" onclick="sendInfo(this)";>发送</a><p class="warning">手机号码格式不正确</p></div><img src="images/dealer-popup-img.jpg" class="dealer-pic"><a href="#" class="btn-dealer-index">进入经销商首页</a></div>';
		return html;
	}
		
	$scope.getBgClass = function (row){
		if(row%2 == 1){
			return "li-bg";
		} else {
			return "";
		}
	};	
	
	 $scope.sendDealerId = function(id){
		$analytics.eventTrack($scope.translation.mEventName.mArrangeATestDrive, {  category: $scope.translation.mEventName.mQuery_main, label: $scope.translation.eventName.select }); 
		localStorage.setItem("city_name",$scope.city.city_name); 
		localStorage.setItem("dealer_id" , id);			
	  };
	
	 $scope.sendInfo = function(index,obj){	   
		 //alert("index popup page");
		$analytics.eventTrack($scope.translation.mEventName.mEnterThePhoneNumberToSend, {  category: $scope.translation.mEventName.mQuery_main, label: $scope.translation.eventName.select }); 
        var phone = $('input#tel'+index).val();
        var data = JSON.stringify(obj); 
        var $o = $('.smsDiv');
		 var html = $o.html();
	     if(isPhoneNum($('input#tel'+index).val())){
           jQuery.ajax({
             url: 'smsdealer',
             type: 'POST',
             data: { id:obj.id,DesNo: phone,Msg :  data },
             success: function( response ) {
           	  if(response.status === null)
           		  {
           		  $('.smsDiv').text('短信发送成功');
           		  }
           	 if(response.status.status === 200)
           		  {
           		  $('.smsDiv').text('短信发送成功');
           		  setTimeout(function(){
           				$o.html(html);
           			},3000)	;
           		  if(console){
 						console.log("SMS sent successfully");
 					}
           		  }
             },
             error: function(){
           	  alert("not sent");
             }
             });
         }else{
			$('input#tel'+index).parent('div').addClass('warning');
	     	$('input#tel'+index).css('border-color','#c92e2c');
	     	$('p#warn'+index).css('display','');
	    }
	  };
	  
	$scope.closePopup = function (){
		var $info = $('#map-dealer-info');		
		$info.removeClass('show').find('.info').hide();
		localStorage.removeItem("i");
		localStorage.removeItem("obj");
	}; 	
	
	$scope.phoneTrigger = function (){		
		$analytics.eventTrack($scope.translation.mEventName.mInputMobilePhoneNumber, {  category: $scope.translation.mEventName.mQuery_main, label: $scope.translation.eventName.editProfile });
	};
	
	$scope.getSelectedClass = function (row){	
		$analytics.eventTrack($scope.translation.mEventName.mSelectDealerInformation, {  category: $scope.translation.mEventName.mQuery_main, label: $scope.translation.eventName.select });
		if($scope.deal != row){
			$scope.deal = row;	
		} else {
			$scope.deal = -2;	
		}
	};
	
	$('.line.clause').on('click', 'a', function(event) {
		event.preventDefault();
		$('#popup-clause').show();
	});

	$('#popup-ok').on('click', 'a.ok', function(event) {
		$(this).parents('#popup-ok').hide();
		$('select option[value=""]').prop('selected',true).trigger('chosen:updated');
		$('input').val('');
	});
	$('.popup').on('click', 'a.popup-close', function(event) {
		$(this).parents('.popup').hide();
		$('select option[value=""]').prop('selected',true).trigger('chosen:updated');
		$('input').val('');
	});

	
	//Create a marker popup window
/*	function createInfoWindow(i) {
		var opts = {
				width : 550,     // 信息窗口宽度
				height: 300,     // 信息窗口高度
				//title : "信息窗口" , // 信息窗口标题
				enableMessage:true//设置允许信息窗发送短息
			};
       
		var json = dealerdata[i];		
		 var deta = JSON.stringify(json);
		var content = "<p style='font-size:18px;'>"+(i+1)+"."+dealerdata[i]['full_name']+"</p>" +
		"<p style='font-size:16px;height:28px;line-height:28px;'>"+dealerdata[i]['address']+"</p>"+
		"<div style='border-top:1px solid #dee1e3;margin-top:15px;padding-top:24px;overflow: hidden;'>" +
		"<div style='float:left;border-right:1px solid #dee1e3;padding-right:20px;'><p style='height:28px;line-height:28px;font-size:16px;width:190px;'>" +
		"销售热线:<span style='color:#1597d9;'>"+dealerdata[i]['sales_hotline']+"</span></p><a href='#' style='color:#1597d9;margin-top:30px;display:block;background:url(angularweb/assets/images/icon-arrow1.png) center left no-repeat;padding-left:24px;font-size:18px;text-decoration: underline;'>" +
				"进入经销商首页</a><a href='#' style='color:#ffffff;background-color:#019ada;width:148px;height:42px;border-radius:3px;text-align:center;margin-top:30px;display:block;line-height:42px;font-size:18px;'  onclick='sendDealerId("+dealerdata[i].id+")'>预约试驾</a></div><div style='float:left;padding-left:20px;'>" +
				"<img src='angularweb/assets/images/dealer-popup-img.jpg' /><div style='margin-top:12px;text-align:center;height:64px; line-height:44px;color:#1597d9;font-size:18px;position: relative;'><input type='tel' id='tel"+i+"' maxlength='11' placeholder='短信获取经销商信息' style='float:left;border:1px solid #dee1e3;background-color:#ffffff;width:229px;height:44px;-webkit-appearance:none;line-height:44px;border-radius:3px;padding:0px 12px;'>" +
				"<a  style='color:#ffffff;background-color:#019ada;width:78px;height:42px;border-radius:3px;text-align:center;display:block;line-height:42px;font-size:18px;float:left;margin-left:10px;' onclick='sendInfo("+i+","+deta+")'>发送</a><p class='warning'></p></div></div></div>";
		var iw = new BMap.InfoWindow(content, opts);
		
		return iw;
	}*/
	//Create a Icon
	function createIcon(json) {		
		var icon = new BMap.Icon("http://dev.baidu.com/wiki/static/map/API/img/ico-marker.gif", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowAnchor:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})  
		return icon; 
	}
}]);