package com.vw.cn.helpers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.domain.Budget;
import com.vw.cn.domain.CarModel;
import com.vw.cn.domain.CarSeries;
import com.vw.cn.domain.PurchaseOrder;
import com.vw.cn.domain.TestDrive;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;
import com.vw.cn.response.ResponseStatus;
import com.vw.cn.response.ResponseStatusCode;
import com.vw.cn.service.ICarService;
import com.vw.cn.service.IGenericService;
import com.vw.cn.utils.CommonUtils;

/**
 * @author karthikeyan_v
 */
@Service
public class CarServiceHelper {

	@Autowired 
	private ICarService carService;

	@Autowired 
	private IGenericService genericService;

	CommonUtils commonutills = CommonUtils.getInstance();

	//Register New CarModel
	public ResponseMessage<CarModel> createcar(CarModel carModelObj){
		ResponseStatus status = null;
		CarModel entity=null;
		try{
			entity=new CarModel();	
			entity.setName(carModelObj.getName());
			entity.setCode(carModelObj.getCode());
			entity.setSeries_code(carModelObj.getSeries_code());
			entity.setLang(carModelObj.getLang());
			entity.setCurrency_code(carModelObj.getCurrency_code());
			entity.setPrice(carModelObj.getPrice());			
			entity.setDelete_flag('0');
			entity.setUpdate_time(new Date());
			entity.setUrl(carModelObj.getUrl());
			carService.insertCarModel(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<CarModel>(status, entity);		
	}

	//View Profile
	public ResponseMessage<CarModel> viewcar(long id){
		ResponseStatus status = null;
		CarModel entity =null;
		try{
			entity = carService.findCarModelById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<CarModel>(status, entity);
	}	

	//get all cars
	public ResponseMessages<CarModel> getallcars(String lang){
		ResponseStatus status = null;
		List<CarModel> entity =null;
		try{
			entity = carService.findAllCarModels(lang);	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<CarModel>(status, entity);
	}
	
	
	//delete Profile
	public ResponseMessage<CarModel> deletecar(long id){
		ResponseStatus status = null;
		CarModel entity =null;
		try{
			entity = carService.findCarModelById(id);
			if(entity!=null){
				entity.setUpdate_time(new Date());
				entity.setDelete_flag('1');
				carService.deleteCarModel(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<CarModel>(status, entity);
	}

	//## Test Drive

	//Register New TestDrive
	public ResponseMessage<TestDrive> createtestdrive(TestDrive testDriveReq){ 
		ResponseStatus responseStatus = null;
		TestDrive entity=null;
		try{
			entity=new TestDrive();	
			entity.setName(testDriveReq.getName());
			entity.setGender(testDriveReq.getGender());
			entity.setMobile_no(testDriveReq.getMobile_no());
			entity.setSeries_code(testDriveReq.getSeries_code());
			entity.setDealer_id(testDriveReq.getDealer_id());
			entity.setDealer_province_code(testDriveReq.getDealer_province_code());
			entity.setDealer_city_code(testDriveReq.getDealer_city_code());
			entity.setPlan_buy_time(testDriveReq.getPlan_buy_time());
			entity.setStatus(testDriveReq.getStatus());
			entity.setRegister_date(new Date());
			entity.setComment(testDriveReq.getComment());
			entity.setUpdated_by(testDriveReq.getUpdated_by());
			entity.setLast_updated_date(new Date());
			carService.insertTestDrive(entity);
			responseStatus=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			responseStatus=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<TestDrive>(responseStatus, entity);		
	}

	//View Profile
	public ResponseMessage<TestDrive> viewtestdrive(long id){
		ResponseStatus status = null;
		TestDrive entity =null;
		try{
			entity = carService.findTestDriveById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<TestDrive>(status, entity);
	}	

	//get all testdrives
	public ResponseMessages<TestDrive> getalltestdrives(){
		ResponseStatus status = null;
		List<TestDrive> entity =null;
		try{
			entity = carService.findAllTestDrives();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<TestDrive>(status, entity);
	}

	//delete Profile
	public ResponseMessage<TestDrive> deletetestdrive(long id){
		ResponseStatus status = null;
		TestDrive entity =null;
		try{
			entity = carService.findTestDriveById(id);
			if(entity!=null){
				carService.deleteTestDrive(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<TestDrive>(status, entity);
	}
	//## Purchase Order

	//Register New PurchaseOrder
	public ResponseMessage<PurchaseOrder> createpurchaseorder(PurchaseOrder purchaseOrderObj){
		ResponseStatus responseStatus = null;
		PurchaseOrder entity=null;
		try{
			entity=new PurchaseOrder();	
			entity.setName(purchaseOrderObj.getName());
			entity.setGender(purchaseOrderObj.getGender());
			entity.setMobile_no(purchaseOrderObj.getMobile_no());
			entity.setModel_code(purchaseOrderObj.getModel_code());
			entity.setDealer_id(purchaseOrderObj.getDealer_id());	
			entity.setDealer_province_code(purchaseOrderObj.getDealer_province_code());
			entity.setDealer_city_code(purchaseOrderObj.getDealer_city_code());
			entity.setOrder_date(new Date());
			entity.setPlan_buy_time(purchaseOrderObj.getPlan_buy_time());
			entity.setDelete_flag('0');
			entity.setStatus(purchaseOrderObj.getStatus());
			entity.setComment(purchaseOrderObj.getComment());
			entity.setUpdated_by(purchaseOrderObj.getUpdated_by());
			entity.setLast_updated_date(new Date());
			carService.insertPurchaseOrder(entity);
			responseStatus=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			responseStatus=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<PurchaseOrder>(responseStatus, entity);		
	}

	//View Profile
	public ResponseMessage<PurchaseOrder> viewpurchaseorder(long id){
		ResponseStatus status = null;
		PurchaseOrder entity =null;
		try{
			entity = carService.findPurchaseOrderById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<PurchaseOrder>(status, entity);
	}	

	//get all purchaseorders
	public ResponseMessages<PurchaseOrder> getallpurchaseorders(){
		ResponseStatus status = null;
		List<PurchaseOrder> entity =null;
		try{
			entity = carService.findAllPurchaseOrders();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<PurchaseOrder>(status, entity);
	}

	//delete Profile
	public ResponseMessage<PurchaseOrder> deletepurchaseorder(long id){
		ResponseStatus status = null;
		PurchaseOrder entity =null;
		try{
			entity = carService.findPurchaseOrderById(id);
			if(entity!=null){
				entity.setLast_updated_date(new Date());
				entity.setDelete_flag('1');
				carService.deletePurchaseOrder(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}			
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<PurchaseOrder>(status, entity);
	}

	public ResponseMessages<CarModel> budgetapi(Budget budgetdetail){
		ResponseStatus status = null;
		List<CarModel> budgetList = null;
		try{
			float minEMI= genericService.getGenericDetails("MINEMI").get(0).getValue();//Minimum EMI
			//float MaxEMI = genericService.getGenericDetails("MAXEMI").get(0).getValue();//Maximum EMI
			int minTenure = (int) genericService.getGenericDetails("MINTENURE").get(0).getValue();//Minimum Tenure
			//int maxTenure = (int) genericService.getGenericDetails("MAXTENURE").get(0).getValue();//Maximum Tenure
			float minDownPayment = genericService.getGenericDetails("MINDWNPAY").get(0).getValue();//Minimum Downpayment
			//float maxDownPayment = genericService.getGenericDetails("MAXDWNPAY").get(0).getValue();//Maximum Downpayment
			float Start=commonutills.RangeCalculation(minEMI, minDownPayment, minTenure);
			float end = commonutills.RangeCalculation(budgetdetail.getEmi(), budgetdetail.getDownpayment(), budgetdetail.getTenure());
			Budget budget= new Budget();
			budget.setStart(Start);
			budget.setEnd(end);
			System.out.println(Start);
			System.out.println(end);
			budgetList = carService.findBudgetCarMaodels(budget);
			if(budgetList!=null && budgetList.size()>0){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}

		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessages<CarModel>(status, budgetList);		
	}

	//CarSeries
	public ResponseMessage<CarSeries> createcarseries(CarSeries carSeriesObj){
		ResponseStatus status = null;
		CarSeries entity=null;
		try{
			entity= new CarSeries();
			entity.setCode(carSeriesObj.getCode());
			entity.setName(carSeriesObj.getName());
			entity.setLang(carSeriesObj.getLang());
			entity.setDelete_flag('0');
			entity.setUpdate_time(new Date());
			carService.insertCarSeries(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<CarSeries>(status, entity);
	}

	//get all cars
	public ResponseMessages<CarSeries> getallcarseries(String lang){
		ResponseStatus status = null;
		List<CarSeries> entity =null;
		try{
			entity = carService.findAllCarSeries(lang);	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<CarSeries>(status, entity);
	}
	//get all cars
		public ResponseMessages<CarModel> getCarModelsBySeries(CarModel carmodel){
			ResponseStatus status = null;
			List<CarModel> entity =null;
			try{
				entity = carService.findAllCarModelBySeries(carmodel);
				if(entity!=null){
					status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
				}
				else{
					status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
				}		
			}
			catch(Exception ex){
				ex.printStackTrace();	
				status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
			}
			return new ResponseMessages<CarModel>(status, entity);
		}
}