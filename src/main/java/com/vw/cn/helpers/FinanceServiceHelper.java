package com.vw.cn.helpers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.Application;
import com.vw.cn.customDomain.FinancialLoan;
import com.vw.cn.domain.FinancialInstitution;
import com.vw.cn.domain.FinancialLoanCalculation;
import com.vw.cn.domain.FinancialProducts;
import com.vw.cn.domain.Loan;
import com.vw.cn.domain.ModelFinancialInterest;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;
import com.vw.cn.response.ResponseStatus;
import com.vw.cn.response.ResponseStatusCode;
import com.vw.cn.service.IFinanceService;
import com.vw.cn.service.IGenericService;
import com.vw.cn.utils.CommonUtils;

import org.slf4j.*;

/**
 * @author karthikeyan_v
 */
@Service
public class FinanceServiceHelper {

//	private static final Logger log = LoggerFactory.getLogger(FinanceServiceHelper.class);
	//Logger log = createLoggerFor("log", "logger.log");
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired 
	private IFinanceService financeService;

	@Autowired 
	private IGenericService genericService;

	CommonUtils commonutills = CommonUtils.getInstance();

	// FinancialInstitution
	//Register new FinancialInstitution
	public ResponseMessage<FinancialInstitution> createFinancialInstitution(FinancialInstitution financialInstitutionObj){
		ResponseStatus status = null;
		FinancialInstitution entity=null;
		try{
			entity=new FinancialInstitution();
			entity.setCode(financialInstitutionObj.getCode());
			entity.setBank_name(financialInstitutionObj.getBank_name());
			entity.setBranch_name(financialInstitutionObj.getBranch_name());
			entity.setCity_code(financialInstitutionObj.getCity_code());
			entity.setProvince_code(financialInstitutionObj.getProvince_code());
			entity.setDistrict_code(financialInstitutionObj.getDistrict_code());
			entity.setLang(financialInstitutionObj.getLang());
			entity.setDelete_flag(false);
			entity.setLast_update_time(new Date());

			financeService.insertFinancialInstitution(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<FinancialInstitution>(status, entity);		
	}

	//update FinancialInstitution
	public ResponseMessage<FinancialInstitution> updateFinancialInstitution(FinancialInstitution financialInstitutionObj){
		ResponseStatus status = null;
		FinancialInstitution entity=null;
		try{
			entity = financeService.findFinancialInstitutionById(financialInstitutionObj.getId());
			if(entity!=null){
				entity.setCode(financialInstitutionObj.getCode());
				entity.setBank_name(financialInstitutionObj.getBank_name());
				entity.setBranch_name(financialInstitutionObj.getBranch_name());
				entity.setCity_code(financialInstitutionObj.getCity_code());
				entity.setProvince_code(financialInstitutionObj.getProvince_code());
				entity.setDistrict_code(financialInstitutionObj.getDistrict_code());
				entity.setLang(financialInstitutionObj.getLang());
				entity.setDelete_flag(financialInstitutionObj.isDelete_flag());
				entity.setLast_update_time(new Date());

				financeService.updateFinancialInstitution(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<FinancialInstitution>(status, entity);
	}

	//View FinancialInstitution
	public ResponseMessage<FinancialInstitution> viewFinancialInstitution(long id){
		ResponseStatus status = null;
		FinancialInstitution entity =null;
		//String s= "val"+id;
		try{
			//entity = financeService.findFinancialInstitutionById(Long.parseLong(s));
			entity = financeService.findFinancialInstitutionById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			log.error("Exception occured in viewFinancialInstitution", ex);
			//ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<FinancialInstitution>(status, entity);
	}	

	//get all FinancialInstitutions
	public ResponseMessages<FinancialInstitution> getallFinancialInstitutions(String lang){
		ResponseStatus status = null;
		List<FinancialInstitution> entity =null;
		try{
			entity = financeService.findAllFinancialInstitutions(lang);	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
		//	ex.printStackTrace();
			log.error("Exception occured in getallFinancialInstitutions", ex);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<FinancialInstitution>(status, entity);
	}

	// FinancialProducts
	//Register new FinancialProducts
	public ResponseMessage<FinancialProducts> createFinancialProducts(FinancialProducts financialProductsObj){
		ResponseStatus status = null;
		FinancialProducts entity=null;
		try{
			entity=new FinancialProducts();
			entity.setCode(financialProductsObj.getCode());
			entity.setFinancial_institution(financialProductsObj.getFinancial_institution());
			entity.setName(financialProductsObj.getName());
			entity.setDescription(financialProductsObj.getDescription());
			entity.setLang(financialProductsObj.getLang());
			entity.setDelete_flag(false);
			entity.setLast_updated_date(new Date());

			financeService.insertFinancialProducts(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			//ex.printStackTrace();
			log.error("Exception occured in createFinancialProducts", ex);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<FinancialProducts>(status, entity);		
	}

	//update FinancialProducts
	public ResponseMessage<FinancialProducts> updateFinancialProducts(FinancialProducts financialProductsObj){
		ResponseStatus status = null;
		FinancialProducts entity=null;
		try{
			entity = financeService.findFinancialProductsById(financialProductsObj.getId());
			if(entity!=null){
				entity.setCode(financialProductsObj.getCode());
				entity.setFinancial_institution(financialProductsObj.getFinancial_institution());
				entity.setName(financialProductsObj.getName());
				entity.setDescription(financialProductsObj.getDescription());
				entity.setLang(financialProductsObj.getLang());
				entity.setDelete_flag(financialProductsObj.isDelete_flag());
				entity.setLast_updated_date(new Date());

				financeService.updateFinancialProducts(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			//ex.printStackTrace();	
			log.error("Exception occured in updateFinancialProducts", ex);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<FinancialProducts>(status, entity);
	}

	//View FinancialProducts
	public ResponseMessage<FinancialProducts> viewFinancialProducts(long id){
		ResponseStatus status = null;
		FinancialProducts entity =null;
		try{
			entity = financeService.findFinancialProductsById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			//ex.printStackTrace();	
			log.error("Exception occured in viewFinancialProducts", ex);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<FinancialProducts>(status, entity);
	}	

	//get all FinancialProductss
	public ResponseMessages<FinancialProducts> getallFinancialProductss(String lang){
		ResponseStatus status = null;
		List<FinancialProducts> entity =null;
		try{
			entity = financeService.findAllFinancialProducts(lang);	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			//ex.printStackTrace();	
			log.error("Exception occured in viewFinancialProducts", ex);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<FinancialProducts>(status, entity);
	}

	// ModelFinancialInterest
	//Register new ModelFinancialInterest
	public ResponseMessage<ModelFinancialInterest> createModelFinancialInterest(ModelFinancialInterest modelFinancialInterestObj){
		ResponseStatus status = null;
		ModelFinancialInterest entity=null;
		try{
			entity=new ModelFinancialInterest();
			entity.setModel_code(modelFinancialInterestObj.getModel_code());
			entity.setFinancial_product_code(modelFinancialInterestObj.getFinancial_product_code());
			entity.setInterest_rate(modelFinancialInterestObj.getInterest_rate());
			entity.setCustomer_interest_rate(modelFinancialInterestObj.getCustomer_interest_rate());

			financeService.insertModelFinancialInterest(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			//ex.printStackTrace();	
			log.error("Exception occured in createModelFinancialInterest", ex);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<ModelFinancialInterest>(status, entity);		
	}

	//update ModelFinancialInterest
	public ResponseMessage<ModelFinancialInterest> updateModelFinancialInterest(ModelFinancialInterest modelFinancialInterestObj){
		ResponseStatus status = null;
		ModelFinancialInterest entity=null;
		try{
			entity = financeService.findModelFinancialInterestByCode(modelFinancialInterestObj.getModel_code());
			if(entity!=null){
				entity.setFinancial_product_code(modelFinancialInterestObj.getFinancial_product_code());
				entity.setInterest_rate(modelFinancialInterestObj.getInterest_rate());
				entity.setCustomer_interest_rate(modelFinancialInterestObj.getCustomer_interest_rate());

				financeService.updateModelFinancialInterest(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			//ex.printStackTrace();	
			log.error("Exception occured in findModelFinancialInterestByCode", ex);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<ModelFinancialInterest>(status, entity);
	}

	//View ModelFinancialInterest
	public ResponseMessage<ModelFinancialInterest> viewModelFinancialInterest(String modelCode){
		ResponseStatus status = null;
		ModelFinancialInterest entity =null;
		try{
			entity = financeService.findModelFinancialInterestByCode(modelCode);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			//ex.printStackTrace();	
			log.error("Exception occured in viewModelFinancialInterest", ex);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<ModelFinancialInterest>(status, entity);
	}	

	//get all ModelFinancialInterests
	public ResponseMessages<ModelFinancialInterest> getallModelFinancialInterests(){
		ResponseStatus status = null;
		List<ModelFinancialInterest> entity =null;
		try{
			entity = financeService.findAllModelFinancialInterests();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			//ex.printStackTrace();	
			log.error("Exception occured in getallModelFinancialInterests", ex);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<ModelFinancialInterest>(status, entity);
	}

	//## Loan
	/*public ResponseMessage<Loan> applyloan(Loan loan){
		ResponseStatus status = null;
		Loan loanobj =null;
		try{
			financeService.insertLoanRecord(loan);
			List<Loan> loanlist= financeService.getAllLoanRecords();
			if(loanlist.size()>0){
				int listsize = loanlist.size();
				loanobj = loanlist.get(listsize-1);
			}
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Loan>(status, loanobj);		
	}*/
	
	public ResponseMessage<Loan> applyloan(Loan loanObj){
		ResponseStatus status = null;
		Loan entity =null;
		try{
			entity=new Loan();
			entity.setCar_series(loanObj.getCar_series());
			entity.setCar_model(loanObj.getCar_model());
			entity.setFinancial_institutions(loanObj.getFinancial_institutions());
			entity.setFinancial_product(loanObj.getFinancial_product());
			entity.setTotal_payment(loanObj.getTotal_payment());
			entity.setDown_payment(loanObj.getDown_payment());
			entity.setLoan_tenure(loanObj.getLoan_tenure());
			entity.setInterest(loanObj.getInterest());
			entity.setDealer_province(loanObj.getDealer_province());
			entity.setDealer_city(loanObj.getDealer_city());
			entity.setDealer(loanObj.getDealer());
			entity.setName(loanObj.getName());
			entity.setTitle(loanObj.getTitle());
			entity.setPhone(loanObj.getPhone());
			entity.setExpected_purchase_time(loanObj.getExpected_purchase_time());
			entity.setLoan_status(loanObj.getLoan_status());
			entity.setDelete_flag(false);
			entity.setLast_updated_date(new Date());
			entity.setApplication_date(new Date());
			entity.setCreated_by(loanObj.getCreated_by());
			
			financeService.insertLoanRecord(entity);		
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			log.error("Exception occured in applyloan", e);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Loan>(status, entity);		
	}

	public ResponseMessages<Loan> getAllLoanRecords(){
		ResponseStatus status = null;
		List<Loan> loanRecords = null;
		try{
			loanRecords = financeService.getAllLoanRecords();
			if(loanRecords!=null && loanRecords.size()>0)	 {
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}	
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			log.error("Exception occured in getAllLoanRecords", e);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<Loan>(status, loanRecords);		
	}

	public ResponseMessage<Loan> getLoanRecordById(long id){
		ResponseStatus status = null;
		Loan loanRecords = null;
		try{
			loanRecords = financeService.getLoanRecordById(id);
			if(loanRecords!=null)	 {
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}	
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			log.error("Exception occured in getLoanRecordById", e);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Loan>(status, loanRecords);		
	}

	/*public ResponseMessage<LoanCalculator> calculateLoan(Loan loan){
		ResponseStatus status = null;
		LoanCalculator loaninfo = new LoanCalculator();
		try{
			float net_price = loan.getRecommended_retail_price();
			float min_downpayment = (float) (net_price*0.2);
			float opted_downpayment = loan.getDown_payment();
			float final_downpayment=min_downpayment;
			int opted_tenure = loan.getLoan_tenure();
			int minTenure = (int) genericService.getGenericDetails("MINTENURE").get(0).getValue();
			int final_tenure = minTenure;

			if(opted_downpayment>min_downpayment){
				final_downpayment = opted_downpayment;
			}
			if(opted_tenure > minTenure)
			{
				final_tenure = opted_tenure;
			}
			float final_retail_price = loan.getRecommended_retail_price();
			String model_code = loan.getCar_model();
			String fp_code = loan.getFinancial_product();
			ModelFinancialInterest modelInterest = null;
			Loan ln = new Loan();
			ln.setCar_model(model_code);
			ln.setFinancial_product(fp_code);
			modelInterest = financeService.findModelFinancialInterestByModelCodeAndProductCode(ln);
			if(modelInterest!=null)
			{
				float final_interest = modelInterest.getCustomer_interest_rate();
				System.out.println(final_downpayment);
				System.out.println(final_retail_price);
				float final_emi =	commonutills.calculateEmi(final_downpayment, final_tenure, final_retail_price, final_interest);
				loaninfo.setDown_payment(final_downpayment);
				loaninfo.setEmi(final_emi);
				loaninfo.setInterest(final_interest);
				loaninfo.setTotal_amount(final_retail_price);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}	
		}
		catch(Exception e)
		{
			e.printStackTrace();
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<LoanCalculator>(status, loaninfo);		
	}*/
	
	/*public ResponseMessage<FinancialLoanCalculationDTO> getFinancialLoanCalculation(Loan loan){
		ResponseStatus status = null;
		FinancialLoanCalculationDTO entity=null;
		
		try{
			FinancialLoanCalculation response = financeService.getFinancialLoanCalculation(loan);	
			if(response!=null){
				entity = new FinancialLoanCalculationDTO();
				entity.setTotal_amount(""+response.getRr_price());
				entity.setDown_payment_amount(""+response.getDown_payment_amount());
				entity.setInterest_amount(""+response.getInterest_amount());
				entity.setLoan_tenure(""+response.getLoan_tenure());
				entity.setEmi(""+response.getMonthly_installment());
				
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<FinancialLoanCalculationDTO>(status, entity);
	} */
	
	public ResponseMessage<FinancialLoanCalculation> getFinancialLoanCalculations(Loan loan){
		ResponseStatus status = null;
		FinancialLoanCalculation entity=null;
		
		try{
			entity = financeService.getFinancialLoanCalculations(loan);	
			if(entity!=null){				
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			//ex.printStackTrace();	
			log.error("Exception occured in getFinancialLoanCalculations", ex);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<FinancialLoanCalculation>(status, entity);
	} 
	
	public ResponseMessages<FinancialLoan> getFinancialLoanCalculation(Loan loan){
		ResponseStatus status = null;
		List<FinancialLoan> entity=null;
		
		try{
			entity = financeService.getFinancialLoanCalculation(loan);	
			if(entity!=null){				
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			//ex.printStackTrace();	
			log.error("Exception occured in getFinancialLoanCalculation", ex);
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<FinancialLoan>(status, entity);
	} 
	
	//get all FinancialProductss
		public ResponseMessages<FinancialProducts> getallFinancialProductsByInstitute(String lang,String financialInstitution){
			ResponseStatus status = null;
			List<FinancialProducts> entity =null;
			try{
				FinancialProducts financialproducts = new FinancialProducts();
				financialproducts.setFinancial_institution(financialInstitution);
				financialproducts.setLang(lang);
				entity = financeService.findAllFinancialProductsByInstitute(financialproducts);	
				if(entity!=null){
					status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
				}
				else{
					status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
				}		
			}
			catch(Exception ex){
				//ex.printStackTrace();	
				log.error("Exception occured in getallFinancialProductsByInstitute", ex);
				status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
			}
			return new ResponseMessages<FinancialProducts>(status, entity);
		}
}