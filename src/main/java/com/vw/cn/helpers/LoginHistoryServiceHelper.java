package com.vw.cn.helpers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.vw.cn.domain.LoginHistory;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;
import com.vw.cn.response.ResponseStatus;
import com.vw.cn.response.ResponseStatusCode;
import com.vw.cn.service.ILoginHistoryService;
/**
 * @author karthikeyan_v
 */
@Service
public class LoginHistoryServiceHelper {

	@Autowired 
	private ILoginHistoryService loginhistoryService;	

	//Register new loginhistory
	public ResponseMessage<LoginHistory> createloginhistory(String userid,String token,Date logintime){
		ResponseStatus status = null;
		LoginHistory entity=null;
		try{
			entity=new LoginHistory();
			entity.setUser_id(Long.parseLong(userid));
			entity.setToken(token);
			entity.setLogin_time(logintime);
			loginhistoryService.insertLoginHistory(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<LoginHistory>(status, entity);		
	}

	//update loginhistory
	public ResponseMessage<LoginHistory> updateloginhistory(String token,Date logouttime){
		ResponseStatus status = null;
		LoginHistory entity=null;
		try{
			entity = loginhistoryService.findLoginHistoryByToken(token);
			if(entity!=null){
				entity.setLogout_time(logouttime);
				loginhistoryService.updateLoginHistory(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<LoginHistory>(status, entity);
	}

	//View loginhistory
	public ResponseMessage<LoginHistory> viewloginhistory(long id){
		ResponseStatus status = null;
		LoginHistory entity =null;
		try{
			entity = loginhistoryService.findLoginHistoryById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<LoginHistory>(status, entity);
	}	

	//get all loginhistorys
	public ResponseMessages<LoginHistory> getallloginhistories(){
		ResponseStatus status = null;
		List<LoginHistory> entity =null;
		try{
			entity = loginhistoryService.findAllLoginHistorys();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<LoginHistory>(status, entity);
	}

	//delete loginhistory
	public ResponseMessage<LoginHistory> deleteloginhistory(long id){
		ResponseStatus status = null;
		LoginHistory entity =null;
		try{
			entity = loginhistoryService.findLoginHistoryById(id);
			if(entity!=null){
				loginhistoryService.deleteLoginHistory(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<LoginHistory>(status, entity);
	}
}