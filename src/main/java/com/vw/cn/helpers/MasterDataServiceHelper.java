package com.vw.cn.helpers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.customDomain.DistrictCityProvince;
import com.vw.cn.customDomain.ProvinceAndCity;
import com.vw.cn.domain.City;
import com.vw.cn.domain.Currency;
import com.vw.cn.domain.DealerType;
import com.vw.cn.domain.District;
import com.vw.cn.domain.Language;
import com.vw.cn.domain.Province;
import com.vw.cn.domain.RecruitmentNetworkType;
import com.vw.cn.domain.Role;
import com.vw.cn.domain.SourceSite;
import com.vw.cn.dto.DistrictDTO;
import com.vw.cn.response.ResponseMessage;
import com.vw.cn.response.ResponseMessages;
import com.vw.cn.response.ResponseStatus;
import com.vw.cn.response.ResponseStatusCode;
import com.vw.cn.service.IMasterDataService;
/**
 * @author karthikeyan_v
 */
@Service
public class MasterDataServiceHelper {	

	@Autowired 
	private IMasterDataService masterDataService;	

	// Role
	//Register new role
	public ResponseMessage<Role> createrole(String name,String description){
		ResponseStatus status = null;
		Role entity=null;
		try{
			entity=new Role();
			entity.setName(name);
			entity.setDescription(description);
			entity.setDelete_flag(false);
			entity.setUpdate_time(new Date());
			masterDataService.insertRole(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<Role>(status, entity);		
	}

	//update role
	public ResponseMessage<Role> updaterole(long id,String name,String description){
		ResponseStatus status = null;
		Role entity=null;
		try{
			entity = masterDataService.findRoleById(id);
			if(entity!=null){
				if(!name.isEmpty()){
					entity.setName(name);
				}
				if(!description.isEmpty()){
					entity.setDescription(description);
				}
				entity.setUpdate_time(new Date());
				masterDataService.updateRole(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Role>(status, entity);
	}

	//View role
	public ResponseMessage<Role> viewrole(long id){
		ResponseStatus status = null;
		Role entity =null;
		try{
			entity = masterDataService.findRoleById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Role>(status, entity);
	}	

	//get all roles
	public ResponseMessages<Role> getallroles(){
		ResponseStatus status = null;
		List<Role> entity =null;
		try{
			entity = masterDataService.findAllRoles();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<Role>(status, entity);
	}

	//delete role
	public ResponseMessage<Role> deleterole(long id){
		ResponseStatus status = null;
		Role entity =null;
		try{
			entity = masterDataService.findRoleById(id);
			if(entity!=null){
				masterDataService.deleteRole(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Role>(status, entity);
	}	

	//Language 

	//Register new language
	public ResponseMessage<Language> createlanguage(String name,String code){
		ResponseStatus status = null;
		Language entity=null;
		try{
			entity=new Language();
			entity.setName(name);
			entity.setCode(code);
			entity.setDelete_flag(false);
			entity.setUpdate_time(new Date());
			masterDataService.insertLanguage(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<Language>(status, entity);		
	}

	//update language
	public ResponseMessage<Language> updatelanguage(long id,String name,String code){
		ResponseStatus status = null;
		Language entity=null;
		try{
			entity = masterDataService.findLanguageById(id);
			if(entity!=null){
				if(!name.isEmpty()){
					entity.setName(name);
				}
				if(!code.isEmpty()){
					entity.setCode(code);
				}
				entity.setDelete_flag(false);
				masterDataService.updateLanguage(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Language>(status, entity);
	}

	//View language
	public ResponseMessage<Language> viewlanguage(long id){
		ResponseStatus status = null;
		Language entity =null;
		try{
			entity = masterDataService.findLanguageById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Language>(status, entity);
	}	

	//get all languages
	public ResponseMessages<Language> getalllanguages(){
		ResponseStatus status = null;
		List<Language> entity =null;
		try{
			entity = masterDataService.findAllLanguages();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<Language>(status, entity);
	}

	//delete language
	public ResponseMessage<Language> deletelanguage(long id){
		ResponseStatus status = null;
		Language entity =null;
		try{
			entity = masterDataService.findLanguageById(id);
			if(entity!=null){
				masterDataService.deleteLanguage(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Language>(status, entity);
	}	

	//Province
	public ResponseMessage<Province> createprovince(String code,String name,String langcode){
		ResponseStatus status = null;
		Province entity=null;
		try{			
			entity=new Province();	
			entity.setCode(code);
			entity.setName(name);
			entity.setLang_code(langcode);
			entity.setDelete_flag(false);
			entity.setUpdate_time(new Date());
			masterDataService.insertProvince(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<Province>(status, entity);		
	}

	public ResponseMessage<Province> updateprovince(long id,String code,String name,String langcode){
		ResponseStatus status = null;
		Province entity =null;
		try{
			entity = masterDataService.findProvinceById(id);
			if(entity!=null){
				if(!code.isEmpty()){
					entity.setCode(code);	
				}
				if(!name.isEmpty()){
					entity.setName(name);
				}
				if(!langcode.isEmpty()){	
					entity.setLang_code(langcode);	
				}
				entity.setUpdate_time(new Date());
				masterDataService.updateProvince(entity);
			}
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Province>(status, entity);	

	}

	public ResponseMessage<Province> viewprovince(long id){
		ResponseStatus status = null;
		Province entity =null;
		try{
			entity = masterDataService.findProvinceById(id);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Province>(status, entity);
	}	

	public ResponseMessages<Province> getallprovinces(String lang){
		ResponseStatus status = null;
		List<Province> entity =null;
		try{
			entity = masterDataService.findAllProvinces(lang);	
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<Province>(status, entity);
	}

	public ResponseMessage<Province> deleteprovince(long id){
		ResponseStatus status = null;
		Province entity =null;
		try{
			entity = masterDataService.findProvinceById(id);
			if(entity!=null){
				masterDataService.deleteProvince(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Province>(status, entity);
	}	

	//City
	//Register new city
	public ResponseMessage<City> addcity(String code,String name,String provincecode,String langcode){
		ResponseStatus status = null;
		City entity=null;
		try{
			entity=new City();
			entity.setCode(code);
			entity.setName(name);
			entity.setProvince_code(provincecode);
			entity.setLang_code(langcode);
			entity.setDelete_flag(false);
			entity.setUpdate_time(new Date());
			masterDataService.insertCity(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<City>(status, entity);		
	}

	//update city
	public ResponseMessage<City> updatecity(long id,String code,String name,String provincecode,String langcode){
		ResponseStatus status = null;
		City entity=null;
		try{
			entity = masterDataService.findCityById(id);
			if(entity!=null){
				if(!code.isEmpty()){
					entity.setCode(code);
				}
				if(!name.isEmpty()){
					entity.setName(name);
				}
				if(!provincecode.isEmpty()){
					entity.setProvince_code(provincecode);
				}
				if(!langcode.isEmpty()){
					entity.setLang_code(langcode);
				}
				entity.setUpdate_time(new Date());		
				masterDataService.updateCity(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<City>(status, entity);
	}

	//View city
	public ResponseMessage<City> viewcity(long id){
		ResponseStatus status = null;
		City entity =null;
		try{
			entity = masterDataService.findCityById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<City>(status, entity);
	}	
	
	public ResponseMessage<City> getCityByName(String name){
		ResponseStatus status = null;
		City entity =null;
		try{
			entity = masterDataService.findCityByName(name);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<City>(status, entity);
	}	

	//get all citys
	public ResponseMessages<City> getallcities(String lang){
		ResponseStatus status = null;
		List<City> entity =null;
		try{
			entity = masterDataService.findAllCities(lang);	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<City>(status, entity);
	}

	//delete city
	public ResponseMessage<City> deletecity(long id){
		ResponseStatus status = null;
		City entity =null;
		try{
			entity = masterDataService.findCityById(id);
			if(entity!=null){
				masterDataService.deleteCity(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<City>(status, entity);
	}	

	//District
	//Register new district
	public ResponseMessage<District> createdistrict(String code,String name,String citycode,String langcode){
		ResponseStatus status = null;
		District entity=null;
		try{
			entity=new District();
			entity.setCode(code);
			entity.setName(name);
			entity.setCity_code(citycode);
			entity.setLang_code(langcode);
			entity.setDelete_flag(false);
			entity.setUpdate_time(new Date());
			masterDataService.insertDistrict(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<District>(status, entity);		
	}

	//update district
	public ResponseMessage<District> updatedistrict(long id,String code,String name,String citycode,String langcode){
		ResponseStatus status = null;
		District entity=null;
		try{
			entity = masterDataService.findDistrictById(id);
			if(entity!=null){
				if(!code.isEmpty()){
					entity.setCode(code);
				}
				if(!name.isEmpty()){
					entity.setName(name);
				}
				if(!citycode.isEmpty()){
					entity.setCity_code(citycode);			
				}
				if(!langcode.isEmpty()){
					entity.setLang_code(langcode);
				}		
				entity.setUpdate_time(new Date());
				masterDataService.updateDistrict(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<District>(status, entity);
	}

	//View district
	public ResponseMessage<District> viewdistrict(long id){
		ResponseStatus status = null;
		District entity =null;
		try{
			entity = masterDataService.findDistrictById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<District>(status, entity);
	}	

	//get all districts
	public ResponseMessages<District> getalldistricts(String lang){
		ResponseStatus status = null;
		List<District> entity =null;
		try{
			entity = masterDataService.findAllDistricts(lang);	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<District>(status, entity);
	}

	//delete district
	public ResponseMessage<District> deletedistrict(long id){
		ResponseStatus status = null;
		District entity =null;
		try{
			entity = masterDataService.findDistrictById(id);
			if(entity!=null){
				masterDataService.deleteDistrict(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<District>(status, entity);
	}	

	//RecruitmentNetworkType 

	public ResponseMessage<RecruitmentNetworkType> createrecruitment(String code,String name,String langcode){
		ResponseStatus status = null;
		RecruitmentNetworkType entity=null;
		try{			
			entity=new RecruitmentNetworkType();	
			entity.setCode(code);
			entity.setName(name);
			entity.setLang_code(langcode);
			entity.setDelete_flag(false);
			entity.setUpdate_time(new Date());
			masterDataService.insertRecruitmentNetworkType(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<RecruitmentNetworkType>(status, entity);		
	}

	public ResponseMessage<RecruitmentNetworkType> updaterecruitment(long id,String code,String name,String langcode){
		ResponseStatus status = null;
		RecruitmentNetworkType entity =null;
		try{
			entity = masterDataService.findRecruitmentNetworkTypeById(id);
			if(entity!=null){
				if(!code.isEmpty()){
					entity.setCode(code);	
				}
				if(!name.isEmpty()){
					entity.setName(name);
				}
				if(!langcode.isEmpty()){	
					entity.setLang_code(langcode);	
				}
				entity.setUpdate_time(new Date());
				masterDataService.updateRecruitmentNetworkType(entity);
			}
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<RecruitmentNetworkType>(status, entity);	

	}

	public ResponseMessage<RecruitmentNetworkType> viewrecruitment(long id){
		ResponseStatus status = null;
		RecruitmentNetworkType entity =null;
		try{
			entity = masterDataService.findRecruitmentNetworkTypeById(id);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<RecruitmentNetworkType>(status, entity);
	}	

	public ResponseMessages<RecruitmentNetworkType> getallrecruitments(String lang){
		ResponseStatus status = null;
		List<RecruitmentNetworkType> entity =null;
		try{
			entity = masterDataService.findAllRecruitmentNetworkTypes(lang);	
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<RecruitmentNetworkType>(status, entity);
	}

	public ResponseMessage<RecruitmentNetworkType> deleterecruitment(long id){
		ResponseStatus status = null;
		RecruitmentNetworkType entity =null;
		try{
			entity = masterDataService.findRecruitmentNetworkTypeById(id);
			if(entity!=null){
				masterDataService.deleteRecruitmentNetworkType(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<RecruitmentNetworkType>(status, entity);
	}


	// Currency
	//Register new Currency
	public ResponseMessage<Currency> createCurrency(Currency currencyObj){
		ResponseStatus status = null;
		Currency entity=null;
		try{
			entity=new Currency();
			entity.setCode(currencyObj.getCode());
			entity.setName(currencyObj.getName());
			entity.setSymbol(currencyObj.getSymbol());
			entity.setDelete_flag(false);
			entity.setUpdate_time(new Date());
			masterDataService.insertCurrency(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<Currency>(status, entity);		
	}

	//update Currency
	public ResponseMessage<Currency> updateCurrency(Currency currencyObj){
		ResponseStatus status = null;
		Currency entity=null;
		try{
			entity = masterDataService.findCurrencyById(currencyObj.getId());
			if(entity!=null){entity.setCode(currencyObj.getCode());
			entity.setName(currencyObj.getName());
			entity.setSymbol(currencyObj.getSymbol());
			entity.setDelete_flag(currencyObj.isDelete_flag());
			entity.setUpdate_time(new Date());
			masterDataService.updateCurrency(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Currency>(status, entity);
	}

	//View Currency
	public ResponseMessage<Currency> viewCurrency(long id){
		ResponseStatus status = null;
		Currency entity =null;
		try{
			entity = masterDataService.findCurrencyById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Currency>(status, entity);
	}	

	//get all Currencys
	public ResponseMessages<Currency> getallCurrencys(){
		ResponseStatus status = null;
		List<Currency> entity =null;
		try{
			entity = masterDataService.findAllCurrencys();	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<Currency>(status, entity);
	}

	//delete Currency
	public ResponseMessage<Currency> deleteCurrency(long id){
		ResponseStatus status = null;
		Currency entity =null;
		try{
			entity = masterDataService.findCurrencyById(id);
			if(entity!=null){
				masterDataService.deleteCurrency(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<Currency>(status, entity);
	}	

	public ResponseMessages<ProvinceAndCity> getallprovinceandcities(String lang){
		ResponseStatus status = null;
		List<ProvinceAndCity> entity =null;
		try{
			entity = masterDataService.getallprovinceAndCities(lang);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<ProvinceAndCity>(status, entity);
	}

	//get all DistrictCityProvince
	public ResponseMessages<DistrictCityProvince> getAllDistrictCityProvince(String lang){
		ResponseStatus status = null;
		List<DistrictCityProvince> entity =null;
		try{
			entity = masterDataService.getAllDistrictCityProvince(lang);	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<DistrictCityProvince>(status, entity);
	}

	// SourceSite
	//Register new SourceSite
	public ResponseMessage<SourceSite> createSourceSite(SourceSite sourceSiteObj){
		ResponseStatus status = null;
		SourceSite entity=null;
		try{
			entity=new SourceSite();
			entity.setCode(sourceSiteObj.getCode());
			entity.setName(sourceSiteObj.getName());
			entity.setLang_code(sourceSiteObj.getLang_code());
			masterDataService.insertSourceSite(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<SourceSite>(status, entity);		
	}

	//update SourceSite
	public ResponseMessage<SourceSite> updateSourceSite(SourceSite sourceSiteObj){
		ResponseStatus status = null;
		SourceSite entity=null;
		try{
			entity = masterDataService.findSourceSiteById(sourceSiteObj.getId());
			if(entity!=null){entity.setCode(sourceSiteObj.getCode());
			entity.setCode(sourceSiteObj.getCode());
			entity.setName(sourceSiteObj.getName());
			entity.setLang_code(sourceSiteObj.getLang_code());
			masterDataService.updateSourceSite(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<SourceSite>(status, entity);
	}

	//View SourceSite
	public ResponseMessage<SourceSite> viewSourceSite(long id){
		ResponseStatus status = null;
		SourceSite entity =null;
		try{
			entity = masterDataService.findSourceSiteById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<SourceSite>(status, entity);
	}	

	//get all SourceSites
	public ResponseMessages<SourceSite> getallSourceSites(String lang){
		ResponseStatus status = null;
		List<SourceSite> entity =null;
		try{
			entity = masterDataService.findAllSourceSites(lang);	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<SourceSite>(status, entity);
	}

	//delete SourceSite
	public ResponseMessage<SourceSite> deleteSourceSite(long id){
		ResponseStatus status = null;
		SourceSite entity =null;
		try{
			entity = masterDataService.findSourceSiteById(id);
			if(entity!=null){
				masterDataService.deleteSourceSite(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<SourceSite>(status, entity);
	}	

	//## Dealer Type
	//Register new DealerType
	public ResponseMessage<DealerType> createDealerType(DealerType dealerTypeObj){
		ResponseStatus status = null;
		DealerType entity=null;
		try{
			entity=new DealerType();
			entity.setCode(dealerTypeObj.getCode());
			entity.setName(dealerTypeObj.getName());
			entity.setLang_code(dealerTypeObj.getLang_code());
			masterDataService.insertDealerType(entity);
			status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}	
		return new ResponseMessage<DealerType>(status, entity);		
	}

	//update DealerType
	public ResponseMessage<DealerType> updateDealerType(DealerType dealerTypeObj){
		ResponseStatus status = null;
		DealerType entity=null;
		try{
			entity = masterDataService.findDealerTypeById(dealerTypeObj.getId());
			if(entity!=null){				
				entity.setCode(dealerTypeObj.getCode());
				entity.setName(dealerTypeObj.getName());
				entity.setLang_code(dealerTypeObj.getLang_code());
				masterDataService.updateDealerType(entity);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerType>(status, entity);
	}

	//View DealerType
	public ResponseMessage<DealerType> viewDealerType(long id){
		ResponseStatus status = null;
		DealerType entity =null;
		try{
			entity = masterDataService.findDealerTypeById(id);
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerType>(status, entity);
	}	

	//get all DealerTypes
	public ResponseMessages<DealerType> getallDealerTypes(String lang_code){
		ResponseStatus status = null;
		List<DealerType> entity =null;
		try{
			entity = masterDataService.findAllDealerTypes(lang_code);	
			if(entity!=null){
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<DealerType>(status, entity);
	}	

	//delete DealerType
	public ResponseMessage<DealerType> deleteDealerType(long id){
		ResponseStatus status = null;
		DealerType entity =null;
		try{
			entity = masterDataService.findDealerTypeById(id);
			if(entity!=null){
				masterDataService.deleteDealerType(id);
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessage<DealerType>(status, entity);
	}	
	
	public ResponseMessages<DistrictDTO> getDistrictByCity(String cityCode,String lang){
		ResponseStatus status = null;
		List<DistrictDTO> entity = null;
		try{
			
			List<District> districtList = masterDataService.getDistrictByCity(cityCode, lang);
			if(districtList!=null){
				entity=new ArrayList<DistrictDTO>();
				for(District district:districtList){
					DistrictDTO distDTO=new DistrictDTO();					
					
					distDTO.setCode(district.getCode());
					distDTO.setCity_code(district.getCity_code());
					distDTO.setLang_code(district.getLang_code());
					distDTO.setName(district.getName());
					
					entity.add(distDTO);
				}
				
				status=new ResponseStatus(ResponseStatusCode.STATUS_OK, "Success");
			}
			else{
				status=new ResponseStatus(ResponseStatusCode.STATUS_NO_CONTENT, "No record found");
			}		
		}
		catch(Exception ex){
			ex.printStackTrace();	
			status=new ResponseStatus(ResponseStatusCode.STATUS_NORECORD, "Failed");
		}
		return new ResponseMessages<DistrictDTO>(status, entity);
	}
	
}