package com.vw.cn.service;

import java.util.List;

import com.vw.cn.customDomain.FinancialLoan;
import com.vw.cn.domain.FinancialInstitution;
import com.vw.cn.domain.FinancialLoanCalculation;
import com.vw.cn.domain.FinancialProducts;
import com.vw.cn.domain.Loan;
import com.vw.cn.domain.ModelFinancialInterest;
/**
 * @author Harihara Subramanian
 */
public interface IFinanceService {

	//## FinancialInstitution
	void insertFinancialInstitution(FinancialInstitution financialInstitution);

	void updateFinancialInstitution(FinancialInstitution financialInstitution); 

	FinancialInstitution findFinancialInstitutionById(long id);			

	List<FinancialInstitution> findAllFinancialInstitutions(String lang);	

	//## FinancialProducts
	void insertFinancialProducts(FinancialProducts financialProducts);

	void updateFinancialProducts(FinancialProducts financialProducts); 

	FinancialProducts findFinancialProductsById(long id);			

	List<FinancialProducts> findAllFinancialProducts(String lang);	

	//##ModelFinancialInterest
	void insertModelFinancialInterest(ModelFinancialInterest modelFinancialInterest);

	void updateModelFinancialInterest(ModelFinancialInterest modelFinancialInterest); 

	ModelFinancialInterest findModelFinancialInterestByCode(String model_code);		
	
	ModelFinancialInterest findModelFinancialInterestByModelCodeAndProductCode(Loan loan);	

	List<ModelFinancialInterest> findAllModelFinancialInterests();	

	//## Loan
	void insertLoanRecord(Loan loan);

	List<Loan> getAllLoanRecords();

	Loan getLoanRecordById(long id);
	
	FinancialLoanCalculation getFinancialLoanCalculations(Loan loan);

	List<FinancialLoan> getFinancialLoanCalculation(Loan loan);	
	
	List<FinancialProducts> findAllFinancialProductsByInstitute(FinancialProducts financialproducts);
}
