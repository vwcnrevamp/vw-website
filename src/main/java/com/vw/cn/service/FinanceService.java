package com.vw.cn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.customDomain.FinancialLoan;
import com.vw.cn.dao.FinanceDao;
import com.vw.cn.domain.FinancialInstitution;
import com.vw.cn.domain.FinancialLoanCalculation;
import com.vw.cn.domain.FinancialProducts;
import com.vw.cn.domain.Loan;
import com.vw.cn.domain.ModelFinancialInterest;

@Service
public class FinanceService implements IFinanceService {

	@Autowired
	FinanceDao financeDao;

	//## FinancailInstitution
	@Override
	public void insertFinancialInstitution(FinancialInstitution financialInstitution) {
		financeDao.insertFinancialInstitution(financialInstitution);
	}

	@Override
	public void updateFinancialInstitution(FinancialInstitution financialInstitution) {
		financeDao.updateFinancialInstitution(financialInstitution);
	}

	@Override
	public FinancialInstitution findFinancialInstitutionById(long id) {
		return financeDao.findFinancialInstitutionById(id);
	}

	@Override
	public List<FinancialInstitution> findAllFinancialInstitutions(String lang) {
		return financeDao.findAllFinancialInstitutions(lang);
	}

	//## FinancailProducts 
	@Override
	public void insertFinancialProducts(FinancialProducts financialProducts) {
		financeDao.insertFinancialProducts(financialProducts);
	}

	@Override
	public void updateFinancialProducts(FinancialProducts financialProducts) {
		financeDao.updateFinancialProducts(financialProducts);
	}

	@Override
	public FinancialProducts findFinancialProductsById(long id) {
		return financeDao.findFinancialProductsById(id);
	}

	@Override
	public List<FinancialProducts> findAllFinancialProducts(String lang) {
		return financeDao.findAllFinancialProducts(lang);
	}

	//## ModelFinancialInterest
	@Override
	public void insertModelFinancialInterest(
			ModelFinancialInterest modelFinancialInterest) {
		financeDao.insertModelFinancialInterest(modelFinancialInterest);
	}

	@Override
	public void updateModelFinancialInterest(
			ModelFinancialInterest modelFinancialInterest) {
		financeDao.updateModelFinancialInterest(modelFinancialInterest);
	}

	@Override
	public ModelFinancialInterest findModelFinancialInterestByCode(
			String model_code) {
		return financeDao.findModelFinancialInterestByCode(model_code);
	}

	@Override
	public List<ModelFinancialInterest> findAllModelFinancialInterests() {
		return financeDao.findAllModelFinancialInterests();
	}

	//## Loan
	@Override
	public void insertLoanRecord(Loan loan) {
		financeDao.insertLoanRecord(loan);
	}
	@Override
	public List<Loan> getAllLoanRecords() {

		return financeDao.getAllLoanRecords();
	}
	@Override
	public Loan getLoanRecordById(long id) {

		return financeDao.getLoanRecordById(id);
	}

	@Override
	public ModelFinancialInterest findModelFinancialInterestByModelCodeAndProductCode(
			Loan loan) {
	
		return financeDao.findModelFinancialInterestByModelCodeAndFinancialProduct(loan);
	}
	
	// for financial loan calculation 	
	public FinancialLoanCalculation getFinancialLoanCalculations(Loan loan){
		return financeDao.getFinancialLoanCalculations(loan);
	}
	
	public List<FinancialLoan> getFinancialLoanCalculation(Loan loan){
		return financeDao.getFinancialLoanCalculation(loan);
	}

	@Override
	public List<FinancialProducts> findAllFinancialProductsByInstitute(
			FinancialProducts financialproducts) {
		return financeDao.findAllFinancialProductsByInstitution(financialproducts);
	}
}
