package com.vw.cn.service;

import java.util.List;

import com.vw.cn.domain.LoginHistory;
/**
 * @author karthikeyan_v
 */
public interface ILoginHistoryService{

	void insertLoginHistory(LoginHistory loginHistory);

	void updateLoginHistory(LoginHistory loginHistory);

	LoginHistory findLoginHistoryById(long id);

	LoginHistory findLoginHistoryByUserID(long id);
	
	LoginHistory findLoginHistoryByToken(String token);

	List<LoginHistory> findAllLoginHistorys();	

	void deleteLoginHistory(long id);
}
