package com.vw.cn.service;

import java.util.List;

import com.vw.cn.customDomain.DealerCityProvince;
import com.vw.cn.customDomain.DealerRecordList;
import com.vw.cn.domain.Dealer;
import com.vw.cn.domain.DealerContact;
import com.vw.cn.domain.DealerRecruitingCity;
import com.vw.cn.domain.DealerRecruitment;
import com.vw.cn.domain.Promotion;
import com.vw.cn.domain.SearchDealer;
import com.vw.cn.domain.ServiceRating;
/**
 * @author karthikeyan_v
 */
public interface IDealerService{

	//## Dealer
	void insertDealer(Dealer dealer);

	void updateDealer(Dealer dealer);

	Dealer findDealerById(long id);

	List<Dealer> findDealerByCode(String prv_code);

	List<Dealer> findAllDealers();	

	void deleteDealer(Dealer dealer);

	//## Dealer Contact
	void insertDealerContact(DealerContact dealerContact);

	void updateDealerContact(DealerContact dealerContact);

	DealerContact findDealerContactById(long id);

	List<DealerContact> findAllDealerContacts();	

	void deleteDealerContact(long id);

	//## Promotion
	void insertPromotion(Promotion promotion);

	void updatePromotion(Promotion promotion);

	Promotion findPromotionById(long id);

	List<Promotion> findAllPromotions();	

	void deletePromotion(long id);		

	//## Service Rating
	void insertServiceRating(ServiceRating serviceRating);

	void updateServiceRating(ServiceRating serviceRating);

	ServiceRating findServiceRatingById(long id);

	List<ServiceRating> findAllServiceRatings();	

	void deleteServiceRating(long id);	

	//##Search Dealer
	List<SearchDealer> searchDealer(SearchDealer dealer);	

	//## Dealer Recruiting City
	void insertDealerRecruitingCity(DealerRecruitingCity recruitingCity);

	void updateDealerRecruitingCity(DealerRecruitingCity recruitingCity);

	DealerRecruitingCity findDealerRecruitingCityById(long id);

	List<DealerRecruitingCity> findAllDealerRecruitingCities();	

	void deleteDealerRecruitingCity(long id);	
	
	List<String> findDealerRecruitingProvinces();	

	List<String> findDealerRecruitingCitiesByProvinceName(String province);		

	List<String> findDealerRecruitingDistrictsByCityName(String city);	
	
	int findTotalCountForDealerRecruitingCitiesByProvince(String province,String recruitmentType);
	
	int findTotalCountForDealerRecruitingCitiesByCity(String city,String recruitmentType);
	
	int findTotalCountForDealerRecruitingCitiesByDistrict(String city, String district,String recruitmentType);
	
	int findTotalCountForDealerRecruitingCities(String recruitmentType);
	
	List<DealerRecruitingCity> findDealerRecruitingCitiesByProvince(String province,String recruitmentType,int start,int end);
	
	List<DealerRecruitingCity> findDealerRecruitingCitiesByCity(String city,String recruitmentType,int start,int end);
	
	List<DealerRecruitingCity> findDealerRecruitingCitiesByDistrict(String city, String district,String recruitmentType, int start,int end);
	
	List<DealerRecruitingCity> findDealerRecruitingCities(String recruitmentType,int start,int end);

	//## Dealer Recruitment
	void insertDealerRecruitment(DealerRecruitment dealerRecruitment);

	void updateDealerRecruitment(DealerRecruitment dealerRecruitment);

	DealerRecruitment findDealerRecruitmentById(long id);

	int findTotalRecordDealerRecruitmentByUserId(long userId);
	
	int findTotalRecordDealerRecruitmentsByStatusAndUserId(long userId,String status);
	
	int getApplicationSequence(long declaredCityId);
	
	List<DealerRecruitment> findDealerRecruitmentByUserId(long userId,int start,int end);
	
	List<DealerRecruitment> findDealerRecruitmentsByStatusAndUserId(long userId,String status,int start,int end);	

	void deleteDealerRecruitment(long id);	

	//Get Dealer Record list
	List<DealerRecordList> getDealerRecordList();	

	List<Dealer> filterDealerByProvince(String prv_code);

	List<Dealer> filterDealerByCity(String city_code);

	List<Dealer> filterDealerByType(String type);

	List<Dealer> filterDealer(Dealer dealer);
	
	List<DealerCityProvince> getDealerCityProvinceName(String lang_code);
}
