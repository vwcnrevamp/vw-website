package com.vw.cn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.dao.UserDao;
import com.vw.cn.domain.User;
import com.vw.cn.domain.UserDetail;
import com.vw.cn.domain.UserRole;
/**
 * @author karthikeyan_v
 */
@Service
public class UserService implements IUserService{

	@Autowired
	UserDao userDAO; 

	@Override
	public void insertUser(User user) {
		userDAO.insertUser(user);		
	}

	@Override
	public void updateUser(User user) {
		userDAO.updateUser(user);
	}

	@Override
	public User findUserById(long id) {	
		return userDAO.findUserById(id);
	}

	@Override
	public User findUserByAccount(String account) {
		return userDAO.findUserByAccount(account);
	}

	@Override
	public List<User> findAllUsers() {
		return userDAO.findAllUsers();
	}	

	@Override
	public void deleteUser(User user) {
		userDAO.deleteUser(user);
	}
	
	@Override
	public void insertUserDetail(UserDetail userDetail) {
		userDAO.insertUserDetail(userDetail);		
	}

	@Override
	public void updateUserDetail(UserDetail userDetail) {
		userDAO.updateUserDetail(userDetail);		
	}

	@Override
	public UserDetail findUserDetailById(long id) {
		return userDAO.findUserDetailById(id);
	}

	@Override
	public List<UserDetail> findAllUserDetails() {
		return userDAO.findAllUserDetails();
	}

	@Override
	public void deleteUserDetail(UserDetail userDetail) {
		userDAO.deleteUserDetail(userDetail);	
	} 
	
	@Override
	public void insertUserRole(UserRole userRole) {
		userDAO.insertUserRole(userRole);
	}

	@Override
	public void updateUserRole(UserRole userRole) {
		userDAO.updateUserRole(userRole);
	}

	@Override
	public UserRole findUserRoleById(long id) {
		return userDAO.findUserRoleById(id);
	}

	@Override
	public UserRole findUserRoleByUserId(long user_id) {
		return userDAO.findUserRoleByUserId(user_id);
	}

	@Override
	public UserRole findUserRoleByRoleId(long role_id) {
		return userDAO.findUserRoleByRoleId(role_id);
	}

	@Override
	public List<UserRole> findAllUserRoles() {
		return userDAO.findAllUserRoles();
	}

	@Override
	public void deleteUserRole(UserRole userRole) {
		userDAO.deleteUserRole(userRole);
	}
}
