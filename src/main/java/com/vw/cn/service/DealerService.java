package com.vw.cn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.customDomain.DealerCityProvince;
import com.vw.cn.customDomain.DealerRecordList;
import com.vw.cn.dao.DealerDao;
import com.vw.cn.domain.Dealer;
import com.vw.cn.domain.DealerContact;
import com.vw.cn.domain.DealerRecruitingCity;
import com.vw.cn.domain.DealerRecruitment;
import com.vw.cn.domain.Promotion;
import com.vw.cn.domain.SearchDealer;
import com.vw.cn.domain.ServiceRating;
/**
 * @author karthikeyan_v
 */
@Service
public class DealerService implements IDealerService{

	@Autowired
	DealerDao dealerDao;

	//## Dealer
	@Override
	public void insertDealer(Dealer dealerRegistration) {
		dealerDao.insertDealer(dealerRegistration);
	}

	@Override
	public void updateDealer(Dealer dealerRegistration) {
		dealerDao.updateDealer(dealerRegistration);
	}

	@Override
	public Dealer findDealerById(long id) {
		return dealerDao.findDealerById(id);
	}

	@Override
	public List<Dealer> findDealerByCode(String prv_code) {
		return dealerDao.findDealerByCode(prv_code);
	}

	@Override
	public List<Dealer> findAllDealers() {
		return dealerDao.findAllDealers();
	}

	@Override
	public void deleteDealer(Dealer dealer) {
		dealerDao.deleteDealer(dealer);
	}

	//## Dealer Contact
	@Override
	public void insertDealerContact(DealerContact dealerRegistrationContact) {
		dealerDao.insertDealerContact(dealerRegistrationContact);
	}

	@Override
	public void updateDealerContact(DealerContact dealerRegistrationContact) {
		dealerDao.updateDealerContact(dealerRegistrationContact);
	}

	@Override
	public DealerContact findDealerContactById(long id) {
		return dealerDao.findDealerContactById(id);
	}

	@Override
	public List<DealerContact> findAllDealerContacts() {
		return dealerDao.findAllDealerContacts();
	}

	@Override
	public void deleteDealerContact(long id) {
		dealerDao.deleteDealerContact(id);
	}

	//## Promotion
	@Override
	public void insertPromotion(Promotion promotion) {
		dealerDao.insertPromotion(promotion);
	}

	@Override
	public void updatePromotion(Promotion promotion) {
		dealerDao.updatePromotion(promotion);
	}

	@Override
	public Promotion findPromotionById(long id) {
		return dealerDao.findPromotionById(id);
	}

	@Override
	public List<Promotion> findAllPromotions() {
		return dealerDao.findAllPromotions();
	}

	@Override
	public void deletePromotion(long id) {
		dealerDao.deletePromotion(id);
	}

	//## ServiceRating
	@Override
	public void insertServiceRating(ServiceRating serviceRating) {
		dealerDao.insertServiceRating(serviceRating);
	}

	@Override
	public void updateServiceRating(ServiceRating serviceRating) {
		dealerDao.updateServiceRating(serviceRating);
	}

	@Override
	public ServiceRating findServiceRatingById(long id) {
		return dealerDao.findServiceRatingById(id);
	}

	@Override
	public List<ServiceRating> findAllServiceRatings() {
		return dealerDao.findAllServiceRatings();
	}

	@Override
	public void deleteServiceRating(long id) {
		dealerDao.deleteServiceRating(id);
	}
	
	//##Search Dealer
	@Override
	public 	List<SearchDealer> searchDealer(SearchDealer dealer){
		return dealerDao.searchDealer(dealer);
	}

	@Override
	public void insertDealerRecruitingCity(DealerRecruitingCity recruitingCity) {
		dealerDao.insertDealerRecruitingCity(recruitingCity);
	}

	@Override
	public void updateDealerRecruitingCity(DealerRecruitingCity recruitingCity) {
		dealerDao.updateDealerRecruitingCity(recruitingCity);
	}

	@Override
	public DealerRecruitingCity findDealerRecruitingCityById(long id) {
		return dealerDao.findDealerRecruitingCityById(id);
	}

	@Override
	public List<DealerRecruitingCity> findAllDealerRecruitingCities() {
		return dealerDao.findAllDealerRecruitingCities();
	}

	@Override
	public void deleteDealerRecruitingCity(long id) {
		dealerDao.deleteDealerRecruitingCity(id);
	}
	
	
	public int findTotalCountForDealerRecruitingCitiesByProvince(String province,String recruitmentType){
		return dealerDao.findTotalCountForDealerRecruitingCitiesByProvince(province, recruitmentType);
	}
	
	public int findTotalCountForDealerRecruitingCitiesByCity(String city,String recruitmentType){
		return dealerDao.findTotalCountForDealerRecruitingCitiesByCity(city, recruitmentType);
	}
	
	public int findTotalCountForDealerRecruitingCitiesByDistrict(String city, String district,String recruitmentType){
		return dealerDao.findTotalCountForDealerRecruitingCitiesByDistrict(city, district, recruitmentType);
	}
	public int findTotalCountForDealerRecruitingCities(String recruitmentType){
		return dealerDao.findTotalCountForDealerRecruitingCities(recruitmentType);
	}
	
	
	public List<DealerRecruitingCity> findDealerRecruitingCitiesByProvince(String province,String recruitmentType,int start,int end){
		
		return dealerDao.findDealerRecruitingCitiesByProvince(province,recruitmentType,start,end);
		
	}

	public List<DealerRecruitingCity> findDealerRecruitingCities(String recruitmentType,int start,int end){
		
		return dealerDao.findDealerRecruitingCities(recruitmentType,start,end);
		
	}
	public List<DealerRecruitingCity> findDealerRecruitingCitiesByCity(String city,String recruitmentType,int start,int end){
		
		return dealerDao.findDealerRecruitingCitiesByCity(city,recruitmentType,start,end);
		
	}
	public List<DealerRecruitingCity> findDealerRecruitingCitiesByDistrict(String city, String district,String recruitmentType,int start,int end){
		
		return dealerDao.findDealerRecruitingCitiesByDistrict(city, district,recruitmentType,start,end);
	}

	@Override
	public void insertDealerRecruitment(DealerRecruitment dealerRecruitment) {
		dealerDao.insertDealerRecruitment(dealerRecruitment);
	}

	@Override
	public void updateDealerRecruitment(DealerRecruitment dealerRecruitment) {
		dealerDao.updateDealerRecruitment(dealerRecruitment);
	}

	@Override
	public DealerRecruitment findDealerRecruitmentById(long id) {
		return dealerDao.findDealerRecruitmentById(id);
	}

	public int findTotalRecordDealerRecruitmentByUserId(long userId){
		return dealerDao.findTotalRecordDealerRecruitmentByUserId(userId);
	}
	
	public int findTotalRecordDealerRecruitmentsByStatusAndUserId(long userId,String status){
		return dealerDao.findTotalRecordDealerRecruitmentsByStatusAndUserId(userId, status);
	}
	
	public int getApplicationSequence(long declaredCityId){
		return dealerDao.getApplicationSequence(declaredCityId);
	}
	
	@Override
	public List<DealerRecruitment> findDealerRecruitmentByUserId(long userId,int start,int end){
		return dealerDao.findDealerRecruitmentByUserId(userId,start,end);
	
	}
	
	@Override
	public List<DealerRecruitment> findDealerRecruitmentsByStatusAndUserId(long userId,String status,int start,int end){
		return dealerDao.findDealerRecruitmentsByStatusAndUserId(userId,status,start,end);
	}

	@Override
	public void deleteDealerRecruitment(long id) {
		dealerDao.deleteDealerRecruitment(id);
	}

	//## Get Dealer Record list
	@Override
	public List<DealerRecordList> getDealerRecordList() {
		return dealerDao.getDealerRecordList();
	}	
	
	@Override
	public List<Dealer> filterDealerByProvince(String prv_code) {
		return dealerDao.filterDealerByProvince(prv_code);
	}
	@Override
	public List<Dealer> filterDealerByCity(String city_code) {
		return dealerDao.filterDealerByCity(city_code);
	}
	
	@Override
	public List<Dealer> filterDealer(Dealer dealer) {
		return dealerDao.filterDealerByCityProvinceAndType(dealer);
	}
	
	@Override
	public List<Dealer> filterDealerByType(String type) {
		return dealerDao.filterDealerByType(type);
	}
	
	@Override
	public List<String> findDealerRecruitingProvinces(){
		return dealerDao.findDealerRecruitingProvinces();
	}

	@Override
	public List<String> findDealerRecruitingCitiesByProvinceName(String province){
		return dealerDao.findDealerRecruitingCitiesByProvinceName(province);
	}

	@Override
	public List<String> findDealerRecruitingDistrictsByCityName(String city){
		return dealerDao.findDealerRecruitingDistrictsByCity(city);
	}

	@Override
	public List<DealerCityProvince> getDealerCityProvinceName(String lang_code) {
		return dealerDao.getDealerCityProvinceName(lang_code);
	}
}