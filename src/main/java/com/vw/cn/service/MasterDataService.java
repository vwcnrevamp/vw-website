package com.vw.cn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vw.cn.customDomain.DistrictCityProvince;
import com.vw.cn.customDomain.ProvinceAndCity;
import com.vw.cn.dao.MasterDataDao;
import com.vw.cn.domain.City;
import com.vw.cn.domain.Currency;
import com.vw.cn.domain.DealerType;
import com.vw.cn.domain.District;
import com.vw.cn.domain.Language;
import com.vw.cn.domain.Province;
import com.vw.cn.domain.RecruitmentNetworkType;
import com.vw.cn.domain.Role;
import com.vw.cn.domain.SourceSite;
/**
 * @author karthikeyan_v
 */
@Service
public class MasterDataService implements IMasterDataService{

	@Autowired
	MasterDataDao masterData;

	// Role
	@Override
	public void insertRole(Role role) {
		masterData.insertRole(role);
	}

	@Override
	public void updateRole(Role role) {
		masterData.updateRole(role);
	}

	@Override
	public Role findRoleById(long id) {
		return masterData.findRoleById(id);
	}

	@Override
	public List<Role> findAllRoles() {
		return masterData.findAllRoles();
	}

	@Override
	public void deleteRole(long id) {
		masterData.deleteRole(id);	
	} 

	// Lang
	@Override
	public void insertLanguage(Language lang) {
		masterData.insertLanguage(lang);
	}

	@Override
	public void updateLanguage(Language lang) {
		masterData.updateLanguage(lang);
	}

	@Override
	public Language findLanguageById(long id) {
		return masterData.findLanguageById(id);
	}

	@Override
	public Language findLanguageByCode(String code) {
		return masterData.findLanguageByCode(code);
	}

	@Override
	public List<Language> findAllLanguages() {
		return masterData.findAllLanguages();
	}

	@Override
	public void deleteLanguage(long id) {
		masterData.deleteLanguage(id);
	}

	// Province
	@Override
	public void insertProvince(Province province) {
		masterData.insertProvince(province);
	}

	@Override
	public void updateProvince(Province province) {
		masterData.updateProvince(province);
	}

	@Override
	public Province findProvinceById(long id) {
		return masterData.findProvinceById(id);
	}

	public Province findProvinceByCode(String code,String lang){
		return masterData.findProvinceByCode(code,lang);
	}

	@Override
	public List<Province> findAllProvinces(String lang) {
		return masterData.findAllProvinces(lang);
	}

	@Override
	public void deleteProvince(long id) {
		masterData.deleteProvince(id);
	}

	// City 
	@Override
	public void insertCity(City city) {
		masterData.insertCity(city);		
	}

	@Override
	public void updateCity(City city) {
		masterData.updateCity(city);
	}

	@Override
	public City findCityById(long id) {
		return masterData.findCityById(id);
	}
	
	@Override
	public City findCityByName(String name)	{
		return masterData.findCityByName(name);
	}

	@Override
	public City findCityByCode(String code,String lang) {
		return masterData.findCityByCode(code,lang);
	}

	@Override
	public List<City> findAllCities(String lang) {
		return masterData.findAllCities(lang);
	}

	@Override
	public void deleteCity(long id) {
		masterData.deleteCity(id);
	}

	// District
	@Override
	public void insertDistrict(District district) {
		masterData.insertDistrict(district);
	}

	@Override
	public void updateDistrict(District district) {
		masterData.updateDistrict(district);
	}

	@Override
	public District findDistrictById(long id) {
		return masterData.findDistrictById(id);
	}

	@Override
	public District findDistrictByCode(String code,String lang) {
		return masterData.findDistrictByCode(code,lang);
	}

	@Override
	public List<District> findAllDistricts(String lang) {
		return masterData.findAllDistricts(lang);
	}

	@Override
	public void deleteDistrict(long id) {
		masterData.deleteDistrict(id);
	}

	// RecruitmentNetworkType
	@Override
	public void insertRecruitmentNetworkType(RecruitmentNetworkType recruitmentNetwork) {
		masterData.insertRecruitmentNetworkType(recruitmentNetwork);
	}

	@Override
	public void updateRecruitmentNetworkType(RecruitmentNetworkType recruitmentNetwork) {
		masterData.updateRecruitmentNetworkType(recruitmentNetwork);
	}

	@Override
	public RecruitmentNetworkType findRecruitmentNetworkTypeById(long id) {
		return masterData.findRecruitmentNetworkTypeById(id);
	}

	@Override
	public RecruitmentNetworkType findRecruitmentNetworkTypeByCode(String code,String lang) {
		return masterData.findRecruitmentNetworkTypeByCode(code,lang);
	}

	@Override
	public List<RecruitmentNetworkType> findAllRecruitmentNetworkTypes(String lang) {
		return masterData.findAllRecruitmentNetworkTypes(lang);
	}

	@Override
	public void deleteRecruitmentNetworkType(long id) {
		masterData.deleteRecruitmentNetworkType(id);
	}

	//##Currency
	@Override
	public void insertCurrency(Currency currency) {
		masterData.insertCurrency(currency);		
	}

	@Override
	public void updateCurrency(Currency currency) {
		masterData.updateCurrency(currency);		
	}

	@Override
	public Currency findCurrencyById(long id) {
		return masterData.findCurrencyById(id);
	}

	@Override
	public List<Currency> findAllCurrencys() {
		return masterData.findAllCurrencys();
	}

	@Override
	public void deleteCurrency(long id) {
		masterData.deleteCurrency(id);	
	}

	public List<ProvinceAndCity> getallprovinceAndCities(String lang)
	{
		return masterData.getallprovinceAndCities(lang);
	}

	@Override
	public List<DistrictCityProvince> getAllDistrictCityProvince(String lang) {
		return masterData.getAllDistrictCityProvince(lang);
	}

	@Override
	public void insertSourceSite(SourceSite sourceSite) {
		masterData.insertSourceSite(sourceSite);		
	}

	@Override
	public void updateSourceSite(SourceSite sourceSite) {
		masterData.updateSourceSite(sourceSite);		
	}

	@Override
	public SourceSite findSourceSiteById(long id) {
		return masterData.findSourceSiteById(id);
	}

	@Override
	public SourceSite findSourceSiteByCode(String code,String lang) {
		return masterData.findSourceSiteByCode(code,lang);
	}

	@Override
	public List<SourceSite> findAllSourceSites(String lang) {
		return masterData.findAllSourceSites(lang);
	}

	@Override
	public void deleteSourceSite(long id) {
		masterData.deleteSourceSite(id);		
	}
	
	//## Dealer Type
	@Override
	public void insertDealerType(DealerType dealerType) {
		masterData.insertDealerType(dealerType);
	}

	@Override
	public void updateDealerType(DealerType dealerType) {
		masterData.updateDealerType(dealerType);		
	}

	@Override
	public DealerType findDealerTypeById(long id) {
		return masterData.findDealerTypeById(id);
	}

	@Override
	public List<DealerType> findAllDealerTypes(String lang_code) {
		return masterData.findAllDealerTypes(lang_code);
	}

	@Override
	public void deleteDealerType(long id) {
		masterData.deleteDealerType(id);	
	}
	
	@Override
	public List<District> getDistrictByCity(String cityCode,String lang) {
		return masterData.findDistrictByCity(cityCode, lang);
	}
}