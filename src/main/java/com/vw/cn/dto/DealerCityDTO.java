package com.vw.cn.dto;

import java.util.List;

public class DealerCityDTO {
	private String city_name;
	private List<DealerDistrictDTO> districts;
	
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public List<DealerDistrictDTO> getDistricts() {
		return districts;
	}
	public void setDistricts(List<DealerDistrictDTO> districts) {
		this.districts = districts;
	}

}
