package com.vw.cn.dto;

import java.util.List;


public class CarModelComparisionDto {


	private List<CarModelParameterDisplayDTO> basics;
	
	private List<CarModelParameterDisplayDTO> interiors;
	
	private List<CarModelParameterDisplayDTO> exteriors;
	
	private List<CarModelParameterDisplayDTO> safetyequipments;
	
	private List<CarModelParameterDisplayDTO> comforts;
	
	private CarModelDto model = null;	
	private CarModelDto model2 = null;
	private CarModelDto model3 = null;
	

	public List<CarModelParameterDisplayDTO> getBasics() {
		return basics;
	}

	public void setBasics(List<CarModelParameterDisplayDTO> basics) {
		this.basics = basics;
	}

	public List<CarModelParameterDisplayDTO> getInteriors() {
		return interiors;
	}

	public void setInteriors(List<CarModelParameterDisplayDTO> interiors) {
		this.interiors = interiors;
	}

	public List<CarModelParameterDisplayDTO> getExteriors() {
		return exteriors;
	}

	public void setExteriors(List<CarModelParameterDisplayDTO> exteriors) {
		this.exteriors = exteriors;
	}

	public List<CarModelParameterDisplayDTO> getSafetyequipments() {
		return safetyequipments;
	}

	public void setSafetyequipments(
			List<CarModelParameterDisplayDTO> safetyequipments) {
		this.safetyequipments = safetyequipments;
	}

	public List<CarModelParameterDisplayDTO> getComforts() {
		return comforts;
	}

	public void setComforts(List<CarModelParameterDisplayDTO> comforts) {
		this.comforts = comforts;
	}

	public CarModelDto getModel() {
		return model;
	}

	public void setModel(CarModelDto model) {
		this.model = model;
	}

	public CarModelDto getModel2() {
		return model2;
	}

	public void setModel2(CarModelDto model2) {
		this.model2 = model2;
	}

	public CarModelDto getModel3() {
		return model3;
	}

	public void setModel3(CarModelDto model3) {
		this.model3 = model3;
	}


}
