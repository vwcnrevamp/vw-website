package com.vw.cn.dto;

import java.util.ArrayList;
import java.util.List;

public class DealerRecruitingCityResponseDTO {
	int totalRecordCount;
	List<DealerRecruitingCityDTO> recruitingCities = new ArrayList<DealerRecruitingCityDTO>();
	
	
	public int getTotalRecordCount() {
		return totalRecordCount;
	}
	public void setTotalRecordCount(int totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}
	public List<DealerRecruitingCityDTO> getRecruitingCities() {
		return recruitingCities;
	}
	public void setRecruitingCities(List<DealerRecruitingCityDTO> recruitingCities) {
		this.recruitingCities = recruitingCities;
	}
	
	
	

}
