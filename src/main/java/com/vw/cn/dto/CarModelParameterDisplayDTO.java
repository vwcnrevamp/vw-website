package com.vw.cn.dto;

public class CarModelParameterDisplayDTO {
	private String parameterCode;
	private String parameterName;
	private CarParameterDto car1;
	private CarParameterDto car2;
	private CarParameterDto car3;
	
	
	public String getParameterCode() {
		return parameterCode;
	}
	public void setParameterCode(String parameterCode) {
		this.parameterCode = parameterCode;
	}
	public String getParameterName() {
		return parameterName;
	}
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	public CarParameterDto getCar1() {
		return car1;
	}
	public void setCar1(CarParameterDto car1) {
		this.car1 = car1;
	}
	public CarParameterDto getCar2() {
		return car2;
	}
	public void setCar2(CarParameterDto car2) {
		this.car2 = car2;
	}
	public CarParameterDto getCar3() {
		return car3;
	}
	public void setCar3(CarParameterDto car3) {
		this.car3 = car3;
	}

}
