package com.vw.cn.dto;

public class CarParameterDto {
	private String code;
	private String name;
	private String lang;
	private String description;
	private String carParameterType;
	
	private String value;
	private String category;
	private boolean displayCircle;
	private boolean displayImage;
	private String imageUrl;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCarParameterType() {
		return carParameterType;
	}
	public void setCarParameterType(String carParameterType) {
		this.carParameterType = carParameterType;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public boolean isDisplayCircle() {
		return displayCircle;
	}
	public void setDisplayCircle(boolean displayCircle) {
		this.displayCircle = displayCircle;
	}
	public boolean isDisplayImage() {
		return displayImage;
	}
	public void setDisplayImage(boolean displayImage) {
		this.displayImage = displayImage;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

}
