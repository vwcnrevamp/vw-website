package com.vw.cn.response;
/**
 * @author karthikeyan_v
 */
public class ResponseStatusCode {

	public static final int STATUS_OK 						=	200;
	public static final int STATUS_CREATED 					= 	201;
	public static final int STATUS_ACCEPTED					=	202;
	public static final int STATUS_NO_CONTENT				=	204;
	public static final int STATUS_NOTMATCHED 				=	205;
	public static final int STATUS_NORECORD 				=	206;
	public static final int STATUS_AlREADY_EXISTS			=	302;
	public static final int STATUS_NOT_MODIFIED				=	304;
	public static final int STATUS_BAD_REQUEST				=	400;
	public static final int STATUS_UNAUTHORIZED 			=	401;
	public static final int STATUS_NOT_FOUND				=	404;
	public static final int STATUS_NOT_ACCEPTABLE			=	406;
	public static final int STATUS_REQUEST_TIMEOUT			=	408;
	public static final int STATUS_CONFLICT 				=	409;
	public static final int STATUS_UNSUPPORTED_MEDIA_TYPE	=	415;
	public static final int	STATUS_INVALID 					=	422;
	public static final int	STATUS_LOGIN_TIMEOUT			=	440;
	public static final int	STATUS_NO_RESPONSE				=	444;
	public static final int STATUS_TOKEN_REQUIRED			=	499;
	public static final int STATUS_INTERNAL_ERROR 			=	500;
	public static final int STATUS_GATEWAY_TIMEOUT			=	504;
	public static final int STATUS_UNKNOWN_ERROR			=	520;
	public static final int STATUS_CONNECTION_TIMED_OUT		=	522;
	public static final int DEALER_PROVINCE_CANNOT_BE_EMPTY =   1101;
	public static final int DEALER_CITY_CANNOT_BE_EMPTY		=   1102;
	
	public static final int CAR_MODEL1_CANNOT_BE_EMPTY		=   1110;
	public static final int CAR_MODEL2_CANNOT_BE_EMPTY		=   1111;
	public static final int ERROR_CAR_COMPARISON			=	10;
}
